/**
 * Useful functions for the webpack project.
 */

/**
 * Build the output file path.
 * Add the filename at the end, without extension.
 *
 * @param {Array} context Splited key registered in webpack.entries. Useful to know output filepath & name.
 * @param {string} outputPathBeginBy The output filepath begins by this.
 * @param {int} contextOffsetToBeginNewPathBuilding In 'context' list, we take an extract from this offset's value.
 * @returns {string}
 */
const getFileWithoutExt = (context, outputPathBeginBy, contextOffsetToBeginNewPathBuilding) => {
    const contextLength = context.length;

    let file = outputPathBeginBy;

    for (let i = contextOffsetToBeginNewPathBuilding, iL = contextLength - 1; i < iL; i++) {
        file += context[i] + "/";
    }

    file += context[contextLength - 1];

    return file;
};

/**
 * Build output CSS file : path & name.
 * Add hash in production mode.
 * Add the extension.
 *
 * @param {Object} chunk Data about an entry file.
 * See this doc :
 * {@link https://webpack.js.org/plugins/mini-css-extract-plugin/#filename-option-as-function}
 * @param {string} mode Flag to build file with a name formatted for dev or production mode.
 * @returns {string}
 */
exports.getCssFile = (chunk, mode) => {
    let file = getFileWithoutExt(
        chunk.name.split("_"),
        "./css/",
        1
    );
    file += mode === "dev" ? ".css" : "_" + chunk.hash + ".css";

    return file;
};

/**
 * Build output JS file : path & name.
 * Add hash in production mode.
 * Add the extension.
 *
 * @param {Object} chunk Data about an entry file.
 * See this doc :
 * {@link https://webpack.js.org/plugins/mini-css-extract-plugin/#filename-option-as-function}
 * @param {string} mode Flag to build file with a name formatted for dev or production mode.
 * @returns {string}
 */
exports.getJsFile = (chunk, mode) => {
    let file = getFileWithoutExt(
        chunk.name.split("_"),
        "./js/",
        1
    );
    file += mode === "dev" ? ".js" : "_" + chunk.hash + ".js";

    return file;
};