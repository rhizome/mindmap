/**
 * Development tasks :
 * - Format css filename in output in dev mode : same name of source file.
 * - Idem for js filename.
 * - Processing with the dev mode of webpack.
 */

const {merge} = require("webpack-merge");
const common = require("./webpack.common.js");
const development_tasks_custom = require("./workspace/webpack/development_tasks");
const utils = require("./webpack.utils");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = merge(common, development_tasks_custom, {
    output: {
        filename: (pathData) => {
            let filename = '[name].js';

            /**
             * If a CSS file : will not run js filename calculation
             */
            if(typeof pathData.chunk.contentHash["css/mini-extract"] === "undefined") {
                filename =  utils.getJsFile(pathData.chunk, "dev");
            }

            return filename;
        },
    },
    mode: "development",
    devtool: false,
    plugins: [
        new MiniCssExtractPlugin({
            filename: ({chunk}) => {
                return utils.getCssFile(chunk, "dev");
            },
        }),
    ],
});
