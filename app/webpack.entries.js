const {merge} = require("webpack-merge");
const images = require("./workspace/webpack/images");

module.exports = merge(images, {
    styles : {
        import: './assets/scss/main.scss',
    },
    scripts: {
        import: "hind/front/Initializers/Main.js",
    }
});