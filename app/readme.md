VERSION DE HIND : 15.0.1

Pré-requis
----------------------

- node.js, npm et webpack sont installés et à jour __(pour node.js, il est conseillé d'utiliser la version spécifiée
  dans le package.json)__.
- __Le système ne fontionne pas avec windows__.

INSTALLATION DU PROJET
----------------------

Créer un répertoire "app"

Si besoin, créez un répertoire "admin"

Se placer dans le répertoire suivant : /app

Créer le package.json avec ce contenu :

    {
      "name": "< nom du projet >",
      "version": "1.0.0",
      "description": "",
      "main": "index.js",
      "scripts": {
        "watch": "BABEL_ENV=development webpack --watch --progress --config webpack.development.config.js",
        "build": "BABEL_ENV=production webpack --progress --config webpack.production.config.js",
        "server": "nodemon --exec babel-node server.js"
      },
      "engines": {
        "node": "20.18.0",
        "npm": ">=10.2.4"
      },
      "author": "",
      "license": "ISC",
      "devDependencies": {
        "hind": "git+ssh://git@github.com/matthieubarbaresco/hind.git#< tag >"
      },
      "overrides": {
        "glob": "10.4.2"
      }
    }

Tag de version HIND :

Préciser la version souhaitée.

[Liste des versions](https://github.com/matthieubarbaresco/hind/releases)

Il est important de préciser un tag plutôt que la _master_.

Et ce, afin de maîtriser les montées en version de HIND en testant votre projet au préalable.

Installer les dépendances du projet :

    npm install

Créer votre répertoire de travail piloté par HIND :

    npx hind create

Vous pouvez constater que le dossier /app contient l'architecture suivante :

    /assets => les fonts, les scripts js et (s)css, ainsi que les images et toutes autres ressources
    se trouveront dans ce dossier.
    Le SCSS est programmé dans le dossier /scss.
    Le dossier /dist - contenant les ressources servies pour le navigateur - y est construit via webpack.
    
    /local_config => S'y trouvent les différents fichiers de configuration de l'application.
    Plus de détails dans le chapitre suivant "Configuration de l'application".
    
    /workspace => C'est dans ce dossier que vous programmerez en JavaScript ES*.
    
    Divers fichiers on été ajoutés :
    => server.js : le gestionnaire du server node.
    => webpack.*.js : la config babel et les tâches webpack (5 fichiers)
    => la config babel - .babelrc - et le package.json pour la preprod, ainsi que la version preprod des config babel : .babelrc_preprod
    => 3 readme dont un à mettre dans le dossier admin, dans le cas d'un wordpress. Sinon, le supprimer.
    => un index.html vide
    => un .gitignore en racine du projet (si il existait déjà, il sera modifié avec le .gitignore de HIND).

CONFIGURATION DE L'APPLICATION
----------------------

Renseigner les fichiers de config suivants (dans "app/local_config") :

- common.js

        export default function common() {
            return {
                'dns': '< dns app >'
        };
        }

    - app.js

            import common from "./common";
          
            export default function app() {
                return {
                    "protocol_app": "< http | https >",
                    "dns_app": common().dns,
                    "url_assets": "< url assets >",
                    "port_app": `:< port front - enlever les : si port 80 >`
            };
            }

        - server.js

                import common from "./common";
            
                export default function server() {
                    return {
                        "instance": "< dev | preprod | prod >",
                        "port": < port server (int) >,
                        "host": common().dns,
                        "filepath_manifest_file": "< chemin absolu >/< manifest file path >",
                        "protocol": < http | https >,
                        "site_is_enabled": < true | false >,
                        "whitelist_ip_maintenance": [],
                          "redirect": {
                              /*"myapp.local:3000": {
                                  "type_of": 301,
                                  "to_url": "https://myapp.paris"
                              }*/
                        }
                      };
                  }

Un fichier de configuration doit aussi figurer pour chaque API utilisée.
Par exemple, pour Wordpress : app/local_config/api/wordpress.js.
Et voici son contenu :

    export default function wordpress() {
        return {
            "< ID de config - ex: wp >": {
                "protocol" : "< http | https >",
                "dns" : "< dns api >",
                "rest_api_endpoint" : "wp-json/wp/v2",
                "custom_rest_api_endpoint" : "wp-json/wrac/v< version majeure voulue >",
                "port" : "< port api >",
                "token" : <false | "ab" >
            }
        };
    }

APPEL DE VOS FICHIERS JS
-------------------------------

    => Faire vos 'require()' dans workspace/webpack/scripts.js

APPEL DE VOS FICHIERS CSS
-------------------------------

    => Faire vos 'require()' dans workspace/webpack/styles.js

APPEL DE VOS IMAGES STATIQUES
-------------------------------

=> Faire vos importations d'images statiques (ssi vous les utilisez dans un template HTML. Si c'est dans SASS, ce sera
importé automatiquement) ici :

    => workspace/webpack/images.js

Tâches Webpack
-------------------------------

Dans `workspace/webpack`, vous trouverez ces 3 fichiers :

- common_tasks.js : Les tâches communes à l'orchestration pour le développement et la mise en production.
- development_tasks.js : les tâches spécifiques à l'orchestration pour le développement.
- production_tasks.js : Les tâches spécifiquesà l'orchestration pour la production.

Vous pouvez donc personnaliser les tâches Webpack, en surcharge de celles présentes dans webpack.common.js,
webpack.development.config.js et webpack.production.config.js.

HTACCESS
----------------------

Renseigner son contenu :

    <IfModule mod_rewrite.c>
    RewriteEngine On
    RewriteBase /
    RewriteRule ^index\.html$ - [L]
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteRule . /index.html [L]
    </IfModule>
    
    <FilesMatch "\.(ttf|otf|eot|woff|svg)$">
        <IfModule mod_headers.c>
            SetEnvIf Origin "< url app >:< port app >" AccessControlAllowOrigin=$0
            Header add Access-Control-Allow-Origin %{AccessControlAllowOrigin}e env=AccessControlAllowOrigin
            Header always set Access-Control-Allow-Headers "x-url, x-postData"
        </IfModule>
    </FilesMatch>

Pour travailler en local
-

__Se placer dans app/.__

- Dans un premier terminal, lancez le watcher (si linux / mac) :

        npm run watch

- Et dans un second terminal, lancez le serveur nodejs :

        npm run server

- Pour travailler en local, renseignez ceci dans "app/config/server.js" :

       "instance" : "dev",

Préparation pour la (pre)production
-

- Pour envoyer en dev, preprod, prod, renseignez ceci dans "app/config/server.js" :

        "instance" : "prod",

- Pour préparer les CSS et JavaScript pour la prod, lancez un build :

        npm run build

HELPER (pour les vues)
------------------------------

    - Admin_to_app_url : supression dans l'url du sous-domaine de l'administration
    
    - Assets : Construction de la balise html pour les scripts js, les css, la favicon, les img
    
    - Menu_item : Retourne le slug d'une url à l'intérieur d'un objet de type item d'un objet menu
    
    - Modules : Appel une méthode d'un module comme Facebook ou Pagination
    
    - Templates : Affichage ou non du template appelé, selon sa localisation :
        
            => selon le template en cours : en priorité
            
            => selon le nom de la page en cours : viens en second temps,
            et seulement si le template n'est pas actif selon le type de template en cours.

BROWSER-SYNC - PAS DISPONIBLE
------------

    => Le watcher, à son chargement, lance votre site en local sur votre navigateur par défaut.

MAJ DU FRAMEWORK
----------------------

Dans le package.json de /app, ciblez le numéro de tag à récupérer pour mettre à jour hind :

    "hind": "git+ssh://git@github.com/matthieubarbaresco/hind.git#< version du tag visé >"

=> Si aucun tag n'est ciblé, les derniers commit de la branche "master" seront tirés du dépôt : non conseillé.

Dans le répertoire /app :

    - Supprimer le dossier de Hind :  rm -r node_modules/hind
    
    - npm install
    
    - npx hind update