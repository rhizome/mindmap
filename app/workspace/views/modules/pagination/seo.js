export default function seo(href_prev, href_canonical, href_next) {
    let html = "";

    if (href_prev !== null) {
        html += `
            <link rel="prev" href="${href_prev}" />
        `;
    }
    if (href_canonical !== null) {
        html += `
            <link rel="canonical" href="${href_canonical}" />
        `;
    }
    if (href_next !== null) {
        html += `
            <link rel="next" href="${href_next}" />
        `;
    }

    return html;
}