import seo from "./seo";
import pagination from "./pagination";

export {seo, pagination};