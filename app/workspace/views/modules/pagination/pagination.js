export default function pagination(total_pages, uri_info, page) {
    //console.log("template");
    //console.log(uri_info);
    return `
        <div id="pagination">
            <div class="pagination_wrapper">
                <div class="pagination_content">
                    ${get_pagination_area(total_pages, uri_info, page)}
                </div>
            </div>
        </div>
    `;
}

const get_pagination_area = (total_pages, uri_info, page) => {
    let html = "";

    if (page === 1) {
        html = `
            ${get_number_list(total_pages, uri_info, page)}
            <button id="next_page" class="button_pagination button_pagination_next" data-number="${page + 1}"></button>
        `;
    } else if (page === total_pages) {
        html = `
            <button id="prev_page" class="button_pagination button_pagination_prev" data-number="${page - 1}"></button>
            ${get_number_list(total_pages, uri_info, page)}
        `;
    } else {
        html = `
            <button id="prev_page" class="button_pagination button_pagination_prev" data-number="${page - 1}"></button>
            ${get_number_list(total_pages, uri_info, page)}
            <button id="next_page" class="button_pagination button_pagination_next" data-number="${page + 1}"></button>
        `;
    }

    return html;
};

const get_number_list = (total_pages, uri_info, page) => {
    let html = "";

    for (let i = 1; i <= total_pages; i++) {
        const link = (i === 1)
            ?
            `/${uri_info.uri}`
            :
            `/${uri_info.uri}/${i}`
        ;

        if (i === page) {
            html += `
                <span class="page_number">${i}</span>
            `;
        } else {
            html += `
                <a class="pagination_link other_browsing_manager" href="${link}" data-number="${i}">
                    <span class="page_number">${i}</span>
                </a>
            `;
        }
    }

    return html;
};