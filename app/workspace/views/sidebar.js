export default function sidebar(data) {
    return `
    <sidebar id="menu" class="sidebar_wrapper">
    
            
            ${nav(data.header_menu)}
            
        
    </sidebar>
   `;
};

const nav = (menu) => {
    return `
    <nav class="sidebar">
        <ul>
           ${items_nav(menu.items)}
        </ul>
    </nav>
   `;
};

const items_nav = (items) => {
    let nav = "";

    for (let item in items) {
        nav += `
        <li style="background-color: ${items[item].title}">
            <a data-slug="${items[item].slug}" href="${items[item].uri}">
                ${items[item].title}
            </a>
        </li>
        `;
    }

    return nav;
};