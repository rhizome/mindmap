export default class Model {
    static get() {
        return {
            mapping_nodename_with_their_ways: {
                '1336 - Coop. production de thé '               : ['0,3,4'],
                '13ticules'                                     : ['1,1,0,2,0,0'],
                'A2C-AutonomieDeClasse'                         : ['1,1,2,2,0'],
                'accueil de jour Sirakadjan'                    : ['0,6,1,3'],
                'Actions Communes'                              : ['3,8,9'],
                Addictions                                      : ['1,1,0,4', '1,1,2,4,2,4'],
                'Adèle'                                         : ['1,1,7,5,0,3', '3,9,0,3'],
                Administratif                                   : ['1,1,3,1'],
                'Agence Centre|13002'                           : ['0,15,0,0,0', '3,14,0,0,0'],
                'Agence Est|13004'                              : ['0,15,0,0,1', '3,14,0,0,1'],
                'Agence Nord|13014'                             : ['0,15,0,0,3', '3,14,0,0,3'],
                'Agence Sud|13008'                              : ['0,15,0,0,2', '3,14,0,0,2'],
                Agendas                                         : ['3,0'],
                'AG InterPro'                                   : ['0,6,3,0'],
                'AG-InterPro'                                   : ['1,0,2,0,0'],
                'AG précaires'                                  : ['0,6,4,2', '1,0,2,1,0,2', '2,1,0,2'],
                AiLSi                                           : ['1,0,6,1'],
                'Aïolibre'                                      : ['0,6,2,5'],
                'Al Manba'                                      : ['0,6,6', '1,1,3,0,2'],
                Alternatiba                                     : ['0,6,2,0'],
                'antifa social club'                            : ['0,6,1,10'],
                'Antifa Social Club'                            : ['1,0,5,4'],
                'ANV COP-21m'                                   : ['0,6,2,0,0'],
                'Approches Cultures & Territoires'              : ['1,1,4,6', '3,0,5'],
                Aquaponie                                       : ['3,13,0,0,2'],
                Artisanat                                       : ['1,1,2,1'],
                Arts                                            : ['1,1,2,0'],
                'Arts Martiaux/Box'                             : ['1,1,5,3'],
                'Assemblées Antifa'                             : ['1,0,5,5'],
                'Assistance Sociale'                            : ['0,15,0', '3,14,0'],
                Asso                                            : ['0,6,4,4', '1,0,2,1,0,4', '2,1,0,4'],
                'Association Cuve'                              : ['0,3,2', '1,1,7,0,1', '1,1,7,4,2'],
                'Atelier Couture (DAR)'                         : ['1,1,2,1,0,0'],
                'Atelier de Couture'                            : ['0,6,1,5'],
                'Atelier du Mouvement (danse)'                  : ['0,6,1,1'],
                'Atelier Palmas(€)'                             : ['1,1,2,0,1,2'],
                'Atelier Recherche Formation'                   : ['0,6,12,1'],
                Ateliers                                        : ['1,1,6,3', '3,10'],
                'Ateliers Nebula'                               : ['1,1,2,4,0'],
                'Ateliers Vélo'                                 : ['3,10,0'],
                'Atelier Théâtre'                               : ['0,6,1,2'],
                "AUP - Demandeurs d'asile"                      : ['1,1,3,0,6', '1,1,4,7'],
                'Autodéfense'                                   : ['1,1,5'],
                Autonomes                                       : ['1,0,2,0'],
                Autonomie                                       : ['3,13,0'],
                Autres                                          : ['1,1,2,6'],
                'Autres Regards'                                : ['1,1,0,3,7', '1,1,3,3,9'],
                AVAD                                            : ['1,1,1,2', '1,1,3,3,11'],
                'Banques Alternatives'                          : ['1,0,3,0'],
                'Bar à Vrac'                                    : ['1,1,7,5,0,0', '3,9,0,0'],
                'Bars | Cafés | Salles'                         : ['0,2'],
                'Bataille Médiatique'                           : ['1,0,0'],
                'Bibliothèque Alcazar'                          : ['0,5,0', '0,8,1'],
                'Bibliothèque Nulle part Ailleurs'              : ['0,6,1,6'],
                'Bien Être'                                     : ['1,1,2,4,2,0'],
                Biodigesteur                                    : ['3,13,0,0,3'],
                Blocage                                         : ['1,0,1'],
                Blogs                                           : ['1,0,0,2'],
                Bouffe                                          : ['1,1,7'],
                'Boulangerie-Café Pain Salvator'                : ['1,1,7,5,2,2', '3,9,2,2'],
                Boulangeries                                    : ['1,1,7,5,2', '3,9,2'],
                'Bourse du Travail'                             : ['0,6,8'],
                Box                                             : ['0,6,14,0'],
                'Box Morozoff'                                  : ['1,1,5,3,1'],
                'Brigade Antifa'                                : ['1,0,5,2'],
                'BTP autonome'                                  : ['0,6,4,1', '1,0,2,1,0,1', '2,1,0,1'],
                'Bus 31/32'                                     : ['1,1,0,4,1'],
                "Café l'Ecomotive"                              : ['1,1,7,5,1,2', '3,9,1,2'],
                'Café Villageois (Lauris)'                      : ['0,7,0'],
                'Calculs Mutuelle'                              : ['3,12,0'],
                'Cantine Chaubouillante'                        : ['0,6,1,8'],
                'Cantine du Midi'                               : ['1,1,7,1,2', '1,1,7,5,1,0', '3,9,1,0'],
                'Cantines Solidaires'                           : ['1,1,7,1'],
                'Cantine The Noble Kitchen'                     : ['0,6,1,7'],
                'Carte Autogéré Rhizome'                        : ['3,1,1'],
                'Carte des Luttes - Reporterre'                 : ['3,1,3'],
                Cartes                                          : ['3,1'],
                'Carto-Marseille'                               : ['3,1,5'],
                'Casa Consolat'                                 : ['1,1,7,1,7', '1,1,7,2,1', '1,1,7,5,1,3', '3,9,1,3'],
                'Causeries Mensueles'                           : ['0,6,12,0'],
                CCAS                                            : ['0,15,0,0', '3,14,0,0'],
                'Centre LGBTQIA+'                               : ['0,6,9', '1,1,0,3,9', '1,1,2,3,2', '1,1,3,3,6', '3,0,4'],
                'Cercle de Nageurs en Eaux Troubles'            : ['0,6,0,2'],
                'CGT chômeur précaires'                         : ['0,6,8,0'],
                'CGT spectacle'                                 : ['0,6,8,1'],
                'Chaines Youtube'                               : ['1,0,0,1'],
                CHO3                                            : ['1,1,4,1', '1,1,7,1,0'],
                'Chomage | Precarité'                           : ['1,1,3,4'],
                'Chomeurs Précaires 13'                         : ['1,1,3,4,1'],
                Chomheureuses                                   : ['1,1,3,4,0'],
                CIMK                                            : ['1,0,10,0'],
                'Ciné Bar (ClubCoop)'                           : ['0,2,0'],
                'Cinéma'                                        : ['1,1,2,0,3'],
                'Cinémas'                                       : ['0,0'],
                CIRA                                            : ['0,5,11', '0,6,12'],
                'Circuit Court'                                 : ['1,1,7,5', '3,9'],
                Cirque                                          : ['1,1,2,0,2'],
                "Cité d'Arts de la Rue"                         : ['0,8,0'],
                "Cité de l'Agriculture"                         : ['0,5,1', '1,1,4,0'],
                'Cité des Associations'                         : ['0,8,2'],
                Clownozoff                                      : ['0,6,14,1'],
                'CLUB PARENTHESE'                               : ['1,1,0,3,4,1', '1,1,3,3,2,1'],
                CNT13                                           : ['0,6,3,1', '1,0,2,1,1', '2,1,1'],
                Coalitions                                      : ['2,3'],
                COFOR                                           : ['1,1,0,3,1', '1,1,3,3,1'],
                'Collectif Anti-Gentrification'                 : ['1,0,7,0'],
                'Collectif Binkadi'                             : ['1,1,3,0,7', '1,1,4,9'],
                'Collectifs | Assos | Projets'                  : ['1'],
                'Collectif Vélos en Ville'                      : ['3,10,0,1'],
                'Collectif WD-40'                               : ['1,1,1,0'],
                'Colletif Antifa'                               : ['1,0,5,3'],
                'Comité Local 13'                               : ['1,0,1,1,0'],
                'CoMob-StCharles'                               : ['1,0,2,0,2'],
                Compostage                                      : ['3,13,0,2'],
                'Confédération Paysanne PACA'                   : ['1,0,2,1,3', '2,1,3'],
                Contraception                                   : ['1,1,0,2,0'],
                'Cours FLE'                                     : ['0,6,6,1', '1,1,3,0,2,1'],
                Couture                                         : ['1,1,2,1,0'],
                CQFD                                            : ['0,6,0,7', '1,0,0,0,0', '3,2,4', '3,5,4'],
                Crefada                                         : ['1,1,2,6,0'],
                'CRIR-AVS PACA'                                 : ['1,1,2,4,3'],
                Cryptomonnaies                                  : ['1,0,3,2'],
                Cuisine                                         : ['1,1,2,0,5'],
                'Cultures Du Cœur 13'                           : ['1,1,3,4,2'],
                DATA                                            : ['0,2,4', '0,5,12', '3,3,6'],
                'Démosphère'                                    : ['3,0,7'],
                'De Saint-Alban à La Borde - France Culture'    : ['3,4,1,0'],
                DICADD                                          : ['3,1,6'],
                'Dispositifs Travail Singularité'               : ['1,2,0'],
                Droit                                           : ['1,1,2,4,2,3'],
                EAH                                             : ['0,15,0,2,0', '3,14,0,2,0'],
                'Écoute Active'                                 : ['1,1,2,4,1,0'],
                'Éducation | Formation'                         : ['1,1,2'],
                'EELV-PACA'                                     : ['2,0,1'],
                'El Aché de Cuba'                               : ['0,2,2'],
                'Énergie'                                       : ['3,13,0,1'],
                'Entreprises-Autog.'                            : ['1,1,7,4'],
                'Entreprises Autogérés'                         : ['0,3'],
                'Entrerpises | Cooperatives'                    : ['0,4'],
                'Épiceries'                                     : ['1,1,7,5,0', '3,9,0'],
                'Equipe SIDIIS'                                 : ['1,1,0,3,12'],
                'Exilées'                                       : ['1,1,3,0'],
                'Extrême Jonglerie'                             : ['1,1,2,0,2,0'],
                ' Face à la Gentrification'                     : ['1,0,7'],
                'Face à la Psychiatrie'                         : ['1,0,6'],
                'Face à la Surveillance'                        : ['1,0,8'],
                'Face au Fascisme'                              : ['1,0,5'],
                'Face au Néo-colonialisme'                      : ['1,0,4'],
                'Face au système Financier'                     : ['1,0,3'],
                'Face aux lois/politiques Gouv.'                : ['1,0,2'],
                'F. au Système Alimentaire'                     : ['1,0,9'],
                'Fédération Anarchiste'                         : ['2,2,0'],
                'Fédérations'                                   : ['2,2'],
                "Ferme de l'Etoile"                             : ['0,11,6'],
                "Ferme du Roy d'Espagne"                        : ['0,11,4'],
                'Ferme Pastière (Meryrargues)'                  : ['0,11,5'],
                Fermes                                          : ['0,11'],
                Festivals                                       : ['3,11'],
                'Filature de Chantemerle (Hautes-Alpes)'        : ['0,10,1,3', '0,11,0,3'],
                'Folie et Politique -Barge'                     : ['3,6,1'],
                'Fondation ERIE'                                : ['1,0,6,0'],
                FransGenre                                      : ['3,1,7'],
                Freinet                                         : ['3,13,1,0'],
                Friperies                                       : ['1,1,7,5,5', '3,9,5'],
                "Frip'Insertion - Capelette "                   : ['1,1,7,5,5,1', '3,9,5,1'],
                "Frip'Insertion - Libération"                   : ['1,1,7,5,5,0', '3,9,5,0'],
                GEMs                                            : ['1,1,0,3,4', '1,1,3,3,2'],
                'Genre de Lutte'                                : ['0,6,15,0'],
                'Genre & Sexualités'                            : ['1,1,2,3'],
                'Gestion Déchets'                               : ['1,1,7,3'],
                'Gestion de Crises'                             : ['1,1,2,4,0,0'],
                'G.L.A.M'                                       : ['0,6,9,0', '1,1,3,0,5'],
                'Grange neuve (Forcarlquier)'                   : ['0,10,1,0', '0,11,0,0'],
                'Graph Maker'                                   : ['3,12,1,0,1'],
                'Gratuit pour les urgences administratives'     : ['0,6,11,0'],
                'Groupe Anti-gentrification'                    : ['0,6,0,5'],
                'GSN Géstion de Conflits'                       : ['0,12,0,0'],
                Habicoop                                        : ['3,1,12', '3,8,8'],
                'Hameaux Légers'                                : ['3,1,13'],
                'H.A.S'                                         : ['1,1,3,3,4'],
                'House of Pain'                                 : ['1,1,7,5,2,3', '3,9,2,3'],
                Hypnose                                         : ['1,1,2,4,1,1'],
                'IMAJSanté'                                     : ['1,1,0,3,6', '1,1,3,3,3'],
                'Imprimerie Partagée'                           : ['0,6,11'],
                InfoKiosques                                    : ['3,6'],
                'Infra-personnel'                               : ['1,2'],
                Internationnalisme                              : ['1,0,10'],
                'Jam Poèsie·Musique'                            : ['0,2,1,0'],
                'Journal Ventilo'                               : ['3,0,1'],
                'Journaux Indépendants'                         : ['1,0,0,0'],
                Juridique                                       : ['3,7,1'],
                JUST                                            : ['1,1,0,3,2', '1,1,4,3'],
                'La Base'                                       : ['0,6,2'],
                'La Bastide à Fruits'                           : ['0,11,8'],
                'Laboratoires de vie'                           : ['0,10'],
                'La Cabrery (Luberon)'                          : ['0,10,1,2', '0,11,0,2'],
                'La Caillasse (Cucuron)'                        : ['0,11,1'],
                'La Cloche Sud'                                 : ['1,1,4,8'],
                'La Cuisine du 101'                             : ['1,1,3,4,3', '1,1,6,0,1', '1,1,7,1,5'],
                'La DAR'                                        : ['0,5,4', '0,6,1'],
                'La Déviation'                                  : ['0,6,16'],
                'La ferme Capri'                                : ['0,11,2', '1,1,7,0,3'],
                'La Ĝ (June)'                                   : ['1,0,3,2,0'],
                'La lutte enchantée'                            : ['1,1,2,0,1,1'],
                'La Marmite Joyeuse'                            : ['1,1,7,1,4', '1,1,7,5,1,5', '3,9,1,5'],
                'La Merveilleuse'                               : ['0,2,1'],
                'La Nebula'                                     : ['0,6,7'],
                'La NEF'                                        : ['1,0,3,0,0'],
                Langues                                         : ['1,1,3,2'],
                'La plaine fraicheur'                           : ['1,1,7,5,0,2', '3,9,0,2'],
                "L'Après M"                                     : ['0,3,0', '1,1,7,4,0'],
                'La Réserve des Arts'                           : ['0,9,2'],
                'La Roue (monnaie)'                             : ['0,6,2,6', '1,0,3,1,0', '1,1,4,5'],
                'La Ruche'                                      : ['0,4,0'],
                "L'Astronef"                                    : ['0,1,0'],
                'La Tarantula'                                  : ['0,12,1'],
                'La terre en Thiers'                            : ['1,0,0,0,2', '3,5,5'],
                "L'Auberge"                                     : ['0,14,0', '1,1,3,3,12'],
                'Le Bar à Pain'                                 : ['1,1,7,5,2,0', '3,9,2,0'],
                'Le Bouillon de Noailles'                       : ['1,1,2,0,5,0', '1,1,7,1,3'],
                'Le Café des Chômheureuses'                     : ['0,6,1,0'],
                'Le Carillon'                                   : ['3,1,16'],
                'LE CHÂTEAU EN SANTÉ'                           : ['0,16,1', '1,1,0,3,11', '1,1,3,3,10'],
                'Le Coeur sur la table'                         : ['3,4,0,2'],
                "L'école des vivants"                           : ['0,10,0'],
                'Le Collet des Comtes'                          : ['0,11,3'],
                'Legal Team 13'                                 : ['1,1,5,0'],
                'Le GR1'                                        : ['1,1,3,0,1'],
                'Le Grain de Sable'                             : ['1,1,7,5,1,4', '3,9,1,4'],
                'Le Gyptis'                                     : ['0,0,2'],
                'Le Marché Rouge'                               : ['1,1,7,2,2', '1,1,7,5,3,0', '3,8,5', '3,9,3,0'],
                "L'Embobineuse"                                 : ['0,2,3'],
                'Le Morozoff'                                   : ['0,6,14'],
                'Le moteur stirling'                            : ['3,13,0,0,0'],
                "L'Épicerie Paysanne"                           : ['0,3,1', '1,1,7,4,1', '1,1,7,5,0,1', '3,9,0,1'],
                'Le Plan de A à Z'                              : ['0,4,1', '0,9,4'],
                'Le Polygone Étoilé'                            : ['0,0,0'],
                'Les 8 Pillards'                                : ['0,6,10'],
                'Les ateliers blancarde'                        : ['0,9,1'],
                'Les Cahiers A2C'                               : ['3,5,2'],
                'Les Champi. de Marseille'                      : ['1,1,7,0,0'],
                'Les Champignons de Marseille (?)'              : ['0,3,3'],
                'Les Champignons de Marseille'                  : ['1,1,7,4,3'],
                'Les Couilles sur la table'                     : ['3,4,0,1'],
                'Les Écotables'                                 : ['3,1,15'],
                'les gadjis'                                    : ['0,6,1,9'],
                'Les Héroïnes'                                  : ['0,5,10'],
                'Les Mains Gauches-festival queer féministe'    : ['0,6,13,0'],
                'Les Mains Libres'                              : ['1,1,7,5,2,1', '3,9,2,1'],
                'Le Snack'                                      : ['0,12,0'],
                'Le Social Brûle 13'                            : ['0,6,4,5', '1,0,2,0,3', '1,0,2,1,0,5', '2,1,0,5'],
                'Les Ondines'                                   : ['1,1,7,5,1,1', '3,9,1,1'],
                'Les Paniers Marseillais'                       : ['1,1,7,2,0'],
                'Le Spot-Longchamp'                             : ['0,16,0'],
                'LES RASCASSES'                                 : ['1,1,2,0,1,0'],
                LESTOCKK                                        : ['0,2,5'],
                'Le Talus'                                      : ['0,9,3', '0,11,7', '1,1,7,0,2'],
                'Le Vortex'                                     : ['3,0,0'],
                LFI13                                           : ['1,0,2,2,2', '2,0,0'],
                'LGBT+ PACA'                                    : ['3,1,11'],
                "L'Histoire de l'oeil"                          : ['0,5,9'],
                "L'Hydre aux milles têtes"                      : ['0,5,6'],
                'Librairies | Bibliothèques'                    : ['0,5'],
                "Lieu d'Échanges et d'Ouvertures LEO"           : ['1,1,0,3,4,0', '1,1,3,3,2,0'],
                'Lieu de Répit'                                 : ['0,13,0'],
                Lieux                                           : ['0'],
                'Lieux de Répit'                                : ['0,13'],
                'Lieux Publics'                                 : ['0,8'],
                'Lieux Ruraux Autogérés'                        : ['0,7'],
                'Lieux Urbains Autogérés'                       : ['0,6'],
                'LiveGap Charts'                                : ['3,12,1,0,0'],
                Logement                                        : ['0,15,0,2', '1,1,6,1', '1,1,8', '3,14,0,2'],
                'Longo Maï'                                     : ['0,10,1', '0,11,0'],
                Lounapo                                         : ['1,1,2,5,0'],
                'Luttes Locales'                                : ['1,1,4'],
                Macropolitique                                  : ['1,0'],
                'Maisons de Solidarité'                         : ['0,15,0,1', '3,14,0,1'],
                'maladroite boxe populaire'                     : ['0,6,1,4'],
                'Maladroite BoxPop'                             : ['1,1,5,3,0'],
                Manifesten                                      : ['0,5,7', '0,6,0'],
                Manifestin                                      : ['0,6,0,0', '1,1,7,1,6'],
                Manoeuvre                                       : ['1,1,2,3,0,0'],
                'Marché des artisans et des producteurs bio'    : ['1,1,7,5,3,1', '3,9,3,1'],
                'Marchés'                                       : ['1,1,7,5,3', '3,9,3'],
                Marsactu                                        : ['1,0,0,0,1'],
                'Marseille vs Darmanin'                         : ['1,0,2,0,1'],
                'Mars Infos Autonomes'                          : ['3,0,2', '3,2,2'],
                Marsmut                                         : ['1,1,6,0,0'],
                Marsnet                                         : ['1,1,6,4,0'],
                MARSS                                           : ['0,6,0,1', '1,1,0,3,0', '1,1,3,3,0', '1,1,4,2'],
                "Mars'Soins"                                    : ['1,1,5,2'],
                'Masculinités'                                  : ['1,1,2,3,0'],
                'Mas de Granier (Saint Martin de Crau)'         : ['0,10,1,1', '0,11,0,1'],
                'Massalia VOx'                                  : ['0,6,5'],
                'Matériel/Outils'                               : ['1,1,6,0'],
                'MDS BELLE DE MAI | 03'                         : ['0,15,0,1,2', '3,14,0,1,2'],
                'MDS CHARTREUX | 04'                            : ['0,15,0,1,3', '3,14,0,1,3'],
                'MDS COLBERT | 01'                              : ['0,15,0,1,0', '3,14,0,1,0'],
                'MDS DE BONNEVEINE | 08'                        : ['0,15,0,1,5', '3,14,0,1,5'],
                'MDS DE LA VISTE | 15'                          : ['0,15,0,1,10', '3,14,0,1,10'],
                "MDS DE L'ESTAQUE | 16"                         : ['0,15,0,1,11', '3,14,0,1,11'],
                'MDS DU LITTORAL | 02'                          : ['0,15,0,1,1', '3,14,0,1,1'],
                'MDS LE NAUTILE | 13'                           : ['0,15,0,1,8', '3,14,0,1,8'],
                'MDS MALPASSÉ | 13'                             : ['0,15,0,1,9', '3,14,0,1,9'],
                'MDS PONT DE VIVAUX | 10'                       : ['0,15,0,1,6', '3,14,0,1,6'],
                'MDS SAINT MARCEL | 11'                         : ['0,15,0,1,7', '3,14,0,1,7'],
                'MDS ST SEBASTIEN | 06'                         : ['0,15,0,1,4', '3,14,0,1,4'],
                'Médias'                                        : ['3,2'],
                'Médiation | Gestion de Conflits'               : ['1,1,1'],
                'Mémoire des Sexualités'                        : ['0,5,8', '0,6,15'],
                Micropolitique                                  : ['1,1'],
                'Mille Bâbords'                                 : ['0,5,3', '0,6,3', '3,0,3', '3,2,3'],
                Mobilizon                                       : ['3,0,8'],
                'Modules COFOR'                                 : ['1,1,2,4,2'],
                'Monnaies Locales'                              : ['1,0,3,1'],
                'Mot de Passe (Français)'                       : ['1,1,3,2,0'],
                'Mouche Soldat Noir'                            : ['3,13,0,0,1'],
                Musique                                         : ['1,1,2,0,1'],
                Mutualisation                                   : ['1,1,6'],
                'Mutuelle MTPGB'                                : ['1,1,6,2,0'],
                Navigation                                      : ['1,1,2,5'],
                'NFP-13'                                        : ['2,3,1'],
                'Nouvelle Aube'                                 : ['1,1,0,3,3', '1,1,0,4,0', '1,1,4,4'],
                NPA13                                           : ['1,0,2,2,1', '2,0,2'],
                'Numérique'                                     : ['1,1,6,4'],
                'Numériques'                                    : ['3,12,1'],
                NUPES13                                         : ['1,0,2,2,0', '2,3,0'],
                'Orga. Politiques'                              : ['2'],
                Outils                                          : ['3,12'],
                Palama                                          : ['0,13,1'],
                Partis                                          : ['1,0,2,2', '2,0'],
                'Paul B. Preciado, trans-philosophe'            : ['3,4,0,4'],
                'Paul B. Preciado : trans révolutionnaire'      : ['3,4,0,5'],
                'Pédagogie'                                     : ['3,13,1'],
                'Pédagogie Institutionnelle'                    : ['3,13,1,1'],
                Peinture                                        : ['1,1,2,0,4'],
                'Permanence Juridique'                          : ['0,6,6,0', '1,1,3,0,2,0'],
                'Planning Familial 13'                          : ['1,1,0,3,5', '1,1,3,3,7'],
                Podcasts                                        : ['3,4'],
                Politique                                       : ['1,1,2,2'],
                'Pompe à bélier'                                : ['3,13,0,0,4'],
                'Pompes à eau'                                  : ['3,13,0,4'],
                'Porn on Mars festival de porn queer'           : ['1,1,2,0,3,0'],
                'Pour une Thérapie Transversale'                : ['1,1,0,0,0'],
                'Près de Chez Nous'                             : ['3,1,9'],
                Primitivi                                       : ['3,2,0'],
                'Prix libre pour affiches/flyers/micro-éditions': ['0,6,11,1'],
                'Production en ville'                           : ['1,1,7,0'],
                'Projet ASSAB'                                  : ['3,8,3'],
                'Psycho-Social'                                 : ['1,1,0,3', '1,1,2,4', '1,1,3,3'],
                'Psychothérapie Institutionnelle'               : ['3,4,1'],
                PsySafe                                         : ['3,7,0,1'],
                'QX1 - WelcomeMap'                              : ['1,1,3,0,4', '3,1,2'],
                'Radar Squat'                                   : ['3,0,6'],
                'Radio BAM'                                     : ['3,3,3'],
                'Radio Galère (88.4)'                           : ['3,3,0'],
                'Radio Gazelle (98.0)'                          : ['3,3,2'],
                'Radio Grenouille (88.8)'                       : ['3,3,1'],
                Radios                                          : ['3,3'],
                'Radio Zinzine (Aix)'                           : ['3,3,5'],
                'Radio Zinzine (Limans)'                        : ['3,3,4'],
                "Rallumeurd'étoiles"                            : ['0,2,6'],
                Ramina                                          : ['1,1,3,0,0'],
                'Refuge | Hébergement'                          : ['0,14'],
                Regain                                          : ['1,1,8,0'],
                'Regain & Habitat Participatif Fr'              : ['3,1,14'],
                'Relève'                                        : ['3,11,0'],
                'Remèdes,Potions,Médocs'                        : ['1,1,0,1'],
                'Répertoires'                                   : ['3,7'],
                RERS                                            : ['3,13,1,2'],
                'Réseau COFOR'                                  : ['3,8,2'],
                'Réseau des Créfad'                             : ['3,8,7'],
                'Réseau des Paniers Marseillais - AMAP'         : ['3,1,0'],
                'Réseau Gestion_Mars_Conflits'                  : ['1,1,1,1', '3,8,4'],
                'Réseau Les Paniers Marseillais (AMAP)'         : ['3,8,1'],
                'Réseaux'                                       : ['3,8'],
                'Réseaux PDC'                                   : ['1,1,7,2'],
                'Résistance Aggression Publicitaire'            : ['0,6,2,4'],
                Ressources                                      : ['3'],
                'Restos | Cantines'                             : ['1,1,7,5,1', '3,9,1'],
                'Retab.fr'                                      : ['3,1,4'],
                'Rétablissement'                                : ['1,1,2,4,2,2'],
                Revenus                                         : ['1,1,6,2'],
                'Rev.Permanente13'                              : ['1,0,2,2,3', '2,0,3'],
                'Revue Charbon'                                 : ['3,5,6'],
                'Revue Rhizome'                                 : ['3,5,7'],
                'Revues | Journaux'                             : ['3,5'],
                RezoProspec                                     : ['3,8,0'],
                'Riposte Alimentaire'                           : ['1,0,9,0'],
                'Riposte Antifa'                                : ['1,0,5,1'],
                "Saccage (Anti-JO's)"                           : ['0,6,0,4'],
                "Sang D'encre"                                  : ['3,5,1'],
                'Santé'                                         : ['0,16'],
                'Santé | Soin | Thérapie'                       : ['1,1,0', '3,7,0'],
                'Savoir-faire'                                  : ['3,13'],
                'S.d.l.Terre'                                   : ['1,0,1,1'],
                'Sélection Rhizome'                             : ['3,13,0,0'],
                'Services Sociaux'                              : ['0,15', '3,14'],
                'SESL - solidaires étudiants'                   : ['0,6,4,0', '1,0,2,1,0,0', '2,1,0,0'],
                'Sexualité'                                     : ['1,1,0,2'],
                sirakadjan                                      : ['0,6,1,11'],
                SoinSoin                                        : ['3,5,0'],
                Solidaires13                                    : ['0,6,4', '1,0,2,1,0', '2,1,0'],
                'Solidaires MNA'                                : ['1,1,3,0,3'],
                'Solidarité | Entraide'                         : ['1,1,3'],
                'Solidarité Femmes 13'                          : ['1,1,0,3,8', '1,1,3,3,5'],
                SPAAM                                           : ['0,6,0,3', '1,1,2,4,1', '1,1,5,1'],
                'SPAAM-infos'                                   : ['3,6,0'],
                'SPAAM-Répertoire'                              : ['3,7,0,2'],
                Squats                                          : ['0,12'],
                'sud éduc'                                      : ['0,6,4,3', '1,0,2,1,0,3', '2,1,0,3'],
                'Super Cafoutch'                                : ['1,1,7,5,4,0', '3,9,4,0'],
                'Supermarchés'                                  : ['1,1,7,5,4', '3,9,4'],
                'Survie - PACA'                                 : ['1,0,4,0'],
                Syndicats                                       : ['1,0,2,1', '2,1'],
                'Systèmes Agricoles'                            : ['3,13,0,3'],
                'Tango Queer'                                   : ['0,6,14,2'],
                Technopolice                                    : ['0,6,0,6', '1,0,8,0'],
                'Télé Mouche'                                   : ['0,6,2,7', '3,2,1'],
                'Terre de Liens'                                : ['3,1,10'],
                'Testing children node'                         : ['0,6,2,0,0,0'],
                'Thêatre'                                       : ['1,1,2,0,0'],
                'Thêatre Autogéré DAR'                          : ['1,1,2,0,0,0'],
                "Thêatre de l'Opprimé·e LABASE"                 : ['1,1,2,0,0,1'],
                'Théâtres'                                      : ['0,1'],
                TheNobelKitchen                                 : ['1,1,7,1,1'],
                'Thérapie'                                      : ['1,1,0,0'],
                'Thérapie Transversale'                         : ['1,2,1'],
                'Tiers-Lab'                                     : ['0,9,0'],
                'Tiers-lieux'                                   : ['0,9'],
                Transat                                         : ['0,6,2,1', '1,1,0,3,10', '1,1,3,3,8'],
                'Transféminisme*'                               : ['3,4,0'],
                TransFriendly                                   : ['3,7,0,0'],
                Transiscope                                     : ['3,1,8'],
                Transit                                         : ['0,5,5'],
                'Treynas (Ardèche)'                             : ['0,10,1,4', '0,11,0,4'],
                TRUC                                            : ['3,8,6'],
                UEEH                                            : ['1,1,2,2,1', '1,1,2,3,1'],
                'Un Autre Monde'                                : ['3,5,3'],
                'UNEF-13'                                       : ['1,0,2,1,2', '2,1,2'],
                'Un monstre qui vous parle'                     : ['3,4,0,3'],
                'Un podcast à soi'                              : ['3,4,0,0'],
                'Urgence Palestine'                             : ['1,0,2,0,4', '1,0,4,1', '1,0,5,0'],
                'Vélo Sapiens'                                  : ['3,10,0,0'],
                'Vidéodrome 2'                                  : ['0,0,1', '0,6,13'],
                Visualisation                                   : ['3,12,1,0'],
                'Vivre Avec'                                    : ['1,1,2,4,2,1'],
                Wildproject                                     : ['0,5,2'],
                'XR - Extinction Rebellion'                     : ['0,6,2,2'],
                'Youth for Climate'                             : ['0,6,2,3'],
                Zads                                            : ['1,0,1,0'],
                'Zone à Patates'                                : ['1,0,1,0,0']
            },
            hierarchy_tree_data             : {
                "name"    : "Rhizome Marseille",
                "free"    : true,
                "children": [
                    {
                        "name"    : "Lieux",
                        "free"    : true,
                        "children": [

                            {
                                "name"    : "Cinémas",
                                "free"    : true,
                                "children": [

                                    {
                                        "name": "Le Polygone Étoilé",
                                        "free": true,
                                    },
                                    {
                                        "name": "Vidéodrome 2",
                                        "free": true,
                                    },
                                    {
                                        "name": "Le Gyptis",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Théâtres",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "L'Astronef",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Bars | Cafés | Salles",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Ciné Bar (ClubCoop)",
                                        "free": true,
                                    },
                                    {
                                        "name"    : "La Merveilleuse",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Jam Poèsie·Musique",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name": "El Aché de Cuba",
                                        "free": true,
                                    },
                                    {
                                        "name": "L'Embobineuse",
                                        "free": true,
                                    },
                                    {
                                        "name": "DATA",
                                        "free": true,
                                    },
                                    {
                                        "name": "LESTOCKK",
                                        "free": true,
                                    },
                                    {
                                        "name": "Rallumeurd'étoiles",
                                        "free": true,
                                    },

                                ]
                            },
                            {
                                "name"    : "Entreprises Autogérés",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "L'Après M",
                                        "free": true,
                                    },
                                    {
                                        "name": "L'Épicerie Paysanne",
                                        "free": true,
                                    },
                                    {
                                        "name": "Association Cuve",
                                        "free": true,
                                    },
                                    {
                                        "name": "Les Champignons de Marseille (?)",
                                        "free": true,
                                    },
                                    {
                                        "name": "1336 - Coop. production de thé ",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Entrerpises | Cooperatives",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "La Ruche",
                                        "free": true,
                                    },
                                    {
                                        "name": "Le Plan de A à Z",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Librairies | Bibliothèques",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Bibliothèque Alcazar",
                                        "free": true,
                                    },
                                    {
                                        "name": "Cité de l'Agriculture",
                                        "free": true,
                                    },
                                    {
                                        "name": "Wildproject",
                                        "free": true,
                                    },
                                    {
                                        "name": "Mille Bâbords",
                                        "free": true,
                                    },
                                    {
                                        "name": "La DAR",
                                        "free": true,
                                    },
                                    {
                                        "name": "Transit",
                                        "free": true,
                                    },
                                    {
                                        "name": "L'Hydre aux milles têtes",
                                        "free": true,
                                    },
                                    {
                                        "name": "Manifesten",
                                        "free": true,
                                    },
                                    {
                                        "name": "Mémoire des Sexualités",
                                        "free": true,
                                    },
                                    {
                                        "name": "L'Histoire de l'oeil",
                                        "free": true,
                                    },
                                    {
                                        "name": "Les Héroïnes",
                                        "free": true,
                                    },
                                    {
                                        "name": "CIRA",
                                        "free": true,
                                    },
                                    {
                                        "name": "DATA",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Lieux Urbains Autogérés",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Manifesten",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Manifestin",
                                                "free": true,
                                            },
                                            {
                                                "name": "MARSS",
                                                "free": true,
                                            },
                                            {
                                                "name": "Cercle de Nageurs en Eaux Troubles",
                                                "free": true,
                                            },
                                            {
                                                "name": "SPAAM",
                                                "free": true,
                                            },
                                            {
                                                "name": "Saccage (Anti-JO's)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Groupe Anti-gentrification",
                                                "free": true,
                                            },
                                            {
                                                "name": "Technopolice",
                                                "free": true,
                                            },
                                            {
                                                "name": "CQFD",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "La DAR",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Le Café des Chômheureuses",
                                                "free": true,
                                            },
                                            {
                                                "name": "Atelier du Mouvement (danse)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Atelier Théâtre",
                                                "free": true,
                                            },
                                            {
                                                "name": "accueil de jour Sirakadjan",
                                                "free": true,
                                            },
                                            {
                                                "name": "maladroite boxe populaire",
                                                "free": true,
                                            },
                                            {
                                                "name": "Atelier de Couture",
                                                "free": true,
                                            },
                                            {
                                                "name": "Bibliothèque Nulle part Ailleurs",
                                                "free": true,
                                            },
                                            {
                                                "name": "Cantine The Noble Kitchen",
                                                "free": true,
                                            },
                                            {
                                                "name": "Cantine Chaubouillante",
                                                "free": true,
                                            },
                                            {
                                                "name": "les gadjis",
                                                "free": true,
                                            },
                                            {
                                                "name": "antifa social club",
                                                "free": true,
                                            },
                                            {
                                                "name": "sirakadjan",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "La Base",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Alternatiba",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name"    : "ANV COP-21m",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Testing children node",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                ]
                                            },
                                            {
                                                "name": "Transat",
                                                "free": true,
                                            },
                                            {
                                                "name": "XR - Extinction Rebellion",
                                                "free": true,
                                            },
                                            {
                                                "name": "Youth for Climate",
                                                "free": true,
                                            },
                                            {
                                                "name": "Résistance Aggression Publicitaire",
                                                "free": true,
                                            },
                                            {
                                                "name": "Aïolibre",
                                                "free": true,
                                            },
                                            {
                                                "name": "La Roue (monnaie)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Télé Mouche",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Mille Bâbords",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "AG InterPro",
                                                "free": true,
                                            },
                                            {
                                                "name": "CNT13",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Solidaires13",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "SESL - solidaires étudiants",
                                                "free": true,
                                            },
                                            {
                                                "name": "BTP autonome",
                                                "free": true,
                                            },
                                            {
                                                "name": "AG précaires",
                                                "free": true,
                                            },
                                            {
                                                "name": "sud éduc",
                                                "free": true,
                                            },
                                            {
                                                "name": "Asso",
                                                "free": true,
                                            },
                                            {
                                                "name": "Le Social Brûle 13",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name": "Massalia VOx",
                                        "free": true,
                                    },
                                    {
                                        "name"    : "Al Manba",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Permanence Juridique",
                                                "free": true,
                                            },
                                            {
                                                "name": "Cours FLE",
                                                "free": true,
                                            }
                                        ]
                                    },
                                    {
                                        "name": "La Nebula",
                                        "free": true,
                                    },
                                    {
                                        "name"    : "Bourse du Travail",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "CGT chômeur précaires",
                                                "free": true,
                                            },
                                            {
                                                "name": "CGT spectacle",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Centre LGBTQIA+",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "G.L.A.M",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name": "Les 8 Pillards",
                                        "free": true,
                                    },
                                    {
                                        "name"    : "Imprimerie Partagée",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Gratuit pour les urgences administratives",
                                                "free": true,
                                            },
                                            {
                                                "name": "Prix libre pour affiches/flyers/micro-éditions",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "CIRA",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Causeries Mensueles",
                                                "free": true,
                                            },
                                            {
                                                "name": "Atelier Recherche Formation",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Vidéodrome 2",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Les Mains Gauches-festival queer féministe",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Le Morozoff",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Box",
                                                "free": true,
                                            },
                                            {
                                                "name": "Clownozoff",
                                                "free": true,
                                            },
                                            {
                                                "name": "Tango Queer",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Mémoire des Sexualités",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Genre de Lutte",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name": "La Déviation",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Lieux Ruraux Autogérés",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Café Villageois (Lauris)",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Lieux Publics",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Cité d'Arts de la Rue",
                                        "free": true,
                                    },
                                    {
                                        "name": "Bibliothèque Alcazar",
                                        "free": true,
                                    },
                                    {
                                        "name": "Cité des Associations",
                                        "free": true,
                                    },
                                ]

                            },
                            {
                                "name"    : "Tiers-lieux",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Tiers-Lab",
                                        "free": true,
                                    },
                                    {
                                        "name": "Les ateliers blancarde",
                                        "free": true,
                                    },
                                    {
                                        "name": "La Réserve des Arts",
                                        "free": true,
                                    },
                                    {
                                        "name": "Le Talus",
                                        "free": true,
                                    },
                                    {
                                        "name": "Le Plan de A à Z",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Laboratoires de vie",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "L'école des vivants",
                                        "free": true,
                                    },
                                    {
                                        "name"    : "Longo Maï",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Grange neuve (Forcarlquier)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Mas de Granier (Saint Martin de Crau)",
                                                "free": true,
                                            },
                                            {
                                                "name": "La Cabrery (Luberon)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Filature de Chantemerle (Hautes-Alpes)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Treynas (Ardèche)",
                                                "free": true,
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                "name"    : "Fermes",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Longo Maï",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Grange neuve (Forcarlquier)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Mas de Granier (Saint Martin de Crau)",
                                                "free": true,
                                            },
                                            {
                                                "name": "La Cabrery (Luberon)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Filature de Chantemerle (Hautes-Alpes)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Treynas (Ardèche)",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name": "La Caillasse (Cucuron)",
                                        "free": true,
                                    },
                                    {
                                        "name": "La ferme Capri",
                                        "free": true,
                                    },
                                    {
                                        "name": "Le Collet des Comtes",
                                        "free": true,
                                    },
                                    {
                                        "name": "Ferme du Roy d'Espagne",
                                        "free": true,
                                    },
                                    {
                                        "name": "Ferme Pastière (Meryrargues)",
                                        "free": true,
                                    },
                                    {
                                        "name": "Ferme de l'Etoile",
                                        "free": true,
                                    },
                                    {
                                        "name": "Le Talus",
                                        "free": true,
                                    },
                                    {
                                        "name": "La Bastide à Fruits",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Squats",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Le Snack",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "GSN Géstion de Conflits",
                                                "free": true,
                                            }
                                        ]
                                    },
                                    {
                                        "name": "La Tarantula",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Lieux de Répit",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Lieu de Répit",
                                        "free": true,
                                    },
                                    {
                                        "name": "Palama",
                                        "free": "true",
                                    },
                                ]
                            },
                            {
                                "name"    : "Refuge | Hébergement",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "L'Auberge",
                                        "free": "true",
                                    },
                                ]
                            },
                            {
                                "name"    : "Services Sociaux",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Assistance Sociale",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "CCAS",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Agence Centre|13002",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Agence Est|13004",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Agence Sud|13008",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Agence Nord|13014",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Maisons de Solidarité",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "MDS COLBERT | 01",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS DU LITTORAL | 02",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS BELLE DE MAI | 03",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS CHARTREUX | 04",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS ST SEBASTIEN | 06",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS DE BONNEVEINE | 08",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS PONT DE VIVAUX | 10",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS SAINT MARCEL | 11",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS LE NAUTILE | 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS MALPASSÉ | 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS DE LA VISTE | 15",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS DE L'ESTAQUE | 16",
                                                        "free": true,
                                                    },


                                                ]
                                            },
                                            {
                                                "name"    : "Logement",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "EAH",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                "name"    : "Santé",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Le Spot-Longchamp",
                                        "free": "true",
                                    },
                                    {
                                        "name": "LE CHÂTEAU EN SANTÉ",
                                        "free": true,
                                    },
                                ]
                            }
                        ]
                    },
                    {
                        "name"    : "Collectifs | Assos | Projets",
                        "free"    : true,
                        "children": [
                            {
                                "name"    : "Macropolitique",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Bataille Médiatique",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Journaux Indépendants",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "CQFD",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Marsactu",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "La terre en Thiers",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name": "Chaines Youtube",
                                                "free": true,
                                            },
                                            {
                                                "name": "Blogs",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Blocage",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Zads",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Zone à Patates",
                                                        "free": true,
                                                    }
                                                ]
                                            },
                                            {
                                                "name"    : "S.d.l.Terre",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Comité Local 13",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Face aux lois/politiques Gouv.",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Autonomes",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "AG-InterPro",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Marseille vs Darmanin",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "CoMob-StCharles",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Le Social Brûle 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Urgence Palestine",
                                                        "free": true,
                                                    }
                                                ]
                                            },
                                            {
                                                "name"    : "Syndicats",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name"    : "Solidaires13",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "SESL - solidaires étudiants",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "BTP autonome",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "AG précaires",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "sud éduc",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Asso",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Le Social Brûle 13",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name": "CNT13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "UNEF-13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Confédération Paysanne PACA",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Partis",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "NUPES13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "NPA13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "LFI13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Rev.Permanente13",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Face au système Financier",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Banques Alternatives",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "La NEF",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Monnaies Locales",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "La Roue (monnaie)",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Cryptomonnaies",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "La Ĝ (June)",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Face au Néo-colonialisme",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Survie - PACA",
                                                "free": true,
                                            },
                                            {
                                                "name": "Urgence Palestine",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Face au Fascisme",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Urgence Palestine",
                                                "free": true,
                                            },
                                            {
                                                "name": "Riposte Antifa",
                                                "free": true,
                                            },
                                            {
                                                "name": "Brigade Antifa",
                                                "free": true,
                                            },
                                            {
                                                "name": "Colletif Antifa",
                                                "free": true,
                                            },
                                            {
                                                "name": "Antifa Social Club",
                                                "free": true,
                                            },
                                            {
                                                "name": "Assemblées Antifa",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Face à la Psychiatrie",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Fondation ERIE",
                                                "free": true,
                                            },
                                            {
                                                "name": "AiLSi",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : " Face à la Gentrification",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Collectif Anti-Gentrification",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Face à la Surveillance",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Technopolice",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "F. au Système Alimentaire",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Riposte Alimentaire",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Internationnalisme",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "CIMK",
                                                "free": true,
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                "name"    : "Micropolitique",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Santé | Soin | Thérapie",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Thérapie",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Pour une Thérapie Transversale",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name": "Remèdes,Potions,Médocs",
                                                "free": true
                                            },
                                            {
                                                "name"    : "Sexualité",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name"    : "Contraception",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "13ticules",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Psycho-Social",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "MARSS",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "COFOR",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "JUST",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Nouvelle Aube",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name"    : "GEMs",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Lieu d'Échanges et d'Ouvertures LEO",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "CLUB PARENTHESE",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name": "Planning Familial 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "IMAJSanté",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Autres Regards",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Solidarité Femmes 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Centre LGBTQIA+",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Transat",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "LE CHÂTEAU EN SANTÉ",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Equipe SIDIIS",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Addictions",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Nouvelle Aube",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Bus 31/32",
                                                        "free": true,
                                                    }
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Médiation | Gestion de Conflits",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Collectif WD-40",
                                                "free": true,
                                            },
                                            {
                                                "name": "Réseau Gestion_Mars_Conflits",
                                                "free": true,
                                            },
                                            {
                                                "name": "AVAD",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Éducation | Formation",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Arts",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name"    : "Thêatre",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Thêatre Autogéré DAR",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Thêatre de l'Opprimé·e LABASE",
                                                                "free": true,
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Musique",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "LES RASCASSES",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "La lutte enchantée",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Atelier Palmas(€)",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Cirque",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Extrême Jonglerie",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Cinéma",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Porn on Mars festival de porn queer",
                                                                "free": true,
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "name": "Peinture",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name"    : "Cuisine",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Le Bouillon de Noailles",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Artisanat",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name"    : "Couture",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Atelier Couture (DAR)",
                                                                "free": true,
                                                            },
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                "name"    : "Politique",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "A2C-AutonomieDeClasse",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "UEEH",
                                                        "free": true,
                                                    }
                                                ]
                                            },
                                            {
                                                "name"    : "Genre & Sexualités",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name"    : "Masculinités",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Manoeuvre",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name": "UEEH",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Centre LGBTQIA+",
                                                        "free": true,
                                                    }
                                                ]
                                            },
                                            {
                                                "name"    : "Psycho-Social",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name"    : "Ateliers Nebula",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Gestion de Crises",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "SPAAM",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Écoute Active",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Hypnose",
                                                                "free": true,
                                                            },

                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Modules COFOR",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Bien Être",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Vivre Avec",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Rétablissement",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Droit",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Addictions",
                                                                "free": true,
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "name": "CRIR-AVS PACA",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Navigation",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Lounapo",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Autres",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Crefada",
                                                        "free": true,
                                                    },
                                                ]
                                            },

                                        ]
                                    },
                                    {
                                        "name"    : "Solidarité | Entraide",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Exilées",
                                                "free"    : true,
                                                "children": [

                                                    {
                                                        "name": "Ramina",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Le GR1",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name"    : "Al Manba",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Permanence Juridique",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Cours FLE",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name": "Solidaires MNA",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "QX1 - WelcomeMap",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "G.L.A.M",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "AUP - Demandeurs d'asile",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Collectif Binkadi",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name": "Administratif",
                                                "free": true
                                            },
                                            {
                                                "name"    : "Langues",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Mot de Passe (Français)",
                                                        "free": true,
                                                    }
                                                ]
                                            },
                                            {
                                                "name"    : "Psycho-Social",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "MARSS",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "COFOR",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name"    : "GEMs",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Lieu d'Échanges et d'Ouvertures LEO",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "CLUB PARENTHESE",
                                                                "free": true,
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        "name": "IMAJSanté",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "H.A.S",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Solidarité Femmes 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Centre LGBTQIA+",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Planning Familial 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Transat",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Autres Regards",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "LE CHÂTEAU EN SANTÉ",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "AVAD",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "L'Auberge",
                                                        "free": "true",
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Chomage | Precarité",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Chomheureuses",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Chomeurs Précaires 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Cultures Du Cœur 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "La Cuisine du 101",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Luttes Locales",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Cité de l'Agriculture",
                                                "free": true,
                                            },
                                            {
                                                "name": "CHO3",
                                                "free": true,
                                            },
                                            {
                                                "name": "MARSS",
                                                "free": true,
                                            },
                                            {
                                                "name": "JUST",
                                                "free": true,
                                            },
                                            {
                                                "name": "Nouvelle Aube",
                                                "free": true,
                                            },
                                            {
                                                "name": "La Roue (monnaie)",
                                                "free": true,
                                            },
                                            {
                                                "name": "Approches Cultures & Territoires",
                                                "free": true,
                                            },
                                            {
                                                "name": "AUP - Demandeurs d'asile",
                                                "free": true,
                                            },
                                            {
                                                "name": "La Cloche Sud",
                                                "free": true,
                                            },
                                            {
                                                "name": "Collectif Binkadi",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Autodéfense",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Legal Team 13",
                                                "free": true,
                                            },
                                            {
                                                "name": "SPAAM",
                                                "free": true,
                                            },
                                            {
                                                "name": "Mars'Soins",
                                                "free": true,
                                            },
                                            {
                                                "name"    : "Arts Martiaux/Box",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Maladroite BoxPop",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Box Morozoff",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Mutualisation",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Matériel/Outils",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Marsmut",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "La Cuisine du 101",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name": "Logement",
                                                "free": true,
                                            },
                                            {
                                                "name"    : "Revenus",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Mutuelle MTPGB",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name": "Ateliers",
                                                "free": true,
                                            },
                                            {
                                                "name"    : "Numérique",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Marsnet",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Bouffe",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Production en ville",
                                                "free"    : true,
                                                "children": [

                                                    {
                                                        "name": "Les Champi. de Marseille",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Association Cuve",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Le Talus",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "La ferme Capri",
                                                        "free": true,
                                                    }
                                                ]
                                            },
                                            {
                                                "name"    : "Cantines Solidaires",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "CHO3",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "TheNobelKitchen",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Cantine du Midi",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Le Bouillon de Noailles",
                                                    },
                                                    {
                                                        "name": "La Marmite Joyeuse",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "La Cuisine du 101",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Manifestin",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Casa Consolat",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Réseaux PDC",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Les Paniers Marseillais",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Casa Consolat",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Le Marché Rouge",
                                                        "free": true,
                                                    }
                                                ]
                                            },
                                            {
                                                "name": "Gestion Déchets",
                                                "free": true,
                                            },
                                            {
                                                "name"    : "Entreprises-Autog.",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "L'Après M",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "L'Épicerie Paysanne",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Association Cuve",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Les Champignons de Marseille",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Circuit Court",
                                                "free"    : true,
                                                "children": [

                                                    {
                                                        "name"    : "Épiceries",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Bar à Vrac",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "L'Épicerie Paysanne",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "La plaine fraicheur",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Adèle",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Restos | Cantines",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Cantine du Midi",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Les Ondines",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Café l'Ecomotive",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Casa Consolat",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Le Grain de Sable",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "La Marmite Joyeuse",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Boulangeries",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Le Bar à Pain",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Les Mains Libres",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Boulangerie-Café Pain Salvator",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "House of Pain",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Marchés",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Le Marché Rouge",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Marché des artisans et des producteurs bio",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Supermarchés",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Super Cafoutch",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        "name"    : "Friperies",
                                                        "free"    : true,
                                                        "children": [
                                                            {
                                                                "name": "Frip'Insertion - Libération",
                                                                "free": true,
                                                            },
                                                            {
                                                                "name": "Frip'Insertion - Capelette ",
                                                                "free": true,
                                                            },
                                                        ]
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Logement",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Regain",
                                                "free": true,
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                "name"    : "Infra-personnel",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Dispositifs Travail Singularité",
                                        "free": true,
                                    },
                                    {
                                        "name": "Thérapie Transversale",
                                        "free": true,
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        "name"    : "Orga. Politiques",
                        "free"    : true,
                        "children": [
                            {
                                "name"    : "Partis",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "LFI13",
                                        "free": true,
                                    },
                                    {
                                        "name": "EELV-PACA",
                                        "free": true,
                                    },
                                    {
                                        "name": "NPA13",
                                        "free": true,
                                    },

                                    {
                                        "name": "Rev.Permanente13",
                                        "free": true,
                                    },


                                ]
                            },
                            {
                                "name"    : "Syndicats",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Solidaires13",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "SESL - solidaires étudiants",
                                                "free": true,
                                            },
                                            {
                                                "name": "BTP autonome",
                                                "free": true,
                                            },
                                            {
                                                "name": "AG précaires",
                                                "free": true,
                                            },
                                            {
                                                "name": "sud éduc",
                                                "free": true,
                                            },
                                            {
                                                "name": "Asso",
                                                "free": true,
                                            },
                                            {
                                                "name": "Le Social Brûle 13",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name": "CNT13",
                                        "free": true,
                                    },
                                    {
                                        "name": "UNEF-13",
                                        "free": true,
                                    },
                                    {
                                        "name": "Confédération Paysanne PACA",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Fédérations",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Fédération Anarchiste",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Coalitions",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "NUPES13",
                                        "free": true,
                                    },
                                    {
                                        "name": "NFP-13",
                                        "free": true,
                                    },
                                ]
                            },
                        ]
                    },
                    {
                        "name"    : "Ressources",
                        "free"    : true,
                        "children": [
                            {
                                "name"    : "Agendas",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Le Vortex",
                                        "free": true,
                                    },
                                    {
                                        "name": "Journal Ventilo",
                                        "free": true,
                                    },
                                    {
                                        "name": "Mars Infos Autonomes",
                                        "free": true,
                                    },
                                    {
                                        "name": "Mille Bâbords",
                                        "free": true,
                                    },
                                    {
                                        "name": "Centre LGBTQIA+",
                                        "free": true,
                                    },
                                    {
                                        "name": "Approches Cultures & Territoires",
                                        "free": true,
                                    },
                                    {
                                        "name": "Radar Squat",
                                        "free": true,
                                    },
                                    {
                                        "name": "Démosphère",
                                        "free": true,
                                    },
                                    {
                                        "name": "Mobilizon",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Cartes",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Réseau des Paniers Marseillais - AMAP",
                                        "free": true,
                                    },
                                    {
                                        "name": "Carte Autogéré Rhizome",
                                        "free": true,
                                    },
                                    {
                                        "name": "QX1 - WelcomeMap",
                                        "free": true,
                                    },
                                    {
                                        "name": "Carte des Luttes - Reporterre",
                                        "free": true,
                                    },
                                    {
                                        "name": "Retab.fr",
                                        "free": true,
                                    },
                                    {
                                        "name": "Carto-Marseille",
                                        "free": true,
                                    },
                                    {
                                        "name": "DICADD",
                                        "free": true,
                                    },
                                    {
                                        "name": "FransGenre",
                                        "free": true,
                                    },
                                    {
                                        "name": "Transiscope",
                                        "free": true,
                                    },
                                    {
                                        "name": "Près de Chez Nous",
                                        "free": true,
                                    },
                                    {
                                        "name": "Terre de Liens",
                                        "free": true,
                                    },
                                    {
                                        "name": "LGBT+ PACA",
                                        "free": true,
                                    },
                                    {
                                        "name": "Habicoop",
                                        "free": true,
                                    },
                                    {
                                        "name": "Hameaux Légers",
                                        "free": true,
                                    },
                                    {
                                        "name": "Regain & Habitat Participatif Fr",
                                        "free": true,
                                    },
                                    {
                                        "name": "Les Écotables",
                                        "free": true,
                                    },
                                    {
                                        "name": "Le Carillon",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Médias",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Primitivi",
                                        "free": true,
                                    },
                                    {
                                        "name": "Télé Mouche",
                                        "free": true,
                                    },
                                    {
                                        "name": "Mars Infos Autonomes",
                                        "free": true,
                                    },
                                    {
                                        "name": "Mille Bâbords",
                                        "free": true,
                                    },
                                    {
                                        "name": "CQFD",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Radios",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Radio Galère (88.4)",
                                        "free": true,
                                    },
                                    {
                                        "name": "Radio Grenouille (88.8)",
                                        "free": true,
                                    },
                                    {
                                        "name": "Radio Gazelle (98.0)",
                                        "free": true,
                                    },
                                    {
                                        "name": "Radio BAM",
                                        "free": true,
                                    },
                                    {
                                        "name": "Radio Zinzine (Limans)",
                                        "free": true,
                                    },
                                    {
                                        "name": "Radio Zinzine (Aix)",
                                        "free": true,
                                    },
                                    {
                                        "name": "DATA",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Podcasts",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Transféminisme*",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Un podcast à soi",
                                                "free": true,
                                            },
                                            {
                                                "name": "Les Couilles sur la table",
                                                "free": true,
                                            },
                                            {
                                                "name": "Le Coeur sur la table",
                                                "free": true,
                                            },
                                            {
                                                "name": "Un monstre qui vous parle",
                                                "free": true,
                                            },
                                            {
                                                "name": "Paul B. Preciado, trans-philosophe",
                                                "free": true,
                                            },
                                            {
                                                "name": "Paul B. Preciado : trans révolutionnaire",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Psychothérapie Institutionnelle",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "De Saint-Alban à La Borde - France Culture",
                                                "free": true,
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "name"    : "Revues | Journaux",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "SoinSoin",
                                        "free": true,
                                    },
                                    {
                                        "name": "Sang D'encre",
                                        "free": true,
                                    },
                                    {
                                        "name": "Les Cahiers A2C",
                                        "free": true,
                                    },
                                    {
                                        "name": "Un Autre Monde",
                                        "free": true,
                                    },
                                    {
                                        "name": "CQFD",
                                        "free": true,
                                    },
                                    {
                                        "name": "La terre en Thiers",
                                        "free": true,
                                    },
                                    {
                                        "name": "Revue Charbon",
                                        "free": true,
                                    },
                                    {
                                        "name": "Revue Rhizome",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "InfoKiosques",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "SPAAM-infos",
                                        "free": true,
                                    },
                                    {
                                        "name": "Folie et Politique -Barge",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Répertoires",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Santé | Soin | Thérapie",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "TransFriendly",
                                                "free": true,
                                            },
                                            {
                                                "name": "PsySafe",
                                                "free": true,
                                            },
                                            {
                                                "name": "SPAAM-Répertoire",
                                                "free": true,
                                            }
                                        ]
                                    },
                                    {
                                        "name": "Juridique",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Réseaux",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "RezoProspec",
                                        "free": true,
                                    },
                                    {
                                        "name": "Réseau Les Paniers Marseillais (AMAP)",
                                        "free": true,
                                    },
                                    {
                                        "name": "Réseau COFOR",
                                        "free": true,
                                    },
                                    {
                                        "name": "Projet ASSAB",
                                        "free": true,
                                    },
                                    {
                                        "name": "Réseau Gestion_Mars_Conflits",
                                        "free": true,
                                    },
                                    {
                                        "name": "Le Marché Rouge",
                                        "free": true,
                                    },
                                    {
                                        "name": "TRUC",
                                        "free": true,
                                    },
                                    {
                                        "name": "Réseau des Créfad",
                                        "free": true,
                                    },
                                    {
                                        "name": "Habicoop",
                                        "free": true,
                                    },
                                    {
                                        "name": "Actions Communes",
                                        "free": true,
                                    },
                                ]
                            },
                            {
                                "name"    : "Circuit Court",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Épiceries",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Bar à Vrac",
                                                "free": true,
                                            },
                                            {
                                                "name": "L'Épicerie Paysanne",
                                                "free": true,
                                            },
                                            {
                                                "name": "La plaine fraicheur",
                                                "free": true,
                                            },
                                            {
                                                "name": "Adèle",
                                                "free": true,
                                            }
                                        ]
                                    },
                                    {
                                        "name"    : "Restos | Cantines",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Cantine du Midi",
                                                "free": true,
                                            },
                                            {
                                                "name": "Les Ondines",
                                                "free": true,
                                            },
                                            {
                                                "name": "Café l'Ecomotive",
                                                "free": true,
                                            },
                                            {
                                                "name": "Casa Consolat",
                                                "free": true,
                                            },
                                            {
                                                "name": "Le Grain de Sable",
                                                "free": true,
                                            },
                                            {
                                                "name": "La Marmite Joyeuse",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Boulangeries",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Le Bar à Pain",
                                                "free": true,
                                            },
                                            {
                                                "name": "Les Mains Libres",
                                                "free": true,
                                            },
                                            {
                                                "name": "Boulangerie-Café Pain Salvator",
                                                "free": true,
                                            },
                                            {
                                                "name": "House of Pain",
                                                "free": true,
                                            }
                                        ]
                                    },
                                    {
                                        "name"    : "Marchés",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Le Marché Rouge",
                                                "free": true,
                                            },
                                            {
                                                "name": "Marché des artisans et des producteurs bio",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Supermarchés",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Super Cafoutch",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Friperies",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Frip'Insertion - Libération",
                                                "free": true,
                                            },
                                            {
                                                "name": "Frip'Insertion - Capelette ",
                                                "free": true,
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                "name"    : "Ateliers",
                                "free"    : true,
                                "children": [

                                    {
                                        "name"    : "Ateliers Vélo",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Vélo Sapiens",
                                                "free": true,
                                            },
                                            {
                                                "name": "Collectif Vélos en Ville",
                                                "free": true,
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                "name"    : "Festivals",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Relève",
                                        "free": true,
                                    }
                                ]
                            },
                            {
                                "name"    : "Outils",
                                "free"    : true,
                                "children": [
                                    {
                                        "name": "Calculs Mutuelle",
                                        "free": true,
                                    },
                                    {
                                        "name"    : "Numériques",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Visualisation",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "LiveGap Charts",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Graph Maker",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                "name"    : "Savoir-faire",
                                "free"    : true,
                                "children": [

                                    {
                                        "name"    : "Autonomie",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "Sélection Rhizome",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Le moteur stirling",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Mouche Soldat Noir",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Aquaponie",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Biodigesteur",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Pompe à bélier",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name": "Énergie",
                                                "free": true,
                                            },
                                            {
                                                "name": "Compostage",
                                                "free": true,
                                            },
                                            {
                                                "name": "Systèmes Agricoles",
                                                "free": true,
                                            },
                                            {
                                                "name": "Pompes à eau",
                                                "free": true,
                                            },
                                        ]
                                    },
                                    {
                                        "name"    : "Pédagogie",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name": "Freinet",
                                                "free": true,
                                            },
                                            {
                                                "name": "Pédagogie Institutionnelle",
                                                "free": true,
                                            },
                                            {
                                                "name": "RERS",
                                                "free": true,
                                            },
                                        ]
                                    },
                                ]
                            },
                            {
                                "name"    : "Services Sociaux",
                                "free"    : true,
                                "children": [
                                    {
                                        "name"    : "Assistance Sociale",
                                        "free"    : true,
                                        "children": [
                                            {
                                                "name"    : "CCAS",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "Agence Centre|13002",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Agence Est|13004",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Agence Sud|13008",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "Agence Nord|13014",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                            {
                                                "name"    : "Maisons de Solidarité",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "MDS COLBERT | 01",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS DU LITTORAL | 02",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS BELLE DE MAI | 03",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS CHARTREUX | 04",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS ST SEBASTIEN | 06",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS DE BONNEVEINE | 08",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS PONT DE VIVAUX | 10",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS SAINT MARCEL | 11",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS LE NAUTILE | 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS MALPASSÉ | 13",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS DE LA VISTE | 15",
                                                        "free": true,
                                                    },
                                                    {
                                                        "name": "MDS DE L'ESTAQUE | 16",
                                                        "free": true,
                                                    },


                                                ]
                                            },
                                            {
                                                "name"    : "Logement",
                                                "free"    : true,
                                                "children": [
                                                    {
                                                        "name": "EAH",
                                                        "free": true,
                                                    },
                                                ]
                                            },
                                        ]
                                    },
                                ]
                            },
                        ]
                    }
                ]
            }
        };
    }
}