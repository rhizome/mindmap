const entities = {
    'Rhizome Marseille'                 : {
        name       : 'Rhizome Marseille',
        description: "🧞‍♀️ Plateforme en développement <br><br> Rhizome·13 est un projet politique transversal en développement, ayant comme but de <ul><li> Mettre à disposition des espaces de travail hebdomadaires autour de thèmes essentiels : <i>gestion de conflits et violences, logement, thérapie, moyens d’échange, modes d’organisation, modes de production, distribution et consommation, l'école, l’amour (amitié, amour amoureux, famille), l’éducation des enfants, la contraception…</i> L’idée étant de former des groupes de travail mutants, potentiellement éphémères, sans engagement (je viens quand je veux), animés par le désir d’agir et non par l’idéologie ou par une morale militante qui juge et culpabilise, en partant du principe que nous ne sommes pas et que nous ne cherchons pas nécessairement à être d’accord, en faisant du dissensus un terreau fertile : accueil de la différence, culture de la convergence, respect des singularités. Ces groupes mutants laisseront une trace de leur travail afin de ne pas avoir à réinventer l’eau tiède chaque fois qu’un groupe se forme autour du même sujet : liste de ressources, idées développées, projections, expérimentations, barrières, blocages, conflits, solutions… Tout cela sera réellement accessible, littéralement, à tout le monde.</li><li>Articuler ce que nous considérons comme les trois différents « terrains » d’action pour la transformation sociale. Le niveau macro-politique concerne les grands ensembles humains : villes, régions, pays et monde entier. C’est là que l’on agit lorsqu’on s’oppose à une politique gouvernementale ou lorsqu’on manifeste pour un cessez-le-feu à Gaza. Le niveau micro-politique, lui, désigne tout ce qui nous est le plus immédiat : les relations de voisinage, de famille, d’amitié, d’amour, nos collectifs, coopératives et associations, nos réseaux, nos projets collectifs et individuels, la gestion de conflits, etc. Aux niveaux précédents, nous voulons articuler sérieusement la dimension que nous appelons « nano-politique » ou « infra-personnelle », concernant la société à « l’intérieur » de soi : nous sommes traversé·es par la société, même  dans les endroits les plus intimes. Le travail politique à ce niveau consiste à déconstruire et transformer les « logiciels » socialement produits qui opèrent dans nos cœurs, nos tripes et nos cerveaux. Même si l’on partage un ensemble de constructions sociales et d’affects, ce travail est nécessairement singulier, chaque personne étant comme un territoire où les montagnes et les vallées, les rivières et les rochers, les entrées et les sorties, les cloisonnements et les rigidités, sont singulièrement disposés, agencés. Les méthodes et les pratiques ne sont pas les mêmes que celles que l’on emploierait pour agir vis-à-vis du macro ou du micro-politique. Chacun·e son territoire, et pour chaque territoire une géopolitique émancipatrice singulière. Le travail individuel à ce niveau est très limité, d’une part parce que l’on a toujours besoin d’un point de vue/regard/analyse extérieure, et d’autre part en raison d’un manque généralisé de sensibilités et compétences nécessaires pour ce type de travail. Le travail collectif à ce niveau dépendra de notre capacité, d’une part, à cultiver ces sensibilités-compétences, et d’autre part, à mettre en place des dispositifs dédiés spécifiquement à ce type d’accompagnement hautement politique, individuellement et socialement thérapeutique, où l’identification et la catalysation des singularités sont essentielles. Nous considérons et soulignons que la production de nouveaux possibles, la guérison sociale et culturelle, passe nécessairement par une articulation sérieuse d'un travail sur ces trois niveaux. Dès le moment où l’on néglige l’un ou plusieurs d’entre eux, comme c’est globalement le cas concernant l’infra-personnel, nous nous mettons « hors-jeux », nous faisons bouffer par le système, finissons par le nourrir et le reproduire tout en luttant contre et le dénonçant. </li><li> Dans la continuité des points précédents, il nous semble important de focaliser notre attention en particulier sur la sociothérapie : favoriser des espaces qui nous permettraient collectivement d'identifier les mécanismes de défense et les biais cognitifs, de cultiver et raffiner les sensibilités psycho-sociales, travailler le rapport à l’altérité, à l’inconscient, au corps, au temps, à la finitude, à l’erreur… Et plus globalement : comment concevoir et pratiquer la thérapie sans dissocier nos traumatismes et nos névroses de l'ensemble des institutions et constructions pathogènes de nos sociétés ? C'est ce que nous proposons d'explorer.</li><li> A l'aide d'une plateforme numérique en cours de développement (linktr.ee/rhizome13), nous visons à catalyser l’articulation et la mise en réseau de celleux qui œuvrent dans le sens de la « justice » et de la transformation sociale. Pour l'instant exclusive à la ville de Marseille et alentours, cette plateforme serait collaborative et permettrait d'héberger un répertoire de lieux, projets, collectifs et associations, de mettre en commun des ressources de toutes sortes, de publier instantanément des événements, annonces ou articles, d'organiser des forums de discussion et éventuellement de proposer des outils pour la gestion des communs.</li><li> L’ensemble des pratiques évoquées dans les points précédents – au cœur desquelles il y aurait une remise en question permanente ; analyse et méta-analyse par le prisme du devenir plutôt que l’être – si elles prennent vie, elles feront de Rhizome·13 une école-laboratoire. À commencer par la plus petite échelle, nous voulons bricoler une école qui soit à la hauteur des enjeux de notre époque, en explorant méticuleusement le lien entre nos affects et nos institutions, entre l’inconscient et le pouvoir, entre le capitalisme, le fascisme et la névrose, entre la transformation sociale et la thérapie. Pratique de la théorie, philosophie de l’action militante : expérimenter sur le fond comme sur la forme, confronter les fins et les moyens, apprendre à identifier les rigidités, les cloisonnements, les accrochages ainsi que les mécanismes de défense, les biais cognitifs et les dynamiques inconscientes – là où le pouvoir se cache, se déguise, se voile – bref, tout ça peut se résumer en un mot : transversalité. Ce sera là l’esprit de cette école-laboratoire, faire danser la transversalité au son de l’écosophie: l’art d’Habiter. Ne plus traiter séparément le mental, le social et l’environnemental, identifier les dynamiques et les rapports écologiques à ces trois niveaux, et surtout comprendre leur intrication.</li></ul>",
        url        : 'https://rhizomiser.org'
    },
    Lieux                               : {
        name       : 'Lieux',
        description: 'Infos par lieu',
        url        : undefined
    },
    Associations                        : {
        name       : 'Associations',
        description: '',
        url        : ''
    },
    'Cité des Associations'             : {
        name       : 'Cité des Associations',
        description: "📍93 La Canebière|13001<br><br>C’est au sein de Marseille, ville qui concentre le plus d’associations, que se trouve la Cité des Associations, plus communément appelée « Maison des Associations ». Située sur la Canebière, c’est un lieu privilégié de rencontres et d’échanges qui existe depuis plus de 20 ans et accueille et soutient un grand nombre d’associations officielles (au statut déposé en préfecture).<br><br>C’est un siège social qui sert d’adresse et même de boîte aux lettres aux associations. De plus, elle leur permet un accès à des salles d’exposition, à des ordinateurs, aux poste PAO et à l’atelier vidéo. Si vous êtes adhérent à une des associations qui y siègent, les locaux offrent des salles spacieuses, avec tables et chaises. C’est un endroit de rencontres où l’on croise de nombreuses personnes aux intérêts différents.<br><br>Si vous cherchez à créer votre association, la cité prodigue des conseils, et vous aide à développer vos services au public.De plus, elle organise régulièrement des événements et des conférences.",
        url        : 'https://maisondesassociations.marseille.fr/'
    },
    "Cité de l'Agriculture"             : {
        name       : "Cité de l'Agriculture",
        description: 'Au 6 square Stalingrad - Laboratoire pour la transition écologique des villes',
        url        : 'http://www.cite-agri.fr/le-fonds-documentaire/'
    },
    "Lieux Publics"             : {
        name       : "Lieux Publics",
        description: "Financés/gérés par la ville / la région / l'état",
        url        : ''
    }, 
    "Assistance Sociale"             : {
        name       : "",
        description: "",
    },
    "Services Sociaux"             : {
        name       : "",
        description: "",
    },  
    "CCAS"             : {
        name       : "Centres Communaux d'Action Sociale",
        description: "Les CCAS (centres communaux d’action sociale) sont en charge de l’action sociale dans les communes. Lorsqu’il n’y a pas de CCAS, notamment dans les communes de moins de 1500 habitants, c’est directement à la mairie qu’il convient de s’adresser ou à l’intercommunalité si elle dispose d’un centre intercommunal d’action sociale (CICAS).<br><br><h3>Quelles sont les missions du CCAS ?</h3>Le CCAS a pour mission de soutenir les habitants de la commune, notamment les personnes âgées et leurs proches aidants.<br><br>Il attribue des aides financières : <ul><li>secours d’urgence, </li><li>aide en nature,</li><li>prêts sans intérêt,</li><li>aide alimentaire,</li><li>chèques d’accompagnement personnalisé…</li></ul>Il gère des établissements d’hébergement pour personnes âgées ou des services à domicile.<br><br>Il met en œuvre des actions d’animation ou de soutien : lutte contre l’isolement, maintien du lien social, accès à la culture et aux loisirs, actions de prévention, adaptation du logement…<br><br> Ses principaux domaines d’actions concernent : <ul><li>la lutte contre l’exclusion et l’accès aux droits,</li><li>le soutien aux seniors,</li><li>le soutien au logement et à l’hébergement,</li><li>la petite enfance, enfance, jeunesse, </li><li>le soutien aux personnes en situation de handicap.</li></ul><h3>Pour quelles démarches s’adresser au CCAS ?</h3>Vous pouvez vous adresser au CCAS de votre commune pour les demandes suivantes :<ul><li> l’information sur les droits, les aides et les prestations sociales,</li><li>l’orientation vers le bon interlocuteur en fonction des besoins,</li><li>l’aide aux démarches pour constituer une demande d’APA (allocation personnalisée d’autonomie),</li><li>la dépôt d’une demande d’ASH (aide sociale à l’hébergement),</li><li>les demandes d’aides extralégales.",
        url        : "https://www.pour-les-personnes-agees.gouv.fr/preserver-son-autonomie/a-qui-s-adresser/le-centre-communal-d-action-sociale-ccas-la-mairie"
    }, 
    "Agence Centre|13002"             : {
        name       : "CCAS | 1, 2, 3 & 7ème",
        description: "📍62, rue de l'Évêché | Panier<br> 📞04 91 99 14 60<br>🕐 ouvertes du lundi au vendredi (sauf jours fériés) de 8h30 à 11h45 et de 12h45 à 16h30.",
    }, 
    "Agence Est|13004"             : {
        name       : "CCAS | 4,5,6 & 12ème",
        description: "📍 36, bd de la Fédération | 13004<br>📞 04 88 15 19 26<br>🕐 ouvertes du lundi au vendredi (sauf jours fériés) de 8h30 à 11h45 et de 12h45 à 16h30.",
    }, 
    "Agence Sud|13008"             : {
        name       : "CCAS | 8, 9, 10 & 11ème",
        description: "📍 11, rue Borde | 13008 <br> 📞 04 86 94 40 30 <br>🕐du lundi au vendredi, de 9h à 12h30 et de 13h30 à 18h <br><br>",
    }, 
    "Agence Nord|13014"             : {
        name       : "CCAS | 13, 14, 15 & 16ème",
        description: "📍 151, bd Danielle Casanova | 13014 <br>📞 04 84 52 51 61 <br> 🕐du lundi au vendredi, de 9h à 12h30 et de 13h30 à 18h <br><br>",
    },
    "EAH"             : {
        name       : "Espace Accompagnement Habitat",
        description: "L'Espace accompagnement habitat accueille, informe et accompagne le public du territoire dans tous les domaines relatifs au logement, à l'amélioration de l'habitat, à l'habitat durable et à la rénovation énergétique.<br><br>Ce guichet unique regroupe, dans un même lieu, les services des institutions et organismes en charge des questions d'habitat pour répondre rapidement et efficacement à un large public (locataires, propriétaires occupants et propriétaires bailleurs). Un service à l’habitant axé sur les besoins des ménages.<h3>7 pôles d'accueil</h3> Dès l'accueil, le visiteur, locataire ou propriétaire, est orienté en fonction de ses besoins, vers l'un des pôles identifiés au sein de l'Espace accompagnement habitat. <h4>Prévention habitat</h4><ul><li>Signalement des logements présumés non décents </li><li>Guichet d'autorisation préalable de louer, pour tous les logements situés dans le périmètre de Noailles</li></ul><h4> Informations juridiques</h4><ul><li>Information juridique, financière et fiscale, prévention des expulsions locatives, accès aux droits, accompagnement des nouveaux copropriétaires… </li><li>Aide à la résolution des conflits via la médiation</li></ul><h4> Accompagnement social</h4><ul><li> Accompagnement social des ménages prioritaires, accompagnement personnalisé pour les salariés en lien avec les organismes dédiés </li></ul><h4>Accession sociale à la propriété</h4><ul><li>Dispositifs existants d'accession sociale</li></ul><h4>Demande de logement social</h4><ul><li>Accueil et information des demandeurs de logement social, enregistrement de leur demande dans le fichier du Système National d'enregistrement, centralisé à l'échelle métropolitaine…</li><li>Délivrance du numéro unique départemental de la demande de logement social</li></ul><h4>Aides aux locataires</h4><ul><li>Instruction des dossiers de caution Visale (besoin d'un garant pour louer un logement) accordée par Action Logement, pour les locataires (jeunes de moins de 30 ans, salariés en situation de mobilité professionnelle ou en difficulté) et les propriétaires pour faire des travaux…</li></ul><h4>Travaux de réhabilitation et rénovation énergétique du logement</h4><ul><li>Information et accompagnement pour la rénovation du parc privé (aides à la réhabilitation, à la rénovation énergétique des copropriétés et aides financières de l'Agence nationale de l'habitat – Anah)…</li></ul><br>Du lundi au vendredi de 9h à 12h et de 13h à 16h45<br> Téléphone : 04 95 09 58 58<br>eah@ampmetropole.fr",
        url        : "https://www.marseille.fr/logement-urbanisme/logement/espace-accompagnement-habitat"
    }, 
    "Cité d'Arts de la Rue"             : {
        name       : "Cité d'Arts de la Rue",
        description: "📍225 avenue Ibrahim Ali / Aygalades | 13015 <br><br> La Cité des arts de la rue est un lieu de création et d’expérimentation dédié aux arts de la rue en espace public, coordonné par Lieux publics, centre national des arts de la rue et de l'espace public et pôle européen de création, et réunissant une dizaine de structures dédiées à l’espace public dans les quartiers nord de Marseille.<br><br> La Cité des arts de la rue est un laboratoire de fabrique, un lieu de travail, de construction, de répétition, de réflexion, de formation… 36 000 m2 dédiés à la création de spectacles pour l’espace public. Le lieu se compose d’espaces de répétition intérieurs et extérieurs, d’ateliers de constructions, de rues, parkings, escaliers… En contrebas de la Cité, le fleuve des Aygalades et son saut de 9m !",
        url        : 'https://www.lacitedesartsdelarue.net/'
    },
    JUST                                : {
        name       : 'JUST',
        description: "Justice et Union pour la Transformation Sociale. L'objectif de JUST est de développer et promouvoir en France et à l'international des expérimentations et actions qui permettent une transformation sociale vers plus de justice sociale en s'appuyant sur la participation active des personnes concernées, en réunissant autant que faire se peut des compétences universitaires, médicales, sociales, d'expérience vécue. Une des règles fondamentales que nous posons comme principe de JUST est que les expérimentations et les actions seront menées avec une participation significative des personnes dites « exclues » ou « vulnérables ».",
        url        : 'https://just.earth/'
    },
    'Lieu de répit'                     : {
        name       : 'Lieu de répit',
        description: "Un projet expérimental d'alternative à l'hospitalisation psychiatrique",
        url        : 'https://assojust.cargo.site/Lieu-de-repit'
    },
    ODAMARS                             : {
        name       : 'ODAMARS',
        description: 'Open Dialogue À MARSeille',
        url        : 'https://just.earth/ODAMARS'
    },
    Sindiane                            : {
        name       : 'Sindiane',
        description: 'Programme en santé communautaire : prévention du psychotraumatisme',
        url        : 'https://just.earth/Sindiane'
    },
    'En passant par les calanques'      : {
        name       : 'En passant par les calanques',
        description: "Développement d'actions fondées sur l'intervention psychosociale par la nature et l'aventure",
        url        : 'https://just.earth/En-passant-par-les-calanques'
    },
    'Les régisseurs.ses sociaux'        : {
        name       : 'Les régisseurs.ses sociaux',
        description: 'Sécurisation et sanitarisation des lieux de vie habités par nécessité',
        url        : 'https://just.earth/Les-regisseurs-ses-sociaux'
    },
    'Nouvelle Aube'                     : {
        name       : 'Nouvelle Aube',
        description: "Nouvelle Aube est un groupe d'auto-support, d'action, d'expérimentation, de réflexion, de recherche, de représentation, de témoignage. Notre action a pour objet la prévention, la Réduction Des Risques et des dommages ainsi que la promotion de la santé auprès d'un public jeune, fragilisé, stigmatisé, vivant en squat en rue, en abri et en prison, exposé notamment à la transmission du VIH, des hépatites, des Infections Sexuellement Transmissibles et à l'usage de produits psycho-actifs.",
        url        : 'https://assonouvelleaube.wordpress.com/'
    },
    'Autres Regards'                    : {
        name       : 'Autres Regards',
        description: 'Association de santé communautaire avec et pour les travailleurs et travailleuses du sexe <br><br> 📍 3, rue de Bône | 13005 <br>✉️ contact@autresregards.org<br>📞 04 91 42 42 90<br><br>',
        url        : 'https://autresregards.org/'
    },
    'AIDES à Marseille'                 : {
        name       : 'AIDES à Marseille',
        description: "Permanence accueil et soutien de personnes vivant avec le VIH et/ou les hépatites virales  🕛 Tous les mercredis (15h – 18h). Dépistage TROD VIH, hépatites B (VHB) et C (VHC) dans la rue, et distribution de matériels 🕛 Tous les jeudis (10h – 17h)📍Métro Réformés- Cannebière 🕛 Tous les vendredis (14h – 17h)📍Porte d'Aix 🕛 Les mardis, 1 fois sur deux (10h – 13h)📍Place des Marseillaises 🕛 Les lundis, 1 fois sur deux (14h – 17h)📍Quartier Belsunce ",
        url        : 'https://www.aides.org/le-reseau-aides'
    },
    'Solidarité Réhab.'                 : {
        name       : 'Solidarité Réhab.',
        description: "Solidarité Réhabilitation est une association sans but lucratif créée à Marseille il y a 20 ans, afin de venir en aide aux personnes souffrant d'un trouble psychique et à leurs familles. Solidarité Réhabilitation, c'est l'idée de promouvoir ce qui permet d'innover en matière de soins et d'aide pour les personnes souffrant de troubles psychiques. C'est mettre en commun le savoir des usagers, des aidants et des professionnels.",
        url        : 'https://www.solidarite-rehabilitation.org/association-solidarite-rehabilitation/'
    },
    'Solidarité Femmes 13'              : {
        name       : 'Solidarité Femmes 13',
        description: "📍146 rue Paradis|13006<h4>Accompagne les femmes, et leurs enfants, victimes de violences conjugales ou sexuelles dans les Bouches-du-Rhône</h4><ul><li>Accueil et Écoute</li><li>Hébergement</li><li>Accès au Logement</li><li>Psychologie et Art Thérapie</li><li>Pôle formation et animation de réseaux</li></ul> ☎️ Appel d'urgence au 3919 ⏰ Lundi - Vendredi --» 8h - 22h ⏰ Jours fériés/weekends --» 10h - 20h. 📞 Permanence Téléphonique : 04.91.24.61.50 ⏰  10h - 12h30, 13h30 - 17h",
        url        : 'https://solidaritefemmes13.org/'
    },
    Lounapo                             : {
        name       : 'Lounapo',
        description: "L'association Lounapo (L'ouvroir des Navigations Potentielles) a pour objet d'organiser des actions collectives autour de la mer et de la voile. <br><br>📍36 Rue Bernard 13003<br> 📞 0602651426<br> ✉️assolounapo@gmail.com<br><br>",
        url        : 'https://sangdencre.nouvelleaube.org/lounapo-lacces-a-la-navigation-pour-tous/'
    },
    Crefada                             : {
        name       : 'Crefada',
        description: '📍 1 rue Mongrand | 13006 <br><br> Le Crefada est une association d’éducation populaire qui s’inscrit dans l’histoire du mouvement de Peuple et Culture et du Réseau des Crefad. Il défend des valeurs d’apprentissage, d’émancipation et de transmission tout au long de la vie.<br><br>Créée en 2017, le Centre de Recherche, d’Etudes et de Formation à l’Animation, au Développement et à l’Autonomie (CREFADA) est né du désir de partager et de transmettre des savoirs et savoir-faire qui comptent pour nous. Nous sommes en particulier très attachées à la critique des inégalités et à la lutte contre leur reproduction. Notre perspective en la matière est intersectionnelle : elle comprend les situations de domination comme le résultat d’une simultanéité entre les discriminations de sexe, race et classe, validisme, âgisme, etc. Nous construisons nos propositions en nous référant à différentes approches et perspectives issues des sciences humaines et sociales.<br><br>Les fondatrices sont fortes de leurs expériences dans plusieurs espaces associatifs : lieux de diffusion et de création artistique, cafés associatifs, projets d’économie sociale et solidaire, projets paysans. Elles souhaitent partager leurs expériences et répondre aux besoins exprimés par les acteurs/trices de terrain, qu’ils soient d’ordre théoriques ou pratiques.<br><br>Par la transmission de l’entraînement mental (EM), le Crefada met au cœur de sa pratique la méthodologie de la pensée et de l’action dans la complexité. L’EM vise à accompagner, former, outiller des collectifs et des individu-e-s à concevoir et mener des actions. C’est aussi une méthode qui favorise et soutient le désir d’autoformation de chacun.e et permet de réfléchir à nos pratiques et  habitudes quotidiennes.',
        url        : 'https://crefada.org/'
    },
    'Bars | Cafés | Salles'             : {
        name       : 'Bars | Cafés | Salles',
        description: 'Associatif·ves, Cooperatives, Pirates',
        url        : undefined
    },
    'Ciné Bar (ClubCoop)'               : {
        name       : 'Ciné Bar (ClubCoop)',
        description: '<b>@clubcoopmarseille</b><br>📍 11 Rue Curiol | 13001<br><br>',
        url        : 'https://linktr.ee/clubcoopmarseille'
    },
    'La Merveilleuse'                   : {
        name       : 'La Merveilleuse',
        description: 'Musique live et baby foot',
        url        : 'https://www.facebook.com/lamerveilleusebar?locale=fr_FR'
    },
    'Jam Poèsie·Musique'                : {
        name       : 'Jam Poèsie·Musique',
        description: 'Tous les premiers dimanches de chaque mois',
        url        : undefined
    },
    'El Aché de Cuba'                   : {
        name       : 'El Aché de Cuba',
        description: "Café Culturel et Citoyen Expo, Spectacles, Assemblées et Conférences, Jeux de Société Vide dressing, Disques, K7 Audio, Vidéo, Livres et Objets d'Occasion",
        url        : 'https://www.facebook.com/p/El-ACHE-De-CUBA-100066515022419/?locale=fr_FR'
    },
    "L'Embobineuse"                     : {
        name       : "L'Embobineuse",
        description: "Théâtre de Fortune Hypersonic, élevage intensif d'individus collectifs de mauvais goûts pluriels",
        url        : 'https://www.lembobineuse.biz/'
    },
    DATA                                : {
        name       : 'DATA',
        description: '📍44 Rue des Bons Enfants | 13006 <br><br> Médiathèque alternative, autogérée',
        url        : 'https://datamediatheque.org/'
    },
    LESTOCKK                            : {
        name       : 'LESTOCKK',
        description: '📍52 rue Lorette | 13002 <br><br> Lestockk est un lieu alternatif autogéré qui vise à mettre en avant le travail photographique/artistique de jeunes photographes/artistes émergeant.e.s Marseillais.e.s et d’ailleurs.',
        url        : 'https://www.instagram.com/lestockk/'
    },
    "Rallumeurd'étoiles"                : {
        name       : "Rallumeurd'étoiles",
        description: 'Martigues',
        url        : 'https://www.rallumeurdetoiles.com/'
    },
    'Entreprises Autogérés'             : {
        name       : 'Entreprises Autogérés',
        description: '',
        url        : undefined
    },
    "L'Après M"                         : {
        name       : "L'Après M",
        description: 'Mcdo reprit par les travailleureuses',
        url        : 'https://www.facebook.com/lapres.m'
    },
    "L'Épicerie Paysanne"               : {
        name       : "L'Épicerie Paysanne",
        description: '71 rue Léon Bourgeaois, 13001 Marseille - Une SCOP qui propose des produits locaux et de saison gérée par les salarié.e.s !!!',
        url        : 'https://www.facebook.com/p/Epicerie-Paysanne-de-quartier-100063856110664/?locale=fr_FR'
    },
    'Association Cuve'                  : {
        name       : 'Association Cuve',
        description: 'CUVE est un projet associatif de vinification vivante et urbaine, à Marseille.',
        url        : 'https://www.facebook.com/p/CUVE-100064701725296/'
    },
    'Le Plan de A à Z'                  : {
        name       : 'Le Plan de A à Z',
        description: "Tiers-lieu culinaire,solidaire&anti-gaspi Ouvert 7/7j - 10/23h Cantine midi L au V 12/14h30 | Brunch Sam&Dim 12/15h | Miam le soir > 117 La Canebière",
        url        : 'https://www.instagram.com/leplanduplan/'
    },
    'Le Bouillon de Noailles'           : {
        name       : "Le Bouillon de Noailles",
        description: "Le Bouillon de Noailles, association d'intérêt général, porte des actions d'insertion par l'activité professionnelle, d'information, d'expérimentation, des projets culturels dans le domaine fédérateur de l'alimentation et de la cuisine.<br><br>Les projets sont conçus et menés avec les habitants et les partenaires du centre-ville. Le Bouillon de Noailles mobilise des voisins, des étudiants et des professionnels et collecte des fonds pour que ces idées voient le jour et nourrissent le quotidien de chacun.<br><br>Le Bouillon de Noailles gère 3 cuisines sociales :<ul><li> Cuisine & découvertes, des ateliers de cuisine, de sensibilisation à une alimentation équilibrée et durable, des repas partagés, à Noailles et dans tout Marseille</li><li><b>L'Espace des enfants autonomes</b>, une cuisine à Noailles favorisant les apprentissages des jeunes de 6 à 11 ans</li><li><b>La Cuisine du 101</b>, à St Mauront, une cuisine collective pour celles et ceux qui n'en ont pas, un tiers-lieu socioculturel pour le quartier</li><li>Nous œuvrons également à la création de la <b>Cuisine de l'Arc</b>, lieu socio-culinaire à Noailles.</li></ul>Le Bouillon de Noailles anime également :<ul><li><b>Le Bon Bouillon</b>, une contribution à une alimentation saine et choisie pour toutes et tous</li><li><b>Ingrédient</b>, une revue participative de rencontres culinaires</li></ul>",
        url        : "https://www.lebouillondenoailles.fr/",
    },
    'Cuisine'                           : {
        name       : "Cuisine",
        description: "",
        "url"      : "",
    },
    'La Cuisine 101'                    : {
        name       : "La Cuisine 101",
        description: "📍12 rue des Cigarières | 13003 <br><br> Une cuisine ouverte pour ceux qui n'en ont pas et un accompagnement social à dimension culturelle <br> La Cuisine du 101 est un lieu co-construit par ?<i>SoliHa Provence</i>, <i>Nexity Non-Profit</i> et le <i>Bouillon de Noailles</i>, ouvert aux personnes hébergées à l’hôtel ou en foyer, et à la rue. Elles peuvent apporter leurs ingrédients, cuisiner, emporter ou se restaurer sur place dans un cadre quasi domestique. Un accompagnement culinaire et social y est proposé. Des animateurs et animatrices proposent des ateliers de cuisine, de création artistique, également accessibles aux associations et écoles du quartier.<br><br> Ce projet, financé par la Direction Interministérielle à l’Hébergement et à l’Accès au Logement (DIHAL), vise à participer à la lutte contre la précarité alimentaire. Il s’inscrit dans les principes de solidarité et de dignité, et vise à promouvoir l’émancipation, le renforcement du lien social et la santé par l’alimentation. <br><br><b>Horaires : les mardis, jeudis, vendredis et samedis de 11h à 18h </b> <br>@lacuisinedu101",
        "url"      : "https://www.lebouillondenoailles.fr/page/1703722-atelier-101-cuisine-ouverte",
    },
    'Les Champignons de Marseille (?)'  : {
        name       : 'Les Champignons de Marseille (?)',
        description: 'à vérifier',
        url        : 'https://www.facebook.com/champignons.marseille/?locale=fr_FR'
    },
    '1336 - Coop. production de thé '   : {
        name       : '1336 - Coop. production de thé ',
        description: "Les produits de la marque 1336 sont sélectionnés et préparés avec soin. Engagée par nature, l'objectif de la coopérative SCOPTI est de favoriser la revitalisation d'une filière courte en relation avec des producteurs locaux sans distinction régionale, en privilégiant tout le territoire français et en respectant les critères géographiques nécessaires à la qualité gustative de certaines plantes. <br><br> 📍 Boutique 1336 - 500 avenue du Pic de Bertagne -  13420 Gémenos ",
        url        : 'https://www.1336.fr/'
    },
    'Librairies | Bibliothèques'        : {
        name       : 'Librairies | Bibliothèques',
        description: '',
        url        : undefined
    },
    'Bibliothèque Alcazar'                             : {
        name       : 'Bibliothèque Alcazar',
        description: '📍 58 cours Belsunce | 13001 | Marseille <br><br>Bibliothèque Publique<br><br>',
        url        : 'https://www.bmvr.marseille.fr/'
    },
    Wildproject                         : {
        name       : 'Wildproject',
        description: "📍 12 bd National | 13001 <br><br>Comment réorganiser les sociétés humaines dans leurs relations au vivant, pour mettre un terme à l'extinction en cours de la vie sur Terre ? Telle est la tâche à laquelle veut contribuer Wildproject, une maison d'édition indépendante qui a œuvré dans la décennie 2010–2020 à importer et acclimater en langue française les pensées de l'écologie.",
        url        : 'https://wildproject.org/'
    },
    'Mille Bâbords'                     : {
        name       : 'Mille Bâbords',
        description: "Un lieu de culture politique, du travail de mémoire à la pensée vivante, un lieu de débats, d'échanges, de réflexions, un lieu de répercussion des informations, tant en provenance des médias (contestataires, alternatifs ou indépendants) que du mouvement social,  un carrefour et un lieu de rencontre pour la vie associative, politique et le mouvement social marseillais, un outil pour la mise en réseau des volontés d'action et d'information, un soutien logistique et conceptuel à l'élaboration, la gestation et la mise en oeuvre de projets militants innovants ou émergents.<br><br>📍61 RUE CONSOLAT<br> ✉️ contact@millebabords.org<br> 📞 04 91 50 76 04<br><br>",
        url        : 'https://www.millebabords.org/'
    },
    'La DAR'                            : {
        name       : 'La DAR',
        description: 'La Dar est un lieu aux multiples facettes, qui accueille depuis 2013 des ateliers autogérés, cantines et soirées de soutien, projections, discussions et assemblées générales … Un lieu toujours plus en commun, grâce au soutien des dynamiques collectives, un outil collectif mis au service du quartier, des luttes et de ce qui nous anime dans nos vies marseillaises.<br><br>La Dar Centre Social Autogéré c’est un espace d’autonomie collective, géré par et pour ses usager·es et aussi : <ul><li>Un local de quartier (ateliers, soutien scolaire, distribution alimentaire…)</li><li>Un lieu d’auto-organisation et de luttes pour les assemblées d’habitant.es, celles des personnes sans papiers, des précarisé.es et pour les mouvements sociaux…</li><li>Une cuisine collective (cantines, maraudes)</li><li>Un espace de convivialité, avec une bibliothèque, des concerts et des spectacles</li></ul> C’est un espace ouvert, autogéré et non marchand, accessible à toustes. Nous souhaitons voir la Dar comme un outil et un espace d’organisation pour les personnes et collectifs du quartier, comme un lieu dont toustes peuvent se saisir pour proposer des activités, faire vivre leur projet ou investir les ateliers et espaces existants.<br><br><b>Espace autogéré et autofinancé</b><br><br>À la Dar, nous sommes toustes bénévoles. Les membres de la coordination du lieu se réunissent en AG une fois par semaine, et sont réparti.es dans différentes commissions qui permettent de s’occuper de la gestion du lieu. <br><br>Nous sommes autofinancé.es: grâce à un système de cotisations mensuelles, nous payons les charges fixes du lieu (loyer, assurance, électricité, internet, les produits liés à l’utilisation du lieu, etc.). Toute autre rentrée d’argent (buvette, événements de soutien, adhésions, subventions exceptionnelles) servent à financer d’autres dépenses (travaux, achat de matériel, soutien à des luttes).<br><br><b>Oppressions & Mixité Choisie</b><br><br>Nous voulons faire de La Dar un lieu le plus sûr possible et travaillons – avec vous – en ce sens.<br><br>Les comportements oppressifs de tout type (tel que les comportements racistes, sexistes, homophobes… ) n’y sont pas tolérés. Et n’hésitez pas à en parler si vous ne vous sentez pas à l’aise dans ce lieu ou si une situation vous a fait violence. Nous nous gardons le droit de refuser certaines personnes à l’entrée ou d’exclure des personnes du lieu pour leur comportement violent, oppressif ou à l’encontre des valeurs du lieu.<br><br>La coordination s’engage collectivement et individuellement à respecter cette charte, et vous encourage toustes à nous signaler toute situation problématique ou de tension concernant le lieu ou ses membres, personnellement ou par mail à alloladar@la-dar.org. Également, via une boite aux lettres à l’entrée de la Dar si c’est plus adapté (anonymat possible). <br><br>Un petit groupe de personnes défini (déjà existant) recevra ces témoignages et s’engage à vous rencontrer (à deux, trois personnes max) pour en discuter si vous le souhaitez. Également, nous respecterons l’anonymat si la situation évoquée doit être discutée en plus grand nombre.<br><br>Nous avons à cœur d’organiser et d’encourager des évènements en mixité choisie. Ce sont des moments de lutte, de réflexion mais aussi de festivité qui participent aux luttes contre les oppressions. Lors de ces événements, l’entrée pourra donc être refusée à certaines personnes qui ne se reconnaissent pas dans la mixité choisie de l’événement.<br><br>Nous continuons à travailler dessus et souhaitons nous améliorer, ainsi n’hésitez pas à nous faire part de vos suggestions en ce sens !<br><br><b>Prix Libre & Mise à disposition</b><br><br>Ici, c’est du prix libre, mais les boissons vendues au bar sont à prix fixe.<br><br>La Dar est un lieu où le prix n’est pas un frein à l’entrée, ou encore un facteur de jugement. Ainsi, le prix libre (et non le prix conseillé !) se fait dans une boîte permettant une discrétion aux personnes quant à ce qu’iels donnent et la possibilité pour elleux de faire leur propre monnaie.<br><br>Nous mettons également le lieu à disposition pour divers rencontres, réunions ou autre par des associations ou collectifs, et ce à prix libre.<br><br>La Dar est un espace d’autogestion et il va de soi que tout le monde doit prendre soin de cet espace partagé et des personnes présentes.<br><br><b>Accès au lieu</b><br><br>L’accès à La Dar se fait au 127 rue d’Aubagne, 13006. L’adhésion est obligatoire, à prix libre et annuelle (du 1er septembre au 31 août).<br><br>Pour devenir adhérent.e, venez nous voir ou inscrivez-vous à l’entrée sur le petit carnet ! N’oubliez pas de vous faire une carte d’adhérent.e aussi !<br><br>Le rez-de-chaussée est accessible aux personnes à mobilité réduite, mais pas le premier étage où se trouve la bibliothèque, ni les toilettes.<br><br>Nous vous conseillons de porter un œil sur vos affaires personnelles et même de ne pas les laisser trop traîner. En cas de perte ou de vol, nous ne pourrons malheureusement faire grand chose si ce n’est de tenter de vous redonner le moral 🙂<br><br><b>Contact</b><br><br>Si vous voulez proposer une activité, une soirée, une cantine, bref vous souhaitez programmer quelque chose à la Dar, écrivez-nous à programmation@la-dar.org.<br><br> Sinon, écrivez-nous à contact@la-dar.org.<br><br>Finalement, on a réparé notre boite au lettres 😉 ! L’adresse c’est:<br><br>📍127 rue d’Aubagne | 13006 <br><br>',
        url        : 'https://la-dar.org/'
    },
    Transit                             : {
        name       : 'Transit',
        description: "📍 51 bd Libération | 13001<br><br> L'association Transit est une librairie dont l'identité « éditoriale » repose essentiellement sur son fonds, sa programmation, ses rencontres et son « nomadisme ». Défendre l'édition indépendante, défendre les pensées critiques et alternatives du Tout monde, rendre audibles et intelligibles les voix étouffées dans le vacarme des voix dominantes, accueillir avec curiosité, enthousiasme et admiration les pensées, les textes, les poèmes, confronter, mettre en lumière, subjectiver, échanger, partager, diffuser, provoquer, enchanter, incarner sont les piliers sur lesquels reposent...",
        url        : 'https://transit-librairie.org/'
    },
    "L'Hydre aux milles têtes"          : {
        name       : "L'Hydre aux milles têtes",
        description: "📍 96 Rue Saint-Savournin | 13001<br><br>Au cœur du quartier de La Plaine, L'Hydre aux mille têtes invite les lecteurs et lectrices de tous âges à découvrir son large assortiment de livres, revues, DVD et créations graphiques originales. Par leurs choix forts et leurs conseils avisés, les libraires de L'Hydre souhaitent porter les voix de celles et ceux qui créent et luttent pour l'émancipation. 96 rue Saint Savournin ",
        url        : 'https://www.librairesdusud.com/portfolio-item/lhydre-aux-mille-tetes/'
    },
    Manifesten                          : {
        name       : 'Manifesten',
        description: 'Bibliothèque, café-bar, ateliers, evénements',
        url        : 'https://www.facebook.com/manifesten'
    },
    'Mémoire des Sexualités'            : {
        name       : 'Mémoire des Sexualités',
        description: "L'association Mémoire des Sexualités met à disposition un fonds documentaire, organise débats publics et salons, et participe à la construction du mouvement militant LGBT.",
        url        : 'https://www.memoire-sexualites.org/'
    },
    "L'Histoire de l'oeil"              : {
        name       : "L'Histoire de l'oeil",
        description: "📍 25 rue Fontange | 13006<br><br>L'Histoire de l'oeil est spécialisée dans les formes contemporaines. Qu'il s'agisse de littérature ou d'art, nous sommes attachés à faire des propositions actuelles. Le rayon littérature comprend les romans, les romans policiers, mais aussi un important fond en théâtre et en poésie. Le rayon art propose des monographies d'artistes, catalogues d'exposition, écrits d'artistes, multiples... Quant à la jeunesse, nous proposons un choix de livres très graphiques ou de textes d'auteurs que nous souhaitons défendre.",
        url        : 'https://www.histoiredeloeil.com/'
    },
    'Les Héroïnes'                      : {
        name       : 'Les Héroïnes',
        description: '📍 91 rue Loubon | 13003<br><br>Bibliothèque feministe <br><br> Ouvert les lundis 18h-21h  Tout le publique le premier lundi du mois',
        url        : ''
    },
    CIRA                                : {
        name       : 'CIRA',
        description: "<b>Centre International de Recherche sur l'Anarchisme</b><br><br> Le principal but du Centre International de Recherches sur l’Anarchisme de Marseille est de collecter, de classer et d’archiver tout ce qui a un rapport avec l’anarchisme. Le fonds, en décembre 2021, se compose d’environ 10500 livres (9224 en français, 541 en castillan, 368 en italien, 297 en anglais,74 en allemand, 36 en portugais, 6 en suédois, espérento et russe, ect.), 5035 brochures, 308 thèses et 1161 documents numériques. Ces documents ont été écrits par des anarchistes, publiés par des anarchistes ou portent d’une manière ou d’une autre sur le mouvement ou les idées anarchistes. On trouvera donc aussi bien des livres favorables que défavorables aux idées anarchistes. De même, sont conservés des écrits et des biographies de personnes qui n’ont été anarchistes qu’une partie de leur vie seulement. Le CIRA possède également des archives personnelles de militants, des affiches, des tracts, des cassettes vidéo, des documents iconographiques (cartes postales, photos…), des travaux universitaires, des dossiers biographiques…<br><br> De nombreux périodiques sont envoyés par ceux qui les éditent (1394 en français). Le CIRA possède un répertoire recensant 3212 publications anarchistes parues en langue française entre 1850 et 1993.<br><br> Les documents sont écrits dans une vingtaine de langues. Les plus représentées sont le français, le castillan et l’italien.<br><br> La bibliothèque de prêt est alimentée par les dons et les services de presse d’éditeurs (plusieurs centaines de titres chaque année). L’informatisation du catalogue de livres a commencé en l’an 2000 et se poursuit. Ce catalogue peut être consulté sur le site Internet. Un catalogue sur fiches des livres est consultable sur place.<br><br>L’ensemble du fonds peut être consulté librement et gratuitement par toute personne intéressée : militant, étudiant, chercheur, écrivain, universitaire, journaliste ou simple curieux. Il est répondu par courrier aux demandes de renseignements lorsque les recherches ne sont pas trop longues. <br><br> Un bulletin est publié (45 numéros à ce jour). Des sujets très variés y ont été abordés. Les premiers bulletins faisaient un état des collections. Puis ont été édités des bulletins thématiques. Ils étaient d’abord consacrés à la région marseillaise : le Congrès de Marseille en 1879, la section de l’Internationale, Louise Michel en Provence. Puis fut abordée la Seconde Guerre mondiale vue par les anarchistes français et espagnols. Des biographies de militants sont également parues. Un bulletin a été consacré à l’anarchisme en Argentine.<br><br>Ces bulletins sont complétés par une liste des travaux en cours, une bibliographie anarchiste annuelle (depuis 1990), une filmographie (depuis 2017) et par une Feuille d’infos mensuelle (depuis 1999). Éditeur, le CIRA a publié deux livres en collaboration avec d’autres associations (Han Ryner et André Arru) et quinze calendriers (depuis 2008). Le CIRA organise régulièrement des causeries, des débats, des tables rondes, des cycles de discussion, des expositions, des rencontres avec des auteurs et des éditeurs. En 2019, les sujets suivants ont été abordés : la guerre nucléaire, les luttes en Espagne dans les années 1970, les anarchistes italiens en exil, l’humour libertaire de Daniel Villanova, les coopératives, les idées de Kropotkine, le maquis limousin, les femmes en prison, Camus libertaire. Le CIRA collabore à des colloques (L’anarchisme, Barcelone, 1993, La culture libertaire, Grenoble, 1996, La littérature prolétarienne, Saint-Nazaire, 2002, Le mouvement ouvrier en Provence pendant la Première Guerre mondiale, Marseille, 2014) et en organise (L’extrême-droite à Marseille, 1987, Han Ryner, 2002, Alexandre Marius Jacob, 2005). Il prête des documents pour des expositions.<br><br>En 2003, 2010 et 2015, le CIRA a organisé Foire aux livres anarchistes de Marseille (FLAM) avec des stands d’éditeurs, des débats et des spectacles. Il participe à divers salons du livre, anarchistes ou non, présentant la production des éditeurs libertaires.<br><br> Quand il a perdu son local en 1989, le CIRA a déposé une partie de son fonds (1750 périodiques, 2000 affiches…) aux Archives départementales des Bouches-du-Rhône. Ces documents ont été classés et peuvent y être consultés tous les jours.<br><br>Le CIRA compte, fin 2021, plus de 200 membres, originaires de toutes les régions de France et d’autres pays. Certains, bien qu’éloignés de Marseille, participent très activement au CIRA (correspondances, recherches). Depuis 1987, il est constitué en association Loi de 1901. La gestion du Centre est faite de manière collective et militante par un conseil d’administration élu en assemblée générale. Le Centre vit essentiellement des cotisations de ses membres.<br><br>Le CIRA fait partie de la Fédération internationale des centres d’étude et de documentation libertaires (FICEDL) qui s’est réunie pour la dernière fois à Bologne (Italie) en 2016. Il est indépendant de toute organisation politique ou syndicale. Cela ne l’empêche pas de participer à certaines actions de solidarité.<br><br>📍50 rue Consolat | 13001 <br><br> ",
        url        : 'https://www.cira-marseille.info/'
    },
    'Lieux Urbains Autogérés'           : {
        name       : 'Lieux Urbains Autogérés',
        description: '',
        url        : undefined
    },
    Manifestin                          : {
        name       : 'Manifestin',
        description: 'Cantine de soutien pour divers projets/causes',
        url        : 'https://www.facebook.com/manifesten'
    },
    MARSS                               : {
        name       : 'MARSS',
        description: "Mouvement et Action pour le Rétablissement Sanitaire et Social L'équipe de rue, considérée comme une « équipe mobile psychiatrie précarité », a été la première activité de l'équipe MARSS, dès 2005. L'équipe intervient à plusieurs niveaux : d'abord pour orienter, ensuite pour soutenir des partenaires qui rencontrent des situations complexes et enfin et surtout dans le suivi direct des personnes.",
        url        : 'https://www.marssmarseille.eu/activites-de-soins/equipe-de-rue'
    },
    'Cercle de Nageurs en Eaux Troubles': {
        name       : 'Cercle de Nageurs en Eaux Troubles',
        description: 'Autour de la psychiatrie, santé mentale, pair-aidance...',
        url        : ''
    },
    SPAAM                               : {
        name       : 'SPAAM',
        description: 'SPAAM propose des temps de rencontres et amène des pratiques de soutien et de soin (écoute active, temps de formation à des outils d’organisation collective, d’hypnose et d’auto-hypnose, massage et auto-massage, herboristerie...) afin de permettre aux personnes ayant subi ou susceptibles de subir ces violences étatiques de ne pas se retrouver isolées et démunies face à ses conséquences.<br><br>Nous avons commencé à nous organiser autour de nos propres expériences de la répression, du soin et de nos besoins anti-répressifs, à nous auto-former à des pratiques de soin qui nous paraissent pertinentes dans ce contexte. Nous avons un info-kiosque sur ces thématiques. La formation, l’auto-formation et l’information sur tout ce qui peut accompagner des processus de soin liés à la répression nous semblent essentiels pour renforcer nos autonomies et nos cultures du soin en milieu militant.<br><br> Pour tout retour, toute question, contactez nous par mail (SPAAM13@RISEUP.NET) et par Signal (+33 7 45 97 56 99)',
        url        : 'https://we.riseup.net/spaam13/infokiosque-spaam-zip+809570'
    },
    'SPAAM-infos'                       : {
        name       : 'SPAAM-infos',
        description: "Sélection de brochures du collectif « Soin Psy Anti-rep Marseille ».<br><br> Le fichier Zip contient ces brochures : Agir en groupe affiniaire - Base Arrière 25 mars 2023 - Blessé.es par la police info conseils - Brochure juridique IGPN 2022 - Brochure Partie Medic 2021 - Débrief psy post mobilisation 2023 - FAQ Soin psy post mobilisation 2023 - Fyler armes SDLT 2023 - Guide du manifestant aux urgences - Guide perquiz livret - Out Of Action Premiers secours emotionnel - Plainte ou non 2023 - Réduire les risques psycho-emotionnel lors de mobilisations 2023 - Rétablissement et résilience Soutien&Rétablissement 2018 - Sommeil insomnies herboristerie 2023 - Soutenir une personne en détresse - écoute active 2023 - Techniques anti-stress - Trauma et blessures 2023 - Tuto écoute active sur événement 2023 - Zbeule ton contrôle policier 2023",
        url        : 'https://we.riseup.net/spaam13/infokiosque-spaam-zip+809570'
    },

    "Saccage (Anti-JO's)"                           : {
        name       : "Saccage (Anti-JO's)",
        description: '',
        url        : ''
    },
    'Groupe Anti-gentrification'                    : {
        name       : 'Groupe Anti-gentrification',
        description: '',
        url        : ''
    },
    Technopolice                                    : {
        name       : 'Technopolice',
        description: '',
        url        : undefined
    },
    CQFD                                            : {
        name       : 'CQFD',
        description: "Mensuel de critique et d'expérimentation sociales",
        url        : 'https://cqfd-journal.org/'
    },
    'Le Café des Chômheureuses'                     : {
        name       : 'Le Café des Chômheureuses',
        description: '',
        url        : ''
    },
    'Atelier du Mouvement (danse)'                  : {
        name       : 'Atelier du Mouvement (danse)',
        description: '',
        url        : ''
    },
    'Atelier Théâtre'                               : {
        name       : 'Atelier Théâtre',
        description: '',
        url        : ''
    },
    'accueil de jour Sirakadjan'                    : {
        name       : 'accueil de jour Sirakadjan',
        description: '',
        url        : ''
    },
    'maladroite boxe populaire'                     : {
        name       : 'maladroite boxe populaire',
        description: '',
        url        : ''
    },
    'Atelier de Couture'                            : {
        name       : 'Atelier de Couture',
        description: '',
        url        : ''
    },
    'Bibliothèque Nulle part Ailleurs'              : {
        name       : 'Bibliothèque Nulle part Ailleurs',
        description: '',
        url        : ''
    },
    'Cantine The Noble Kitchen'                     : {
        name       : 'Cantine The Noble Kitchen',
        description: '',
        url        : ''
    },
    'Cantine Chaubouillante'                        : {
        name       : 'Cantine Chaubouillante',
        description: '',
        url        : ''
    },
    'les gadjis'                                    : {
        name       : 'les gadjis',
        description: 'narcotiques anonyme en mixité choisie sans mecs cis',
        url        : ''
    },
    'antifa social club'                            : {
        name       : 'antifa social club',
        description: '',
        url        : ''
    },
    sirakadjan                                      : {
        name       : 'sirakadjan',
        description: '',
        url        : ''
    },
    'La Base'                                       : {
        name       : 'La Base',
        description: 'Collectifs, associations et voisin·es marseillais·es construisent ensemble un nouvel espace dédié à la transition vers une société plus juste et plus durable. Nous construisons et gérons ensemble un espace commun et autogéré.',
        url        : 'https://labasemarseille.org/'
    },
    Alternatiba                                     : {
        name       : 'Alternatiba',
        description: '',
        url        : ''
    },
    'ANV COP-21m'                                   : {
        name       : 'ANV COP-21m',
        description: '',
        url        : ''
    },
    'Testing children node'                         : {
        name       : 'Testing children node',
        description: '',
        url        : ''
    },
    Transat                                         : {
        name       : 'Transat',
        description: "L'association Transat est une association de personnes trans, à visée militante et d'entraide, basée sur Marseille et ses environs.<br><br>Aujourd'hui, le sujet de la transidentité est encore très méconnu du grand public. Les personnes trans sont encore largement exposées au quotidien à des préjugés transphobes.<br><br>Par ailleurs, ces préjugés peuvent venir ralentir ou empêcher des démarches de transition (changer d'état civil, accéder à des traitements médicaux etc.) souvent nécessaires pour le bien-être des personnes. Dans ce cadre, nos actions visent à sensibiliser le public à la question de la transidentité, et apporter du soutien aux personnes concerné.es, dans un objectif de défense de nos droits et de lutte contre la transphobie.<br><br> <b>Nos objectifs :</b> <ul><li> s'entraider entre personnes trans</li><li> défendre et plaider pour les droits des personnes trans</li><li>lutter contre la transphobie</li></ul><br> <b>Ce que nous faisons :</b><br><br>Nous organisons des permanences et des activités pour les personnes trans et leurs proches, dans une démarche de valorisation de l'échange, de soutien, d'écoute, et d'empowerment.<br>Nous organisons et participons à des événements et/ou formations, dans le but de sensibiliser le public aux questions ayant trait à la transidentité.<br>Nous allons à la rencontre des institutions publiques ou privées pour faire valoir les droits des personnes trans.<br>Nous nous inscrivons dans une approche inclusive, féministe et intersectionnelle, et privilégions autant que possible une posture d'ouverture et de dialogue. <br><br><b>Nos revendications :</b> <br><ul><li>la dé-psychiatrisation réelle des parcours de transition médicale et la fin des inégalités d'accès aux soins et aux services publics qui touchent les personnes trans </li><li> la possibilité pour tou.tes de changer de prénom et de mention «sexe» à l'état civil sur simple déclaration</li><li>l'ouverture de la P.M.A. et de la conservation des gamètes à toutes les personnes quelque soit leur orientation sexuelle, identité de genre ou situation conjugale.</li><li> la légalisation de la G.P.A. pour tou.tes</li></ul><br><br><b>Nos permanences :</b><ul><li>Pour les personnes trans et/ou en questionnement : tous les premiers samedi du mois, de 15h à 18h </li><li>Pour les proches de personnes trans et/ou en questionnement : tous les premiers mercredis du mois, de 18h à 21h</li></u > à la BASE --» 3, rue Pierre Roche, 13004 <br><br><b>Nous contacter :</b> <br>Mail : transat.asso@gmail.com <br><br><b>Nous suivre : </b><br>Twitter : @Asso_Transat <br>Instagram : @associationtransat <br> https://www.facebook.com/TransatAsso/ ",
        url        : 'https://www.lgbt-paca.org/annuaire/transat/'
    },
    'XR - Extinction Rebellion'                     : {
        name       : 'XR - Extinction Rebellion',
        description: '',
        url        : ''
    },
    'Youth for Climate'                             : {
        name       : 'Youth for Climate',
        description: '',
        url        : ''
    },
    'Résistance Aggression Publicitaire'            : {
        name       : 'Résistance Aggression Publicitaire',
        description: '',
        url        : ''
    },
    'Aïolibre'                                      : {
        name       : 'Aïolibre',
        description: '',
        url        : ''
    },
    'La Roue (monnaie)'                             : {
        name       : 'La Roue (monnaie)',
        description: "La Monnaie Locale Complémentaire et Citoyenne en Provence - Alpes du Sud. En adoptant la Roue, vous participez à une démarche citoyenne innovante au service de la transition économique, écologique et sociale du territoire. Elle est un levier destiné à dynamiser le commerce de proximité et les circuits courts, en faveur du maintien et du développement de l'emploi en Provence et Alpes du Sud.",
        url        : 'https://laroue.org/'
    },
    'Télé Mouche'                                   : {
        name       : 'Télé Mouche',
        description: 'La WebTV indépendante ! Télé Mouche est un circuit-court audiovisuel, une plateforme mutualisée, qui vous invite à partager un média libre et participatif.',
        url        : 'https://telemouche.com/'
    },
    'AG InterPro'                                   : {
        name       : 'AG InterPro',
        description: 'Signal link : https://signal.group/#CjQKIBAKYyGwvBmjSgV3d6SnuKu12Q8IrQZF-kE7hGbIPQnWEhDFZ7GXoQ4KiSu6jUKBs4wU',
        url        : 'https://www.instagram.com/ag_interpro_marseille/'
    },
    CNT13                                           : {
        name       : 'CNT13',
        description: '',
        url        : undefined
    },
    Solidaires13                                    : {
        name       : 'Solidaires13',
        description: '',
        url        : undefined
    },
    'SESL - solidaires étudiants'                   : {
        name       : 'SESL - solidaires étudiants',
        description: '',
        url        : ''
    },
    'BTP autonome'                                  : {
        name       : 'BTP autonome',
        description: 'Collectif de travailleuses et travailleurs du bâtiment en lutte!<br><br> Permanence tous les mercredis, 18h30-20h au 📍29 Bd Longchamp, 13001<br>✉️btp13@riseup.net<br>📞06 95 75 42 71 ',
        url        : 'https://www.instagram.com/btp13autonome/'
    },
    'AG précaires'                                  : {
        name       : 'AG précaires',
        description: '',
        url        : ''
    },
    'sud éduc'                                      : {
        name       : 'sud éduc',
        description: '',
        url        : ''
    },
    Asso                                            : {
        name       : 'Asso',
        description: '',
        url        : ''
    },
    'Le Social Brûle 13'                            : {
        name       : 'Le Social Brûle 13',
        description: 'Collectif des travailleur.es du social & du médico-social en lutte à Marseille.Nous contacter : lesocialbrule@riseup.net',
        url        : 'https://www.facebook.com/groups/LeSocialBrule/'
    },
    'Al Manba'                                      : {
        name       : 'Al Manba',
        description: 'Collectif Soutien Migrants 13',
        url        : 'https://www.facebook.com/collectifmigrants13'
    },
    'Permanence Juridique'                          : {
        name       : 'Permanence Juridique',
        description: '',
        url        : ''
    },
    'Cours FLE'                                     : {
        name       : 'Cours FLE',
        description: 'Français Langue Étrangère',
        url        : ''
    },
    'La Nebula'                                     : {
        name       : 'La Nebula',
        description: 'Atelier privé (uniquement sur rdv) et partagé revendiqué comme un multivers créatif féministe',
        url        : 'https://www.facebook.com/LaNebulamars'
    },
    'Bourse du Travail'                             : {
        name       : 'Bourse du Travail',
        description: '',
        url        : ''
    },
    'CGT chômeur précaires'                         : {
        name       : 'CGT chômeur précaires',
        description: '',
        url        : ''
    },
    'CGT spectacle'                                 : {
        name       : 'CGT spectacle',
        description: '',
        url        : ''
    },
    'Centre LGBTQIA+'                               : {
        name       : 'Centre LGBTQIA+',
        description: "Agenda du centre <br><br> Situé à proximité du Vieux-Port, le Centre a été créé en 2023 pour répondre aux besoins d'inclusion, de soutien et de visibilité des LGBTQIA+. Ouvert à tous·tes, le Centre accueille et célèbre la communauté queer marseillaise dans toute sa diversité. Nous sommes fier·e·s de travailler avec les associations et collectifs marseillais·e·s concerné·e·s ainsi que des professionnel·le·s, militant·e·s et bénévoles pour proposer des ressources, ateliers, séances de soutien et accompagnement spécifiques aux personnes LGBTQIA+. Venez aussi profiter de notre bar pour échanger, rencontrer, assister à nos événements culturels et festifs !",
        url        : 'https://centrelgbtqiamarseille.org/agenda/'
    },
    'G.L.A.M'                                       : {
        name       : 'G.L.A.M',
        description: "Groupe de lutte pour l'acceuil des Migrant·es - Nos permanences ont lieu tous les mardi de 18h30 à 21h au @centre_lgbtqia_marseille 🏳️‍🌈🏳️‍⚧️ dans l'espace Santé ✍️ Accueil sans rendez vous 🗓️",
        url        : 'https://www.instagram.com/glam13lgbt'
    },
    'Les 8 Pillards'                                : {
        name       : 'Les 8 Pillards',
        description: 'Laboratoire de Production et de Vie',
        url        : 'https://www.les8pillards.com/'
    },
    'Imprimerie Partagée'                           : {
        name       : 'Imprimerie Partagée',
        description: '60 Rue Edmond Rostand',
        url        : 'https://qx1.org/lieu/imprimerie-partagee-rue-rostand/'
    },
    'Gratuit pour les urgences administratives'     : {
        name       : 'Gratuit pour les urgences administratives',
        description: '',
        url        : ''
    },
    'Prix libre pour affiches/flyers/micro-éditions': {
        name       : 'Prix libre pour affiches/flyers/micro-éditions',
        description: '',
        url        : ''
    },
    'Causeries Mensueles'                           : {
        name       : 'Causeries Mensueles',
        description: '',
        url        : ''
    },
    'Atelier Recherche Formation'                   : {
        name       : 'Atelier Recherche Formation',
        description: '',
        url        : ''
    },
    'Vidéodrome 2'                                  : {
        name       : 'Vidéodrome 2',
        description: "L'association Solaris porte un projet unique de diffusion cinématographique qui permet par le prix libre, l'accès au quotidien à une programmation exigeante et hétérogène au plus grand nombre, quelles que soient ses conditions. Elle conjugue avec joie et inventivité patrimoine cinématographique du monde entier, cinéma de genre, expérimentations visuelles et sonores, cinéma militant et création documentaire contemporaine. Se voulant un cinéma de quartier, héritier des cinéclubs, l'association Solaris est un lieu hospitalier de toutes les formes cinématographiques et défend une programmation à vocation culturelle, philosophique et sociologique, en collaboration avec les différents acteurs socioculturels de Marseille.",
        url        : 'https://www.videodrome2.fr/'
    },
    'Les Mains Gauches-festival queer féministe'    : {
        name       : 'Les Mains Gauches-festival queer féministe',
        description: '',
        url        : ''
    },
    'Le Morozoff'                                   : {
        name       : 'Le Morozoff',
        description: 'Collectif de Cirque, Soin & Bricolage',
        url        : 'http://www.lemorozoff.org/'
    },
    Box                                             : {
        name       : 'Box',
        description: 'De lundi à vendredi, 11h',
        url        : ''
    },
    Clownozoff                                      : {
        name       : 'Clownozoff',
        description: 'Tous les Lundis 15h-17h',
        url        : ''
    },
    'Tango Queer'                                   : {
        name       : 'Tango Queer',
        description: 'Tous les mercredis 19h30-22h',
        url        : ''
    },
    'Genre de Lutte'                                : {
        name       : 'Genre de Lutte',
        description: '',
        url        : ''
    },
    'La Déviation'                                  : {
        name       : 'La Déviation',
        description: "📍210 chemin de la Nerthe | 13016 <br><br> Située sur les hauteurs de L’Estaque (16e arrondissement de Marseille) dans une ancienne usine, La Déviation se présente comme un lieu de vie et de recherches artistiques.<br><br> La Déviation est un lieu pluridisciplinaire, où l'on expérimente des formes d’échanges, de critiques réciproques, où les compétences circulent. Nous décloisonnons les frontières des arts, nous redéfinissons en permanence les esthétiques et mettons à l’œuvre l’interdisciplinarité dans toutes les étapes de création.<br><br> La Déviation est un lieu de vie, de convivialité et d’hospitalité. Il s'agit de tisser des alliances entre les résidents et les habitants marseillais ou de la région, et aussi plus largement au niveau national et international. Nous sommes aux aguets du territoire que l’on habite et nous rendons poreux les liens entre lieu de vie et de fabrique artistique. La Déviation interroge ainsi l'idée du rapport à la propriété. Nous faisons le choix du principe de propriété d'usage, c'est-à-dire qu'il n'y a pas de propriété privée puisque nous considérons que les biens doivent appartenir à ceux qui les utilisent. Le lieu appartiendra toujours à ceux qui en font l'usage. <br><br>Dans le contexte d’un foncier assez rare et cher, la Déviation propose l'accessibilité à ses activités (résidences, ateliers, repas...) et la mutualisation de ses espaces selon la politique du prix libre, afin de soutenir le développement de projets artistiques et associatifs à Marseille.<br><br> Nous fonctionnons selon des modes d’organisation collégiaux : gouvernance horizontale et responsabilité partagée. Nous travaillons à construire des formes de vie et de partage, des manières de se rapporter à ce qui se cherche, s’invente, se dévie, se tente. L’autogestion du lieu implique qu’il est de la responsabilité des usagers de participer à la vie du lieu, à l’entretien et l’aménagement du bâtiment ainsi qu’à l’accueil des artistes et des visiteurs.<br><br><b>Ouverture au public :  Tous les vendredis à 18h</b>",
        url        : 'http://www.ladeviation.org/'
    },
    'Lieux Ruraux Autogérés'                        : {
        name       : 'Lieux Ruraux Autogérés',
        description: '',
        url        : undefined
    },
    'Café Villageois (Lauris)'                      : {
        name       : 'Café Villageois (Lauris)',
        description: "A Lauris, on éteint sa télé, on descend dans les rues, on tchatche, on s'entraide et on expérimente la convivialité. Au Maquis propose des espaces de développement local pour échanger, rêver et créer.<br><br>📍Avenue de la Gare 84360 LAURIS <br>📞07 68 26 42 65<br> ✉️contact@aumaquis.org",
        url        : 'https://www.aumaquis.org/'
    },
    'Laboratoires de vie'                           : {
        name       : 'Laboratoires de vie',
        description: "Lieux d'experimentation sociale « totale » : production, organisation, autonomie, école, construction, soin, etc",
        url        : undefined
    },
    "L'école des vivants"                           : {
        name       : "L'école des vivants",
        description: "L'école des vivants est une école buissonnière, créée et portée par des artistes, des paysans, des scientifiques et des militant'es habitées par une conviction : on ne changera pas cette société sans apprendre, ni explorer ni expérimenter… Elle est créée et portée par des artistes, des militant'es et des scientifiques habités par une conviction : l'époque n'est plus aux demi-teintes. À la grise mine. On nous a assez coupé de ce qu'on peut. Il est temps de battre le capitalisme sur le terrain du désir. De déployer nos sensibilités, de hisser nos couleurs, nos valeurs, nos envies. Qu'elles prennent corps. Il est temps de faire bruisser ce monde qu'on veut voir advenir, d'en faire sentir la poussée végétale, le soleil de liens, les puissances animales qui nous traversent et la vitalité qu'on en tire dans nos rapports au dehors et aux autres. L'école des vivants fait partie d'un projet politique plus vaste que nous avons baptisé « la ZESTE » pour Zone d'Expérimentation Sociale, Terrestre et… Enchantée !",
        url        : 'https://www.ecoledesvivants.org/'
    },
    'Longo Maï'                                     : {
        name       : 'Longo Maï',
        description: "La Coopérative européenne Longo Maï est une coopérative agricole et artisanale autogérée, internationale, d'inspiration alternative, libertaire, laïque, rurale et anticapitaliste. Fondée en 1973 à Limans (Alpes-de-Haute-Provence), elle regroupe en réseau onze coopératives en France, Allemagne, Autriche, Suisse, Ukraine, Roumanie et Costa Rica. <br>Le nom est issu d'une formule du provençal exprimant le souhait « que ça dure » et utilisée par exemple lors des mariages : Vivo li nòvi e longo mai !, traduisible par « Que vivent les mariés, et longtemps encore ! ».",
        url        : 'https://www.prolongomaif.ch/'
    },
    'Grange neuve (Forcarlquier)'                   : {
        name       : 'Grange neuve (Forcarlquier)',
        description: "La plus ancienne et la plus grande des coopératives sur près de 300 hectares. Une centaine de personnes de toutes générations partagent leur quotidien grâce à des infrastructures collectives (cuisine, salle commune ...). Tous les métiers qui participent à la recherche d'autonomie de la coopérative y sont pratiqués : textile, bois, construction, agriculture, arboriculture, semences, élevage, fromagerie, meunerie et boulangerie, transformation de fruits, légumes et viande, herbes médicinales et aromatiques, ateliers mécanique et métal, pour ne citer qu'eux, participent à la vie foisonnante qui se perpétue depuis plus de quarante ans. C'est également un lieu d'accueil, de formation, de création, de rencontres et de fêtes. ",
        url        : 'https://prolongomai2017.jimdofree.com/longo-ma%C3%AF/kooperativen/grange-neuve-fr/'
    },
    'Mas de Granier (Saint Martin de Crau)'         : {
        name       : 'Mas de Granier (Saint Martin de Crau)',
        description: "Sur la plaine de la Crau entre Marseille, Arles et Avignon, dans un environnement politiquement hostile et écologiquement dégradé, le Mas de Granier est une véritable oasis. Une quinzaine de coopérateur·rice·s mènent une production maraîchère bio pour leur consommation et la transformation en conserves de qualité vendues sur les marchés. La conserverie est également utilisée par de nombreux petits paysans et paysannes locaux. Profitant de l'irrigation, ils produisent le célèbre foin de Crau qui alimente les troupeaux de Limans et d'ailleurs. Sur sept autres hectares sont produites des céréales ensuite transformées en pain et pâtes. Volailles, cochons, olives, miel complètent la gamme des productions.",
        url        : 'https://www.prolongomaif.ch/longo-ma%C3%AF/coop%C3%A9ratives/mas-de-granier-fr/'
    },
    'La Cabrery (Luberon)'                          : {
        name       : 'La Cabrery (Luberon)',
        description: "C'est la coopérative vinicole de Longo maï. Sur les vingt hectares du domaine, dix sont cultivés en vignes de différents cépages qui donnent des vins biologiques et en partie naturels blancs et rouges ainsi que du jus de raisin. La douzaine d'habitant·e·s ont diversifié leurs activités, en produisant du pain à partir de leurs céréales, des fruits et légumes, de l'huile d'olive, du fromage de chèvre, ainsi que des plantes tinctoriales utilisées dans l'atelier de sérigraphie de la coopérative de Limans.  Ils vendent leur vin sur des marchés et des foires régionales ou sur commande.",
        url        : 'https://www.lacabrery.org/'
    },
    'Filature de Chantemerle (Hautes-Alpes)'        : {
        name       : 'Filature de Chantemerle (Hautes-Alpes)',
        description: "📍St. Chaffrey, Hautes-Alpes <br>Dans cette ancienne usine textile, une des dernières filatures des Alpes, une équipe expérimentée transforme la laine des brebis de Longo maï et celle récoltée par les équipes de tonte. Les produits de la filature sont vendus directement sur place, sur des foires régionales ou sur les stands des marchés de Noël en Suisse. C'est également un lieu de documentation et de formation. Une turbine hydro- électrique récemment rénovée transforme la force de l'eau en énergie électrique pour alimenter l'usine, le surplus est vendu.  ",
        url        : 'https://www.prolongomaif.ch/longo-ma%C3%AF/coop%C3%A9ratives/filature-de-chantemerle-fr/'
    },
    'Treynas (Ardèche)'                             : {
        name       : 'Treynas (Ardèche)',
        description: "📍Chanéac, Ardèche <br>La coopérative s'étend sur 200 hectares dont 100 de forêt. Sur les prairies pâturent le troupeau de moutons, les vaches et les chevaux de traits. Les activités principales sont le bûcheronnage, notamment dans la forêt de la Grangette et les métiers du bois, dont le sciage, mais aussi l'agriculture, le maraîchage et l'élevage. Ces dernières activités assurent une large part de la consommation de la quinzaine de personnes vivant sur ce lieu. Ce groupe, fortement engagé pour la préservation de la forêt, propose plusieurs fois par an des stages de formation au bûcheronnage et débardage à cheval en forêt. ",
        url        : 'https://www.prolongomaif.ch/longo-ma%C3%AF/coop%C3%A9ratives/treynas-fr/'
    },
    Fermes                                          : {
        name       : 'Fermes',
        description: '',
        url        : undefined
    },
    'La Caillasse (Cucuron)'                        : {
        name       : 'La Caillasse (Cucuron)',
        description: "Nous avons pour objectifs de développer et soutenir des activités agricoles et sociales soucieuses de la défense du patrimoine naturel, favorisant l'entraide entre la ville et la campagne et promouvant des modes de production, de coopération et de consommation écologiques. Nous travaillons sans chimie, dans le respect de la vie du sol et autant que possible avec des variétés locales. <br><br>Nous sommes basés au sud du Vaucluse, zone rurale riche en activités agricoles. Nous partons du constat que d’une part, nous vivons proche de grandes villes populaires où l’accès à une nourriture de qualité ne va pas de soi. Cela en raison de son prix élevé et d'un faible approvisionnement mais aussi, à nos yeux, en raison de l’absence de terres agricoles accessibles à proximité de la ville.<br><br> Notre association vise à rapprocher ces milieux en favorisant la rencontre et l’entraide au cours de chantiers agricoles et d’actions autour de l’alimentation tant à la ville qu’à la campagne. L’association compte parmi ses membres tant des habitantEs du Val de Durance que des habitantEs de Marseille où nombre de nos activités prennent sens. Elle fonctionne de façon collective et horizontale et l'engagement s'y fait de manière exclusivement bénévole.<br><br>L'association soutien toute initiative allant dans le sens du partage et de la solidarité.<br><br>",
        url        : 'https://www.facebook.com/LaCaillassecucuron/'
    },
    'La ferme Capri'                                : {
        name       : 'La ferme Capri',
        description: 'Ferme urbaine située dans le 15ème arrondissement. Elle vise à produire et alimenter localement mais aussi à initier et expérimenter.',
        url        : 'http://www.cite-agri.fr/portfolio/ferme-capri/'
    },
    'Le Collet des Comtes'                          : {
        name       : 'Le Collet des Comtes',
        description: '📍137 Bd des Libérateurs | 13012 <br><br>La ferme a été construite en 1855, époque de la fin de la construction du Canal de Marseille qui a rendu possible l’irrigation des terres environnantes et les cultures maraîchères destinées à alimenter la ville. Avant cette date, toute la région était très aride, comme on peut le voir dans les films « Jean de Florette » ou « Manon des Sources ». Dans les années 1980, la Ville de Marseille a racheté la propriété pour la préserver des promoteurs éventuels et l’a transformée en ferme pédagogique en 1988. Aujourd’hui, la ferme est réservée aux groupes scolaires mais elle est ouverte gratuitement au public toute l’année, les samedi matin et les mardi de 16h30 à 19h, pour des visites en famille ou la vente de légumes bio. Des activités et des animations pour enfants sont aussi organisées en dehors des horaires scolaires.<br><br>',
        url        : 'https://ferme-pedagogique-collet-des-comtes.fr/'
    },
    "Ferme du Roy d'Espagne"                        : {
        name       : "Ferme du Roy d'Espagne",
        description: "📍Rue Jules Rimet | 13009 <br><br>Nous sommes un collectif sans hiérarchie d'une dizaine de personnes. Nous vivons de l'agriculture paysanne et de la sensibilisation à l'environnement sur la ferme municipale du Roy d'Espagne à Marseille.<br><br>Ouverture au public<br>durant les heures de marché<br>Mercredi après-midi de 16h à 18h30<br>Samedi matin de 09h à 12h30<br><br>",
        url        : 'https://www.fermeduroydespagne.org/'
    },
    'Ferme Pastière (Meryrargues)'                  : {
        name       : 'Ferme Pastière (Meryrargues)',
        description: '🐰 De la fourche à la fourchette !🌾 Production de céréales et légumineuses 🧑‍🌾 Meunerie et fabrication de pâtes 🌱 Bio, Artisanal, Vegan 📍 Meyrargues',
        url        : 'https://miimosa.com/fr/projects/la-ferme-pastiere-lance-le-printemps-des-legumineuses'
    },
    "Ferme de l'Etoile"                             : {
        name       : "Ferme de l'Etoile",
        description: "📍 111 boulevard Notre Dame de Santa Cruz | 13014<br><br> On cultive des fruits et légumes bios, de saison et sans traitements, qu'on propose en paniers 🌱<br><br>",
        url        : 'https://www.facebook.com/people/La-ferme-de-lEtoile-Marseille/100068154728538/'
    },
    'Le Talus'                                      : {
        name       : 'Le Talus',
        description: "tiers-lieu marseillais de partage, de découverte et d'expérimentation d'une nouvelle façon de vivre et d'aménager la ville.",
        url        : 'https://www.letalus.com/'
    },
    'La Bastide à Fruits'                           : {
        name       : 'La Bastide à Fruits',
        description: '📍Allée Archam Babayan | 13012 | @bastidefruits <br><br> Verger partagé au cœur de Marseille',
        url        : 'https://www.vvoum.org/'
    },
    Squats                                          : {
        name       : 'Squats',
        description: '',
        url        : undefined
    },
    'Le Snack'                                      : {
        name       : 'Le Snack',
        description: '',
        url        : ''
    },
    'GSN Géstion de Conflits'                       : {
        name       : 'GSN Géstion de Conflits',
        description: '',
        url        : ''
    },
    'La Tarantula'                                  : {
        name       : 'La Tarantula',
        description: '',
        url        : ''
    },
    'Lieux de Répit'                                : {
        name       : 'Lieux de Répit',
        description: '',
        url        : undefined
    },
    'Lieu de Répit'                                 : {
        name       : 'Lieu de Répit',
        description: "Un projet expérimental d'alternative à l'hospitalisation psychiatrique",
        url        : 'https://commedesfous.com/lieu-de-repit/'
    },
    'Refuge | Hébergement'                          : {
        name       : 'Refuge | Hébergement',
        description: '',
        url        : undefined
    },
    Palama                                          : {
        name       : 'Palama',
        description: "📍 13013<br><br>Palama est un cabanon isolé dominant Marseille accueillant le public librement durant la journée ou pour la nuit sur réservation.<br><br>Les notions d’institution artistique, de bien commun, de machine à penser, de patrimoine négligé ou d’équilibre bioclimatique sont abordées à travers le lieu lui même rénové par Stephane Barbier Bouvet et des invitations faites pour Manifesta: Allison Katz, Camilla Wills et Boy Vereecken. <br><br>Palama est un projet pérenne lancé en Août 2020.<br><br>En transport en commun, depuis la station de métro La Rose, prendre le bus 5 jusqu'au terminus La Parade. Continuer à pied jusqu'au cabanon (40 minutes).<br>Si vous êtes motorisé, se rendre au parking dans les arbres au sommet du chemin de Palama (là où la route s’arrête) puis continuer à pied (15 minutes) en suivant le chemin après la barrière verte de la forêt.",
        url        : 'https://manifesta13.org/fr/projects/6993/index.html'
    },
    'Santé'                                         : {
        name       : 'Santé',
        description: '',
        url        : undefined
    },
    'Le Spot-Longchamp'                             : {
        name       : 'Le Spot-Longchamp',
        description: "Le Spot est un lieu prioritairement dédié aux hommes ayant des relations sexuelles entre hommes, aux personnes migrantes, aux personnes trans, aux usagers-ères de drogues et aux travailleurs-euses du sexe. Nous te proposons un parcours de santé sexuelle en t'accordant du temps, un espace de parole libre et non jugeant, une offre médico-sociale et un accompagnement communautaire adaptés à tes besoins.",
        url        : 'https://longchamp.lespot.org/le-spot'
    },
    'LE CHÂTEAU EN SANTÉ'                           : {
        name       : 'LE CHÂTEAU EN SANTÉ',
        description: "<b>Centre de santé communautaire</b><br><br>Le centre de santé est porté par une association à but non lucratif et s'adresse aux habitants des quartiers de Kalliste, la Granière, la Solidarité, les Bourrely.<br>Il propose des consultations de médecine générale, des entretiens sociaux ou infirmiers, un suivi orthophonique dans la limite des places disponibles.<br>Ce lieu est aussi un espace de rencontre autour d'un café, d'échanges sur la santé, sur le « prendre soin », de réflexions collectives, avec les habitants et les professionnels du territoire sur les besoins en santé du territoire, sur le fonctionnement d'une telle structure, sur les moyens de favoriser l'accès aux soins et de lutter contre les inégalités sociales de santé.<br><br ><ul><li>Consultations de médecine générale</li><li> entretiens sociaux</li><li> entretiens infirmiers </li><li> orthophonie</li><li>accueil thé ou café</li></ul>📍Parc Kalliste | 10 impasse Michel Crespin | 13015<br>📞 04 91 75 84 20<br>✉️ contact@chateau-en-sante.org<br>",
        url        : 'https://www.chateau-en-sante.org/'
    },
    'Collectifs | Assos | Projets'                                    : {
        name       : 'Macro, micro et infrapersonnel',
        description: "Nous considérons indispensable de distinguer et d’articuler les trois différents « terrains » d’action pour la transformation sociale : Le macro-politique, le micro-politique et l’infra-personnel. Les distinguer en soulignant leur imbrication. <br><br>Le niveau macro-politique concerne les grands ensembles humains : villes, régions, pays et monde entier. C’est là que l’on agit lorsqu’on s’oppose à une politique gouvernementale ou lorsqu’on manifeste pour un cessez-le-feu à Gaza. Le niveau micro-politique, lui, désigne tout ce qui nous est le plus immédiat : les relations de voisinage, de famille, d’amitié, d’amour, nos collectifs, coopératives et associations, nos réseaux, nos projets collectifs et individuels, la gestion de conflits, etc. Aux niveaux précédents, nous voulons articuler sérieusement la dimension que nous appelons « nano-politique » ou « infra-personnelle », concernant la société à « l’intérieur » de soi : nous sommes traversé·es par la société, même  dans les endroits les plus intimes. Le travail politique à ce niveau consiste à déconstruire et transformer les « logiciels » socialement produits qui opèrent dans nos cœurs, nos tripes et nos cerveaux. Même si l’on partage un ensemble de constructions sociales et d’affects, ce travail est nécessairement singulier, chaque personne étant comme un territoire où les montagnes et les vallées, les rivières et les rochers, les entrées et les sorties, les cloisonnements et les rigidités, sont singulièrement disposés, agencés. Les méthodes et les pratiques ne sont pas les mêmes que celles que l’on emploierait pour agir vis-à-vis du macro ou du micro-politique. Chacun·e son territoire, et pour chaque territoire une géopolitique émancipatrice singulière. Le travail individuel à ce niveau est très limité, d’une part parce que l’on a toujours besoin d’un point de vue/regard/analyse extérieure, et d’autre part en raison d’un manque généralisé de sensibilités et compétences nécessaires pour ce type de travail. Le travail collectif à ce niveau dépendra de notre capacité, d’une part, à cultiver ces sensibilités-compétences, et d’autre part, à mettre en place des dispositifs dédiés spécifiquement à ce type d’accompagnement hautement politique, individuellement et socialement thérapeutique, où l’identification et la catalysation des singularités sont essentielles. Nous considérons et soulignons que la production de nouveaux possibles, la guérison sociale et culturelle, passe nécessairement par une articulation sérieuse d'un travail sur ces trois niveaux. Dès le moment où l’on néglige l’un ou plusieurs d’entre eux, comme c’est globalement le cas concernant l’infra-personnel, nous nous mettons « hors-jeux », nous faisons bouffer par le système, finissons par le nourrir et le reproduire tout en luttant contre et le dénonçant. ",
        url        : undefined
    },
    Macropolitique                                  : {
        name       : 'Macropolitique',
        description: '',
        url        : undefined
    },
    'Bataille Médiatique'                           : {
        name       : 'Bataille Médiatique',
        description: '',
        url        : undefined
    },
    'Journaux Indépendants'                         : {
        name       : 'Journaux Indépendants',
        description: '',
        url        : undefined
    },
    Marsactu                                        : {
        name       : 'Marsactu',
        description: "Créé en 2010, Marsactu a été racheté en 2015 par ses journalistes. Nous avons fait le choix de renoncer à la publicité et aux subventions des collectivités locales. Nous vivons des abonnements de nos lecteurs.Les lecteurs peuvent participer par les commentaires ou par des posts de blog dans l'Agora.",
        url        : 'https://marsactu.fr/'
    },
    'La terre en Thiers'                            : {
        name       : 'La terre en Thiers',
        description: 'Journal des élèves du lycée Thiers à Marseille',
        url        : 'https://www.laterreenthiers.org/'
    },
    'Chaines Youtube'                               : {
        name       : 'Chaines Youtube',
        description: '',
        url        : ''
    },
    Blogs                                           : {
        name       : 'Blogs',
        description: '',
        url        : ''
    },
    Blocage                                         : {
        name       : 'Blocage',
        description: '',
        url        : undefined
    },
    Zads                                            : {
        name       : 'Zads',
        description: '',
        url        : undefined
    },
    'Zone à Patates'                                : {
        name       : 'Zone à Patates',
        description: 'Expulsée',
        url        : undefined
    },
    'S.d.l.Terre'                                   : {
        name       : 'S.d.l.Terre',
        description: 'Mouvement Soulèvements De La Terre',
        url        : undefined
    },
    'Comité Local 13'                               : {
        name       : 'Comité Local 13',
        description: 'soulevementsdelaterre13@riseup.net',
        url        : undefined
    },
    'Face aux lois/politiques Gouv.'                : {
        name       : 'Face aux lois/politiques Gouv.',
        description: '',
        url        : undefined
    },
    Autonomes                                       : {
        name       : 'Autonomes',
        description: '',
        url        : undefined
    },
    'AG-InterPro'                                   : {
        name       : 'AG-InterPro',
        description: '',
        url        : undefined
    },
    'Marseille vs Darmanin'                         : {
        name       : 'Marseille vs Darmanin',
        description: '',
        url        : undefined
    },
    'CoMob-StCharles'                               : {
        name       : 'CoMob-StCharles',
        description: '',
        url        : undefined
    },
    'Urgence Palestine'                             : {
        name       : 'Urgence Palestine',
        description: '✊🏽✊🏻✊🏿✊🏼 Coordination Citoyenne Urgence Palestine : STOP à l’occupation, au colonialisme et à l’Apartheid',
        url        : 'https://linktr.ee/UrgencePalestineMarseille13'
    },
    Syndicats                                       : {
        name       : 'Syndicats',
        description: '',
        url        : undefined
    },
    'UNEF-13'                                       : {
        name       : 'UNEF-13',
        description: "UNION ETUDIANTE AIX-MARSEILLE<br> L'Union Nationale des Etudiants de France est le premier syndicat étudiant de France. Fondé en 1907 et reconnu d'utilité publique le 16 mai 1929, il a marqué l'histoire de notre pays et il est à l'origine des plus grandes conquêtes du monde étudiant, comme le système de bourse sur critère sociaux, la création des restaurants universitaires et des CROUS.<br><br>📞09 86 06 99 63 <br>✉️ contact@union-etudiante-13.fr",
        url        : 'https://union-etudiante-13.fr/'
    },
    'Confédération Paysanne PACA'                   : {
        name       : 'Confédération Paysanne PACA',
        description: "La Confédération paysanne, syndicat pour une agriculture paysanne et la défense de ses travailleurs. La Confédération paysanne Provence-Alpes Côte d'Azur fédère les syndicats des 6 départements de la région. Le comité régional, composé de 3 paysan-ne-s par département, est l'instance décisionnelle de la confédération régionale. ",
        url        : 'https://paca.confederationpaysanne.fr/index.php'
    },
    'Partis'                             : {
        name       : 'Partis',
        description: '',
        url        : undefined
    },
    NUPES13                                         : {
        name       : 'NUPES13',
        description: '',
        url        : undefined
    },
    NPA13                                           : {
        name       : 'Nouveau Parti Anticapitaliste',
        description: "npa13@npa-mail.org <br><br>Le NPA est un mouvement politique qui a tenu son congrès de fondation en févier 2009. Plus que jamais, au moment où la triple crise économique, sociale et écologique s'approfondit et s'accélère, notre but est de rassembler toutes les forces qui veulent sortir de l'ère du profit, rompre avec le capitalisme, pour ouvrir la voie à une société inédite, démocratique et égalitaire, féministe et écologiste.<br><br>Cette société capitaliste est à bout de souffle. Les gouvernements successifs remettent en cause l’ensemble de nos acquis sociaux et démocratiques. Le MEDEF licencie à tour de bras. Les services publics sont sacrifiés. Nos conditions de vie se dégradent. Les riches se gavent de profits pendant que nos salaires, nos pensions de retraites, nos minima sociaux stagnent ou régressent, notre pouvoir d'achat diminue. La crise financière que les grands actionnaires, le patronat et le gouvernement veulent nous faire payer aggrave chaque jour les conditions de vie de la majorité de la population. Le racisme et les discriminations augmentent. La planète est menacée de destruction.<br><br>Face à ces attaques, la gauche institutionelle ne résiste pas. Quand elle se trouve au pouvoir, elle mène des politiques qui tournent le dos aux aspirations populaires. Nous avons besoin d’une gauche de combat, fière de ses couleurs, qui n’a pas peur de s’affronter à une minorité d’actionnaires qui détient la majorité des richesses.<br><br>Nous construisons un nouveau parti pour résister face au gouvernement et au patronat, lutter contre la droite et l'extrême droite, contre ceux qui exploitent, précarisent, détruisent l'environnement.  Un parti qui permette d’en finir avec  le racisme, le sexisme... Un parti qui défende un projet de transformation révolutionnaire de la société, pour une rupture radicale avec ce système dont nous n’avons rien à attendre.<br><br>La politique n’est pas réservée à une minorité de spécialistes. Elle est l’affaire de tous. Par milliers, nous avons commencé à nous organiser le plus largement possible, dans des comités de quartiers ou  d’entreprises, dans les villes et les villages.<br><br>Faisons grandir le mouvement ! Construisons ensemble le NPA !",
        url        : "https://npa-lanticapitaliste.org/"
    },
    LFI13                                           : {
        name       : 'LFI13',
        description: "<h4>La France insoumise est un mouvement évolutif</h4>Il cherche à inventer une nouvelle forme de rassemblement citoyen. Ces principes d’organisation sont donc précisés au fur et à mesure de son évolution, à partir des retours d’expérience, notamment au moment des conventions annuelles.<h4>La France insoumise est un mouvement humaniste</h4>Il promeut l’émancipation globale de la personne humaine, la souveraineté populaire, la justice sociale, la laïcité, l’écologie, l’harmonie entre les êtres humains et leur écosystème. Il noue des relations avec les mouvements d’autres pays qui agissent dans le même sens et travaille de concert avec celles et ceux qui partagent ses objectifs.<h4>La France insoumise est un mouvement ouvert et populaire</h4>Il ne demande ni carte d’entrée ni cotisation et rassemble toutes celles et ceux qui agissent pour promouvoir le programme « L’avenir en commun ». Il met en œuvre la parité. Il favorise l’implication des personnes avec des faibles ressources financières en faisant vivre la solidarité interne. Il facilite l’action de chacun·e en son sein en proposant des formations s’appuyant sur les méthodes de l’éducation populaire.<h4>LA FRANCE INSOUMISE EST UN MOUVEMENT BIENVEILLANT ET INCLUSIF</h4>Les compétitions internes, les conflits de personnes et les affrontements de courants n’y ont pas leur place, tout comme les propos ou les comportements violents, sexistes, racistes, antisémites ou LGBTIphobes. Les prises de décisions au consensus sont privilégiées afin d’éviter les écueils liés aux clivages et aux mises en minorité. Le mouvement expérimente progressivement les modalités démocratiques les plus adaptées à cette orientation. La création et la mise en place du comité de suivi contre les violences sexistes et sexuelles vient renforcer l’application de ce principe.<h4>LA FRANCE INSOUMISE EST UN MOUVEMENT CULTUREL</h4>Face au délitement des sociétés auquel conduisent les politiques libérales, il ne limite pas son action au champ politique mais l’étend dans toutes les sphères pour créer du commun et faire société. Il promeut et relaie des outils d’information alternative pour lutter contre l’idéologie dominante et s’engage dans les expérimentations concrètes d’un nouveau monde<h4>LA FRANCE INSOUMISE EST UN MOUVEMENT TOURNÉ VERS L’ACTION</h4>Son activité est organisée en campagnes d’actions sur l’ensemble du territoire. Son énergie, ses savoir-faire, ses compétences et ses moyens financiers sont mis au service de l’action dans la société à travers l’utilisation du réseau social Action Populaire facilitant la mise en mouvement et le partage des expériences.<h4>LA FRANCE INSOUMISE EST UN MOUVEMENT UTILE TOUT DE SUITE</h4>Une partie significative de son activité est consacrée à des actions de solidarités concrètes. Il impulse et soutient des dynamiques d’auto-organisation populaire à partir des colères et des indignations quotidiennes. Il appuie les mobilisations collectives de la société qui défendent des objectifs similaires aux siens.<h4> LA FRANCE INSOUMISE EST UN MOUVEMENT RÉSEAU</h4>Il fédère des groupes d’action qui se constituent par la volonté des insoumis·es sur une base géographique ou professionnelle. Ces groupes s’organisent selon une charte définie collectivement et disposent d’une autonomie d’action dans le respect du programme « L’avenir en commun ».<h4>LA FRANCE INSOUMISE EST UN MOUVEMENT COLLECTIF ET TRANSPARENT</h4>Ses principales orientations et campagnes sont déterminées par une consultation en ligne des insoumis·es organisée régulièrement, notamment à l’occasion de ses conventions annuelles. Il promeut une multiplicité de formes de participation permettant à chacun·e d’être utile dans le respect de son rythme et de son degré d’engagement. L’utilisation de ses ressources financières est publique.<h4>LA FRANCE INSOUMISE EST UN MOUVEMENT POLYCENTRIQUE</h4> Il compte plusieurs espaces, ouverts et joignables par toutes et tous et s’organise autour de deux structures nationales : <ul><li>La coordination des espaces, qui assure la réalisation de nos campagnes, actions et événements.</li><li>Le conseil politique, espace d’échanges sur les orientations stratégiques et les campagnes du mouvement.</li><li>L’Assemblée représentative, qui réunit des membres de ces différents espaces nationaux et des insoumis·es volontaires tiré·es au sort, notamment au sein des groupes d’action.</li></ul> À ces structures s’ajoutent trois comités afin de faire respecter les principes de La France insoumise, de désigner les candidat·es investi·es par le mouvement et de lutter contre les violences sexistes et sexuelles.<br><br>Enfin la France insoumise participe aussi de la Nouvelle Union Populaire écologique et sociale (NUPES) - mise  à juour : NFP (Le Nouveau Front Populaire)<br><br> La dernière Assemblée représentative du mouvement a eu lieu le 10 décembre 2022. L’organigramme ci-dessus est en évolution permanente et différentes pages et fonctionnalités complémentaires seront ajoutés au fur et à mesure de leur constitution : plus d’informations.",
        url        : "https://lafranceinsoumise.fr/"
    },
    'Rev.Permanente13'                              : {
        name       : 'Rev.Permanente13',
        description: "Révolution Permanente est une organisation politique révolutionnaire fondée officiellement en décembre 2022. Depuis 2015, ses militants animent le quotidien en ligne du même nom. Celui-ci traite l’actualité politique et les luttes nationales et internationales avec un point de vue assumé : du côté des travailleurs, de la jeunesse, des femmes, des personnes LGBT, des quartiers populaires et de tous les exploités et opprimés.<br><br>Révolution Permanente veut également faire entendre leur voix. Depuis les piquets de grève, les lieux d’études ou les manifestations, nous leur donnons la parole. De même, nos pages sont ouvertes à toutes celles et ceux qui veulent contribuer à faire vivre ce projet en devenant correspondants du site ou en envoyant des témoignages sur les luttes, les conditions de vie et de travail et le quotidien de ceux qui font tourner l’ensemble de la société.<br><br>Révolution Permanente s’inscrit dans le projet plus large de redonner une vitalité aux idées marxistes et révolutionnaires, en démontrant qu’elles n’ont pas vocation à rester confinées dans les bibliothèques, les caves ou les musées, mais gardent au contraire toute leur actualité pour comprendre le monde dans lequel nous vivons et former de nouvelles générations militantes prêtes à le transformer.<br><br>Révolution Permanente fait partie du réseau international de quotidiens en ligne La Izquierda Diario, présent dans 14 pays et en 8 langues, et impulsé par les différentes organisations de la Fraction Trotskyste – Quatrième Internationale. A l’image de chacun de ces journaux, nous entendons contribuer aux débats au sein de l’extrême-gauche nationale et internationale, dans le cadre de notre lutte pour reconstruire une internationale de la révolution socialiste, la IVème Internationale.<br><br>Après avoir défendu les idées révolutionnaires au sein du Nouveau Parti Anticapitaliste pendant plus d’une décennie, Révolution Permanente est désormais une organisation politique indépendante. Notre objectif est de contribuer à la reconstruction d’une gauche révolutionnaire à la hauteur des expériences de la lutte des classes des dernières années, des mobilisations contre la loi travail en 2016 à la grande grève contre la réforme des retraites de Macron en 2019, en passant par les gilets jaunes, les nombreuses mobilisations contre le racisme et les violences policières, pour les droits des femmes et des personnes LGBT ou encore contre la destruction de l’environnement, ou encore la bataille des retraites de 2023. Une nécessité face aux défis que cette nouvelle phase de crise capitaliste internationale pose à l’ensemble des travailleurs et travailleuses en France et dans le monde.",
        url        : 'https://www.revolutionpermanente.fr/Marseille-201'
    },
    'Face au système Financier'                     : {
        name       : 'Face au système Financier',
        description: '',
        url        : undefined
    },
    'Banques Alternatives'                          : {
        name       : 'Banques Alternatives',
        description: '',
        url        : undefined
    },
    'La NEF'                                        : {
        name       : 'La NEF',
        description: '',
        url        : 'https://www.lanef.com/'
    },
    'Monnaies Locales'                              : {
        name       : 'Monnaies Locales',
        description: '',
        url        : undefined
    },
    Cryptomonnaies                                  : {
        name       : 'Cryptomonnaies',
        description: 'Ici on affiche les projets se revendiquant de gauche',
        url        : undefined
    },
    'La Ĝ (June)'                                   : {
        name       : 'La Ĝ (June)',
        description: "La monnaie libre place l'être humain au cœur de l'économie, car il devient seule source de création monétaire.",
        url        : 'https://monnaie-libre.fr/'
    },
    'Face au Néo-colonialisme'                      : {
        name       : 'Face au Néo-colonialisme',
        description: 'et au racisme',
        url        : undefined
    },
    'Survie - PACA'                                 : {
        name       : 'Survie - PACA',
        description: "Survie est une association qui lutte contre le néocolonialisme français en Afrique sous toutes ses formes, la Françafrique. Elle milite aussi autour de situations toujours coloniales comme en Kanaky Nouvelle-Calédonie ou à Mayotte. Grâce à la mobilisation de ses membres et de groupes locaux dans toute la France, l'association produit un travail d'enquête et d'analyse critique, et propose des modalités d'action variées. Elle rassemble les personnes qui désirent s'informer, se mobiliser et agir.",
        url        : 'https://survie.org/'
    },
    'Face au Fascisme'                              : {
        name       : 'Face au Fascisme',
        description: '',
        url        : undefined
    },
    'Riposte Antifa'                                : {
        name       : 'Riposte Antifa',
        description: '',
        url        : undefined
    },
    'Brigade Antifa'                                : {
        name       : 'Brigade Antifa',
        description: '',
        url        : undefined
    },
    'Colletif Antifa'                               : {
        name       : 'Colletif Antifa',
        description: '',
        url        : undefined
    },
    'Antifa Social Club'                            : {
        name       : 'Antifa Social Club',
        description: '',
        url        : undefined
    },
    'Assemblées Antifa'                             : {
        name       : 'Assemblées Antifa',
        description: '',
        url        : undefined
    },
    'Face à la Psychiatrie'                         : {
        name       : 'Face à la Psychiatrie',
        description: '',
        url        : undefined
    },
    'Fondation ERIE'                                : {
        name       : 'Fondation ERIE',
        description: "Créée en 2019, la Fondation Erié est née de la volonté d'une famille de s'unir autour d'un projet à fort impact. La fondation dédie son action à la santé mentale.<br>Elle soutient des projets qui font évoluer la prévention, la prise en charge, la recherche et le regard porté sur les troubles psychiques.<br>Elle porte également l'ambition de témoigner des réalités du terrain et de faire du plaidoyer pour l'évolution des politiques publiques en faveur d'une santé mentale équilibrée pour tous.",
        url        : 'https://fondation-erie.org/'
    },
    AiLSi                                           : {
        name       : 'AiLSi',
        description: "Le programme « Alternative à l'incarcération pour le logement et le suivi intensif » (AiLSi), couvert par le contrat à impact signé ce jour, vise à favoriser le rétablissement sanitaire et social de prévenus vivant avec des troubles psychiatriques sévères, sans logement, afin de lutter contre le parcours « prison – rue – hébergement – hôpital ». Le programme rend possible un prononcé d'alternative au mandat de dépôt en fournissant des garanties de représentation par l'accès à un logement et un suivi intensif pluridisciplinaire orienté vers le rétablissement. D'une durée de cinq ans, il se déploiera sur la juridiction de Marseille. Cette expérimentation sera considérée comme un succès si le programme AiLSi permet de réduire le taux de réincarcération des bénéficiaires et si le coût de cet accompagnement innovant est globalement inférieur aux coûts qui auraient été générés par ces bénéficiaires sans cet accompagnement.",
        url        : 'https://www.medecinsdumonde.org/press_release/programme-ailsi-alternative-a-lincarceration-pour-le-logement-et-le-suivi-intensif/'
    },
    ' Face à la Gentrification'                     : {
        name       : ' Face à la Gentrification',
        description: '',
        url        : undefined
    },
    'Collectif Anti-Gentrification'                 : {
        name       : 'Collectif Anti-Gentrification',
        description: '',
        url        : undefined
    },
    'Face à la Surveillance'                        : {
        name       : 'Face à la Surveillance',
        description: '',
        url        : undefined
    },
    'F. au Système Alimentaire'                     : {
        name       : 'F. au Système Alimentaire',
        description: '',
        url        : undefined
    },
    'Riposte Alimentaire'                           : {
        name       : 'Riposte Alimentaire',
        description: '<b>Exigeons une sécurité sociale de l’alimentation durable</b><br><br>Aujourd’hui, notre système alimentaire est défaillant. Il échoue à remplir des objectifs fondamentaux tels que garantir les besoins de la population, respecter les limites planétaires et assurer une vie digne aux paysannes et paysans qui nous nourrissent au quotidien.<br>Il est urgent de nous organiser collectivement pour une société plus juste et résiliente face aux défis actuels.<br><br>Pour changer ce système en profondeur, Riposte Alimentaire exige la mise en place de la Sécurité sociale de l’alimentation durable (SSAD), assortie d’une carte vitale de l’alimentation pour toutes et tous. Nous demandons à l’État de financer les initiatives locales de la SSAD afin de montrer que ce système est viable et qu’il est possible de le déployer à l’échelle nationale.<br><br> Réunions publiques à La Base et au court-circuit 1 semaine sur deux ou chaque semaine en fonction de la dynamique. Inscription ici : https://airtable.com/appm54aPE96KhL5gn/shrvTa9lJWDDsN3ka',
        url        : 'https://ripostealimentaire.fr/'
    },
    Internationnalisme                              : {
        name       : 'Internationnalisme',
        description: '',
        url        : undefined
    },
    CIMK                                            : {
        name       : 'CIMK',
        description: 'Collectif Internationaliste Marseille-Kurdistan - cimk13@riseup.net',
        url        : 'https://serhildan.org/'
    },
    Micropolitique                                  : {
        name       : 'Micropolitique',
        description: '',
        url        : undefined
    },
    'Santé | Soin | Thérapie'                       : {
        name       : 'Santé | Soin | Thérapie',
        description: '',
        url        : undefined
    },
    'Thérapie'                                      : {
        name       : 'Thérapie',
        description: '',
        url        : undefined
    },
    'Pour une Thérapie Transversale'                : {
        name       : 'Thérapie',
        url        : undefined
    },
    'Remèdes,Potions,Médocs'                        : {
        name       : 'Remèdes,Potions,Médocs',
        description: '',
        url        : undefined
    },
    'Sexualité'                                     : {
        name       : 'Sexualité',
        description: '',
        url        : undefined
    },
    Contraception                                   : {
        name       : 'Contraception',
        description: '',
        url        : undefined
    },
    '13ticules'                                     : {
        name       : '13ticules',
        description: 'Collectif Contraception Masculine - Travail avec le Planing Familiale',
        url        : 'https://www.instagram.com/13ticules/?hl=fr'
    },
    'Psycho-Social'                                 : {
        name       : 'Psycho-Social',
        description: '',
        url        : undefined
    },
    COFOR                                           : {
        name       : 'COFOR',
        description: '<b>Le CoFoR est un Centre de Formation au Rétablissement basé à Marseille</b><br> <br> Il s’adresse aux personnes, vivant en région PACA, qui ont ou ont eu des troubles psychiques, qui souhaitent se rétablir et avoir une vie plus satisfaisante.<br> <br> Parce que son approche est différente, le CoFoR est complémentaire aux structures existantes en santé mentale, les personnes en formation sont des étudiant·es. Elles sont actrices de leur rétablissement et contribuent à faire évoluer le projet du CoFoR. <br> <br><b>Venez nous rencontrer !</b><br> À l’occasion d’un focus group<br>un lundi de 14h à 16h à la Cité des associations <br> <br>',
        url        : 'https://www.coforetablissement.fr/'
    },
    GEMs                                            : {
        name       : 'GEMs',
        description: "Groupes d'Entraide Mutuelle",
        url        : "https://fr.wikipedia.org/wiki/Groupe_d'entraide_mutuelle"
    },
    "Lieu d'Échanges et d'Ouvertures LEO"           : {
        name       : "Lieu d'Échanges et d'Ouvertures LEO",
        description: "Destiné à lutter contre la solitude et l'isolement, LEO accompagne les personnes souffrant de troubles psychiques, à recréer du lien social.Cuisine, Mosaïque, Écriture, Informatique, Internet, Sorties voile, Sorties culturelles et de loisirs. Bibliothèque et Ludothèque à disposition. N20 rue Despieds 13003",
        url        : undefined
    },
    'CLUB PARENTHESE'                               : {
        name       : 'CLUB PARENTHESE',
        description: '',
        url        : 'http://www.gemclub.wordpress.com/'
    },
    'Planning Familial 13'                          : {
        name       : 'Planning Familial 13',
        description: "Le Planning Familial est une association féministe et d'éducation populaire, qui s'inscrit dans un mouvement national présent dans plus de 75 départements français. À sa création en 1956, la vocation de ce mouvement était de promouvoir l'accès à la contraception et le droit à l'avortement alors qu'ils étaient interdits. L'association a, au fur et à mesure des années, diversifié ses champs d'intervention et participe aujourd'hui à la mise en œuvre de politiques publiques, en particulier dans le domaine de la santé sexuelle et de l'éducation. Mouvement autonome et indépendant, il dénonce et combat toutes les formes de violences, de discriminations et d'inégalités sociales. Il se mobilise pour créer les conditions d'une sexualité libre, épanouie et consentie, vécue sans répression ni dépendance, dans le respect des différences de chacun·e, et pour la reconnaissance du droit fondamental à disposer librement de son corps. Le Mouvement Français du Planning Familial (MFPF) est organisé en une confédération qui coordonne les actions et diffuse l'information au niveau national. Il réunit 13 fédérations régionales, et 76 associations départementales (Hexagone et Outre-mer) et adhère à l'International Planned Parenthood Federation (IPPF). Le Planning Familial 13 a été créé en 1962.",
        url        : 'https://www.leplanning13.org/'
    },
    'IMAJSanté'                                     : {
        name       : 'IMAJSanté',
        description: "Information Marseille Accueil Jeunes Écoute Santé <br><br> Créée en 1999 par des professionnels de la santé partis du constat qu'il n'existait pas de lieux dédiés aux 12-25 ans en mesure de répondre à leurs diverses problématiques, l'association IMAJE Santé a ouvert à Marseille un lieu d'accueil et d'accompagnement où des professionnels reçoivent inconditionnellement les jeunes, dans le cadre d'une démarche de santé globale et positive. Situé en centre-ville de Marseill, au 35, rue Estelle (1er), l'espace est ouvert du lundi au vendredi (sur RV de 9h à 12h puis de 13h à 19h et sans RV de 13h à 18h) Sur place, des psychologues, des infirmiers, des médecins et des travailleurs sociaux sont disponibles pour écouter et répondre aux questions des jeunes et de leurs familles, les informer, les orienter et les accompagner, sur tout ce qui concerne leur santé au sens global. Ces professionnels sont expérimentés et engagés, formés tout au long de leur carrière, fins connaisseurs du jeune public et des acteurs de la jeunesse sur le territoire marseillais. Ils s'appuient sur le respect du libre-choix et une posture non-jugeante qui favorisent la libre expression et l'émergence de solutions adaptées à chaque personne reçue. Ils contribuent par différentes modalités d'intervention, en accord avec le référentiel national des Points Accueil Ecoute Jeunes, à favoriser la santé globale des adolescents et des jeunes adultes de Marseille, notamment les plus vulnérables, permettant ainsi de prévenir les situations à risques, de maintenir le dialogue entre les jeunes et la société, de favoriser leur autonomie et de participer à leur « bien-être total, physique, social et mental ».<br>IMAJE Santé accueille également le public sur rendez-vous à l'occasion de deux permanences psychologiques hebdomadaires dans le 3ème arrondissement et dans le 15ème arrondissement de Marseille.",
        url        : 'https://www.imajesante.fr/lassociation-imaje-sante/'
    },
    'Equipe SIDIIS'                                 : {
        name       : 'Equipe SIDIIS',
        description: "Suivi Intensif pour la Désinstitutionnalisation et l'Inclusion Sociale",
        url        : 'https://www.groupe-sos.org/structure/sidiis/'
    },
    Addictions                                      : {
        name       : 'Addictions',
        description: '',
        url        : undefined
    },
    'Bus 31/32'                                     : {
        name       : 'Bus 31/32',
        description: "Le Bus 31/32, créée en 2006, est une émanation de Médecins du Monde (MDM), dont cette mission avait débutée en 1994 dans le contexte de l'épidémie de VIH. Cette association marseillaise est gestionnaire de deux établissements médicaux-sociaux : un CSAPA (Centre de Soins, d'Accompagnement et de Prévention des Addictions) et un CAARUD (Centre d'Accueil et d'Accompagnement à la Réduction des risques pour Usagers de Drogues). Un accueil dit bas-seuil d'exigence est proposé soit au local, soit sur une unité mobile 7 jours sur 7. Une équipe pluridisciplinaire permet aux personnes usagères de drogues de bénéficier de traitement adapté, de se « poser » un temps ou d'être orienté vers d'autres structures.",
        url        : 'https://annuaire.action-sociale.org/?p=caarud-de-l-asso-bus-31-32-130025018&details=caracteristiques'
    },
    'Médiation | Gestion de Conflits'               : {
        name       : 'Médiation | Gestion de Conflits',
        description: '',
        url        : undefined
    },
    'Collectif WD-40'                               : {
        name       : 'Collectif WD-40',
        description: 'Collectif qui travaille autour de la gestion des situations de conflit et / ou violence. Permanences mensuelles, ateliers, accompagnements, réflexions à plusieurs autour de situations concrètes, travail sur les dynamiques de groupes.<br><br>✉️ WD-40@riseup.net',
        url        : undefined
    },
    'Réseau Gestion_Mars_Conflits'                  : {
        name       : 'Réseau Gestion_Mars_Conflits',
        description: "Fais-tu face à un conflit et tu ressens le besoin de soutien ? Tu peux signaler ce besoin et décrire brièvement ta situation – en faisant attention aux informations sensibles ou compromettantes – , pour proposer un échange/rencontre virtuelle ou physique avec les participants de ce réseau ayant le temps et l'envie de te soutenir en t'écoutant et en réfléchissant avec toi/vous sur les possibles formes d'aborder le conflit/situation, les potentiels risques, les ressources nécessaires, les outils de gestion, la possible articulation avec la thérapie, etc. On fait appel à l'intelligence collective. <br><br> Cette liste mail (gestion_mars_conflits@framagroupes.org) + un groupe signal ont été proposées suite aux plusieurs expériences aux permanences mensuelles et semi-publiques de gestion de conflits animées par le collectif WD-40: un espace mensuel de 3h où l'on peut constater le sens de collectiviser les situations de conflit qui nous dépassent en tant qu'individu·es ou groupes. S'il y a quelques personnes sensibilisées à l'écoute active et/ou aux outils de gestion de conflits, il n'y a pas d'experts aux permanences. Et pourtant, les personnes venues pour traiter une situation sortent moins démunies, avec quelques ressources et des idées et réflexions collectives. <br><br>La liste mail / groupe signal a pour but de mettre en réseau les personnes intéressées par ce terrain micropolitique et/ou ayant besoin de soutien dans leurs situations, pour que l'on puisse signaler un besoin de soutien + faciliter la formation spontanée de groupes de support, éphémères ou pas, pour ne pas nous laisser isolé·es et/ou démunies face aux situations qui nous dépassent.<br><br> Clicker sur «Page web » ↓ pour accèder au tableu (pad) contenant toutes les infos ;)<br><br> ",
        url        : 'https://cryptpad.fr/pad/#/2/pad/view/HGbXSXV4gsr1c57y1Dn52pZW7OZBg7MHKpylXjK4Fqg/'
    },
    AVAD                                            : {
        name       : 'AVAD',
        description: "<b>Association d’Aide aux Victimes d’Actes de Délinquance</b><br><br> L’AVAD est une association loi 1901 créée en 1983 qui développe des actions pour venir en aide aux victimes d’infractions pénales sur le ressort du tribunal de grande instance de Marseille.<br>L’AVAD a pour objet : <ul><li>d'apporter aide et assistance à toute personne, mineure ou majeure, se déclarant victime d'une infraction pénale</li><li>d'exercer des missions d'administrateur ad hoc</li><li>de développer des actions de sensibilisation, de prévention et de formation sur l'aide aux victimes</li><li>d'intervenir auprès de personnes affectées par des infractions pénales violentes génératrices d'un fort retentissement public</li></ul>Afin d’aider au mieux les victimes que nous recevons, nous leur proposons un accueil global et adapté à chaque situation :<ul><li>Une écoute privilégiée par des personnes formées à l’accueil des victimes. Ce temps d’écoute permet un temps de parole et de clarification.</li><li>Des informations et des explications juridiques : par exemple dépôt de plainte, main courante, constitution de partie civile, l’aide juridictionnelle…</li><li>Une aide et un accompagnement juridique dans les démarches à effectuer à tout moment de la procédure : préparation aux expertises, aux audiences de jugement, indemnisation des victimes…</li><li>Un soutien psychologique par des psychologues cliniciens professionnels et formés au suivi du stress post-traumatique.</li><li>Un accompagnement social (démarches médicales, administratives, assurance…).</li><li>Un accompagnement physique et un soutien moral lors des audiences et auprès des partenaires.</li><li>Si nécessaire, une orientation et un lien vers des partenaires relais.</li></ul> Les victimes sont accueillies sur rendez-vous dans le cadre de permanences (Nos permanences).<br><br> Sur saisine des pouvoirs publics, l’AVAD peut également proposer 7 jours sur 7 une prise en charge en urgence aux victimes de certaines infractions (Service d’Aide aux Victimes en Urgence), ou aider des personnes traumatisées par des faits violents ayant eu lieu dans leur environnement (Dispositif d'Intervention et de Soutien aux Personnes affectées par des Actes Violents). L’AVAD est également agréée par la Cour d’appel d’Aix-en-Provence pour exercer des missions d’administrateur ad hoc.<br>L’AVAD est fédérée à France Victimes (ex-Inavem), qui regroupe toutes les associations d’aide aux victimes en France.<br>L’AVAD est conventionnée avec la Cour d’Appel d’Aix-en-Provence et intervient sur le ressort du tribunal de grande instance de Marseille.<br>Elle est principalement financée par des subventions (Cour d’Appel, Préfecture, Région PACA, Département 13, différentes municipalités…)<br><br>📍13 Boulevard de la Corderie | 13007 <br>📞04 96 11 68 80<br>🕐du lundi au vendredi, de 9h à 12h30 et de 13h30 à 18h <br>",
        url        : 'https://www.avad-asso.fr/accueil/'
    },
    'Éducation | Formation'                         : {
        name       : 'Éducation | Formation',
        description: '',
        url        : undefined
    },
    Arts                                            : {
        name       : 'Arts',
        description: '',
        url        : undefined
    },
    'Thêatre'                                       : {
        name       : 'Thêatre',
        description: '',
        url        : undefined
    },
    'Cirque'                                       : {
        name       : '',
        description: '',
        url        : undefined
    },
    "Extrême Jonglerie"                                       : {
        name       : 'Cirque et Jonglerie',
        description: "📍29 Rue Ferrari |13005 <br><br>Créée en 1997, Extrême Jonglerie est une association de bénévoles regroupant amateurs et professionnels autour de la pratique de la jonglerie et des arts du cirque. Elle a pour vocation de donner un espace d’entraînement, de répétition, et de spectacle à ces disciplines dans le centre de Marseille.<br><br>La salle d’entraînement accueille des ateliers tous les soirs de la semaine, et régulièrement des stages, des résidences (sur demande) et des spectacles. Les dates des évènements à venir sont disponibles sur notre fil d’actualités et dans l’agenda de l’association.<br><br>Extrême Jonglerie est avant tout un lieu de pratique des arts du cirque et de la jonglerie. Nous disposons d’une grande salle équipée de tapis d’acrobatie, de matériel de jonglerie, de matériel d’équilibre, d’une slackline, d’un trapèze fixe, d’un tissu aérien, de matériel de scène, etc.<br><br>La salle accueille des ateliers tous les soirs de la semaine, et régulièrement des stages, des résidences, et des spectacles.<br><br>En chiffres : <ul><li>150m2 de surface totale</li><li>50m2 de tapis</li><li>Un espace scène de 6m x 4m</li></ul>",
        url        : "https://www.extreme-jonglerie.com/"
    },

    'Thêatre Autogéré DAR'                          : {
        name       : 'Thêatre Autogéré DAR',
        description: '',
        url        : undefined
    },
    "Thêatre de l'Opprimé·e LABASE"                 : {
        name       : "Thêatre de l'Opprimé·e LABASE",
        description: '',
        url        : ''
    },
    Musique                                         : {
        name       : 'Musique',
        description: '',
        url        : undefined
    },
    'LES RASCASSES'                                 : {
        name       : 'LES RASCASSES',
        description: 'Chorale féministe',
        url        : ''
    },
    'Cantine du Midi'                               : {
        name       : 'Cantine du Midi',
        description: "📍36 rue Bernard | 13003 <br><br><b>Restaurant associatif à la Belle de Mai</b><br><br>Tout le monde est invité à passer au moins une fois de l’autre côté du comptoir et à faire partie de l’équipe de cuisine, soit en proposant une recette, soit en mettant la main à la pâte. <br>Étant donné la situation sanitaire, nous cuisinerons en équipe restreinte avec des personnes souhaitant se former davantage en cuisine, boulangerie et pâtisserie. Le travail en équipe entre un salarié-cuisinier et un bénévole, stagiaire ou personne en formation, nous permettra de transmettre nos savoir-faire culinaires et les normes d’hygiène, et ainsi, d’établir des parcours de formation individuels adaptés.<br>À côté de ça, l’association met en place des ateliers spécifiques (pâtes fraîches, pâtisserie, confitures, etc.) ouverts à tous et des ateliers pour des groupes ciblés (centres sociaux, maisons de retraite, écoles, etc. ) suite à des propositions reçues et qui ont été co-programmées sur la base des besoins et des envies de ces mêmes groupes.<br><br><b>Quant aux ateliers cuisine, ils se déroulent du mardi au vendredi entre 8.30h et 12h. Pour y participer, contacter le 06 52 90 34 98.</b><br><br> <b>Mode d’emploi pour manger à la cantine :</b> <ul><li> Des menus équilibrés avec des produits de saison, issus de l’agriculture locale et de commerce équitable.</li><li>Pour toute personne souhaitant soutenir le projet, personnes isolées, démunies, en télé-travail, à mobilité réduite, entreprises et associations.</li><li>Une option végétarienne ou à la viande/ au poisson. Possibilité d’adaptation aux régimes alimentaires particuliers.</li><li>Prix accessibles de 8 € par menu à emporter et à partir de 10 € livré chez vous.</li><li>Pré-commande sur notre site ou par téléphone (04 91 05 97 03)</li><li>Repas suspendus : Vous pouvez contribuer en faisant un don aux repas suspendus pour des personnes dans le besoin</li><li>Moyens de paiement acceptés : Virement, Espèces, ticket restaurant, troque.</li><li>La vaisselle : prenez vos couverts (si besoin nous pouvons vous fournir aussi), nous emballons le tout dans de boîtes consignés (comptez environ 4 € par repas). Pour des questions d’hygiène nous ne pourrons pas utiliser vos boîtes.</li></ul>",
        url        : "https://cantinedumidi.enchantier.org/"
    },

    'La lutte enchantée': {
        name       : 'La lutte enchantée',
        description: '',
        url        : ''
    },
    'Atelier Palmas(€)' : {
        name       : 'Atelier Palmas(€)',
        description: 'Palmas, flamenco, Casa Consolat',
        url        : ''
    },
    'Cinémas'           : {
        name       : 'Cinémas',
        description: '',
        url        : ''
    },
    'Le Gyptis'         : {
        name       : 'Le Gyptis',
        description: '📍136, rue Loubon |13003 <br><br>Le Gyptis est le cinéma Art & Essai de la Friche la Belle de Mai. Situé au cœur du quartier de la Belle de Mai, tout près de la Friche, il propose une programmation largement ouverte et curieuse ainsi que de nombreux événements : ciné-concerts, rencontres de réalisateurs, avant-premières, ateliers… <br><br>Le Gyptis fut d’abord un cinéma créé en 1913, puis un théâtre, qui abrita la compagnie Chatôt-Vouyoucas à partir de 1987. En 2014 le lieu a été confié à la Friche la Belle de Mai qui, après rénovation, a redonné au Gyptis sa vocation première : Le cinéma. La programmation est assurée par Shellac, distributeur et producteur cinématographique exigeant installé à la Friche. La salle a été rénovée avec le soutien de la Région PACA, de la Ville de Marseille et du CNC et a rouvert ses portes en 2014.',
        url        : 'https://cinemalegyptis.org/'
    },

    'Cinéma'                           : {
        name       : 'Cinéma',
        description: '',
        url        : undefined
    },
    'Porn on Mars festival de porn queer'       : {
        name       : 'Porn on Mars festival de porn queer',
        description: '',
        url        : undefined
    },
    Peinture                                    : {
        name       : 'Peinture',
        description: '',
        url        : undefined
    },
    Artisanat                                   : {
        name       : 'Artisanat',
        description: '',
        url        : undefined
    },
    Couture                                     : {
        name       : 'Couture',
        description: '',
        url        : undefined
    },
    'Atelier Couture (DAR)'                     : {
        name       : 'Atelier Couture (DAR)',
        description: '',
        url        : undefined
    },
    'Le Polygone Étoilé'                        : {
        name       : 'Le Polygone Étoilé',
        description: "LE POLYGONE ÉTOILÉ, ouvert en 2001 par l’association FILM FLAMME, est un espace de création cinématographique qui défend l’accès aux outils de production pour des films qui trouvent difficilement les moyens de leur réalisation dans le contexte de la production industrielle.",
        url        : "https://www.polygone-etoile.com/"
    },
    Politique                                   : {
        name       : 'Politique',
        description: '',
        url        : undefined
    },
    'A2C-AutonomieDeClasse'                     : {
        name       : 'A2C-AutonomieDeClasse',
        description: 'Revue, Formation',
        url        : 'https://www.instagram.com/a2c.autonomiedeclasse/'
    },
    UEEH                                        : {
        name       : 'UEEH',
        description: "<b>Universités d'Été Euroméditerranéennes des Homosexualités - Rencontres autogérées Féministes LesBiGayTransQueerInterAsex</b><br><br>Cette charte propose une définition a minima de l'espace que nous tentons de construire collectivement. Elle vise à ce que chaque participantE sache à quoi iel s'engage et nous te demandons d'en prendre connaissance. Si tu inscris unE copaine, transmets-lui également avant de commencer.<br><br>Les UEEH, Universités d'Été Euro-méditerrannéennes des Homosexualités - Rencontres LesBiGayTransQueerIntersexeAsexuelLEs, sont une expérience féministe, de vie et de gestion collective.<br><br>Cette expression naît d'une volonté partagée d'organisation collective, manifestée par la prise de décisions au consensus plutôt qu'à la majorité, dans l'écoute et le respect de la parole de l'autre.<br><br>Les UEEH sont un événement autogéré, ce qui signifie qu'elles reposent sur l'implication de toustes les participantEs pour assurer son bon déroulement tant au niveau logistique que dans le contenu qu'on y échange.<br><br>Les UEEH favorisent la rencontre de personnes d'origines sociales, culturelles,… variées, dans un esprit de mixité. Elles réunissent donc des personnes de cultures militantes très différentes : il ne s'agit pas de créer un espace pour militantEs expérimentéEs, mais de s'ouvrir à toute personne disponible à l'expérience que nous proposons.<br><br> Les UEEH sont un lieu de confrontation d'opinions, d'éducation populaire et d'auto-éducation.<br><br>Nous cherchons à créer un espace propice à la prise de conscience que nous sommes toustes porteurEuses d'angles morts et de préjugés, ainsi qu'à la remise en question de nos propres a priori. Une telle situation implique une certaine humilité par rapport à nos propres connaissances, une certaine disponibilité à l'autre et, sans doute, une certaine confiance dans les UEEH elles-mêmes.<br><br>Les UEEH s'adressent à des personnes et/ou des communautés qui ne se reconnaissent pas dans les catégories binaires de genre, notamment celles qui se définissent comme lesbiennes, gays, bi, trans', queer, intersexes, asexuelLEs… Les UEEH visent à construire une expression collective de ces communautés, indépendamment des hiérarchies et des autorités. Elles privilégient l'expression personnelle de chacunE sur soi-même, son propre vécu et son propre genre. Elles reconnaissent et respectent un principe intangible d'autodétermination des personnes.<br>Dans un tel lieu, les discriminations sont combattues par principe, collectivement et individuellement, quelle que soit leur nature ou leur mode de réalisation.<br><br>Au sein des UEEH, des outils sont mis en place pour gérer collectivement certains actes et/ou comportements abusifs, comme les violences sexuelles, conjugales, physiques… mais aussi pour gérer des violences liées à des oppressions systémiques, notamment liées à l'orientation sexuelle, l'identité de genre, le racisme, le validisme, l'âgisme ou le sexisme (voir Les statuts des UEEH, art. II). Le déroulement de cette gestion est détaillé à l'article VIII des Statuts des UEEH.<br><br>« Plus que d'initier ou de fédérer, les UEEH coordonnent, mettent en relation et créent un espace libre d'échange et de création. Les participantEs y proposent et développent elleux-mêmes leurs projets. Les UEEH tentent de constituer un espace-temps où tout le monde puisse exprimer son désir et le partager, dans le respect de chacunE. Chaque personne y participe à son rythme, à sa mesure, et nourrit ce moment afin qu'il représente notre richesse et notre potentiel. Un lieu mixte où être ensemble se fait ensemble !»<br><br>",
        url        : 'https://www.facebook.com/UEEH.net'
    },
    'Genre & Sexualités'                        : {
        name       : 'Genre & Sexualités',
        description: '',
        url        : undefined
    },
    'Masculinités'                              : {
        name       : 'Masculinités',
        description: '',
        url        : undefined
    },
    Manoeuvre                                   : {
        name       : 'Manoeuvre',
        description: "Manœuvre est un collectif mixte sur le genre né en octobre 2020 qui a pour raison d'être de créer des espaces de libre parole, de réflexion critique et d'action concrète dans le champ des masculinités.",
        url        : 'https://manoeuvre-collectif.notion.site/Man-uvre-c194525622f648f7a329cc7b13399b6c'
    },
    'Ateliers Nebula'                           : {
        name       : 'Ateliers Nebula',
        description: '',
        url        : undefined
    },
    'Gestion de Crises'                         : {
        name       : 'Gestion de Crises',
        description: '',
        url        : undefined
    },
    'Ateliers SPAAM'                            : {
        name       : 'Ateliers SPAAM',
        description: '',
        url        : undefined
    },
    'Écoute Active'                             : {
        name       : 'Écoute Active',
        description: '',
        url        : undefined
    },
    Hypnose                                     : {
        name       : 'Hypnose',
        description: '',
        url        : undefined
    },
    'Modules COFOR'                             : {
        name       : 'Modules COFOR',
        description: '',
        url        : undefined
    },
    'Bien Être'                                 : {
        name       : 'Bien Être',
        description: '',
        url        : undefined
    },
    'Vivre Avec'                                : {
        name       : 'Vivre Avec',
        description: '',
        url        : undefined
    },
    'Rétablissement'                            : {
        name       : 'Rétablissement',
        description: '',
        url        : undefined
    },
    Droit                                       : {
        name       : 'Droit',
        description: '',
        url        : undefined
    },
    Navigation                                  : {
        name       : 'Navigation',
        description: '',
        url        : undefined
    },
    Autres                                      : {
        name       : 'Autres',
        description: '',
        url        : undefined
    },
    'Solidarité | Entraide'                     : {
        name       : 'Solidarité | Entraide',
        description: '',
        url        : undefined
    },
    'Cultures Du Cœur 13'                       : {
        name       : 'Cultures Du Cœur 13',
        description: "📍32 rue de Crimée | 13003<br><br> Le projet de Cultures du Cœur s’adresse aux personnes et familles en difficulté. Il s’appuie sur la conviction que la culture constitue un véritable outil de formation personnelle et d’apprentissage de la citoyenneté. <br><br><b>Concrètement, Cultures du Cœur sollicite les organismes culturels afin qu’ils mettent des invitations à disposition des publics qui sont exclus de la culture pour des raisons économiques, sociales ou culturelles.</b>Les places offertes sont mises en ligne sur le site de l’association Cultures du Coeur. <br><br>Pour s’assurer de sensibiliser l’ensemble des publics concernés par son action, Cultures du Cœur travaille avec les structures sociales suivantes : cellules d’appui au RSA, associations de quartier, missions locales, CCAS, organismes de formation, S.Psychothérapie Institutionnelle.P, PJJ, foyers d’enfants, centres sociaux, Maisons Pour Tous, centres d’hébergement d’urgence et de réinsertion, associations luttant contre l’exclusion et la dégradation du lien social sous toutes ses formes…",
        url        : "https://culturesducoeur13.fr/"
    },
    'Exilées'                         : {
        name       : 'Exilées',
        description: '',
        url        : undefined
    },
    'Le GR1'                         : {
        name       : 'Le GR1',
        description: "📍10 Boulevard Barthelemy | 13009<br><br> Le GR1 est un lieu ressource solidaire pour les jeunes exilé·e·s co-porté par Médecins Sans Frontière, la Ligue de l’Enseignement, JUST et Yes We Camp, dans un bâtiment appartenant au Secours Catholique dans le 9e arrondissement de Marseille.<br><br>Le GR1 vise à offrir une alternative à la rue, à répondre aux besoins primaires des mineur·e·s non accompagné·e·s (manger, se reposer, se doucher, charger son téléphone, laver ses vêtements, …) et à leur apporter un soutien pluridisciplinaire dans l’accès à la santé, à la scolarité, aux droits et à des activités d’éducation populaire.<br><br>Ce lieu a aussi pour vocation de favoriser l’entraide en offrant un espace d’intervention aux collectifs solidaires, bénévoles et associations qui soutiennent les jeunes exilé·e·s en situation d’errance sur le territoire (mise à disposition d’espaces, mutualisation des ressources,…).<br><br>Le GR1 a également pour principe d’accueillir d’autres usages et usagers dans une démarche de création de lien social, avec la mise à disposition d’espaces de travail à des associations et des artistes, dans les 13 bureaux situés dans les étages du bâtiment, et l’accueil d’une programmation culturelle et artistique autour de temps forts, ouverts aux jeunes et aux habitants des quartiers riverains. <br><br> <b>Ouvert au public du mardi au vendredi de 11h à 18h30</b>",
        url        : "https://yeswecamp.org/le-gr1/"
    },
    'Entrerpises | Cooperatives'                         : {
        name       : 'Entrerpises | Cooperatives',
        description: '',
    },
    'La Ruche'                         : {
        name       : 'La Ruche',
        description: "La Ruche® Marseille et Inter-Made mutualisent 5 pôles d’activités grâce à leur groupement “Concrétisons nos utopies” dont : <ul><li>Le Pôle coworking, un espace collaboratif accueillant des acteurs du changement spécialisés dans la transition écologique et l’innovation sociale.</li><li>Le Pôle Lab qui a pour mission d’ouvrir nos écosystèmes à des publics différents (entreprises, jeunes, personnes éloignées de l’emploi).</li></ul>À l’image de Marseille, La Ruche® Marseille est chaleureuse, conviviale et colorée ! Un lieu de partage de compétences et de synergies entre les projets.",
        url        : "https://la-ruche.net/espaces/coworking-marseille/",
    },
    Ramina                                      : {
        name       : 'Ramina',
        description: "<b>RAMINA</b> (Réseau d'Accueil des MInots Non Accompagnés) est une association marseillaise composée uniquement de bénévoles qui croient en l'hospitalité. 🧡",
        url        : 'https://linktr.ee/ramina_marseille'
    },
    'Solidaires MNA'                            : {
        name       : 'Solidaires MNA',
        description: "Propositions/Demandes, de sorties, de coup de main, d'activités et de bons plans pour les jeunes en recours de minorité vivant dans les squat de boisson et cadolive 11 bvd boisson 13004, 5 rue cadolive 13004",
        url        : 'https://mensuel.framapad.org/p/nouveauxelles-benevoles-a4cl?lang=fr'
    },
    'QX1 - WelcomeMap'                          : {
        name       : 'QX1 - WelcomeMap',
        description: 'Carte Interactive avec INFOS et CONSEILS pour les MIGRANTSThe name “QX1” comes from the International Maritime Signals Code. It corresponds to the positive response sent by a port when a ship wishes to moor there: “You are authorized to moor in this port”.',
        url        : 'https://qx1.org/en/'
    },
    "AUP - Demandeurs d'asile"                  : {
        name       : "AUP - Demandeurs d'asile",
        description: "Association des Usagers de la PADA - Marseille <br><br>📍25 rue Saint-Basile 13001 <br><br> <ul> <li>Défendre et représenter les intérêts des usagers de la plateforme d'accueil des demandeurs d'asile (PADA) de Marseille </li><li>Ester en justice pour toute atteinte aux intérêts collectifs de ses membres</li><li>Faire respecter les droits des demandeurs d'asile à la PADA (récits d'asile, courriers, aide sociale, traduction...) et plus généralement dans leurs démarches de demande d'asile </li><li>Permettre l'accès des demandeurs d'asile à la compréhension des différents organismes intervenants dans la demande d'asile (Préfecture, OFII, OFPRA, CNDA) </li><li>Maitrise suffisante des réglementations du droit d'asile et de ses évolutions et à la connaissance de leurs droits</li><li>Permettre d'agir en sujets autonomes dans le plein respect de leur dignité humaine</li><li>Prendre part au débat public sur la politique d'asile.</li></ul><br><br> ",
        url        : 'https://aup-marseille-99.webselfsite.net/accueil'
    },
    Administratif                               : {
        name       : 'Administratif',
        description: 'Papiers, Informatique, Impression',
        url        : undefined
    },
    Langues                                     : {
        name       : 'Langues',
        description: '',
        url        : undefined
    },
    'Mot de Passe (Français)'                   : {
        name       : 'Mot de Passe (Français)',
        description: '',
        url        : undefined
    },
    'H.A.S'                                     : {
        name       : 'H.A.S',
        description: "<b>Habitat Alternatif Social</b><br><br>Habitat Alternatif Social (HAS) est une association active et innovante dans le secteur de la lutte contre la grande pauvreté.<br>L'objet social de l'association (article 2 des statuts) est ainsi formulé : « HAS défend, par le droit au logement et à la santé, la dignité des personnes fragiles ».<br>L'association accompagne les plus fragilisés mais aussi ceux qui se sentent démunis, impuissants, en danger, seuls, dépourvus. <br>L'association porte des valeurs très fortes : la dignité, le respect, la tolérance, la bienveillance, l'humanisme, l'ouverture.<br><br>HAS défend la dignité des personnes fragilisées par le logement et la santé par : <ul><li>Une intervention centrée sur la personne</li><li>Une offre d'insertion globale</li><li>Un accompagnement personnalisé</li><li>Une approche du vivre ensemble</li></ul>",
        url        : 'https://www.has.asso.fr/'
    },
    'Chomage | Precarité'                       : {
        name       : 'Chomage | Precarité',
        description: '',
        url        : undefined
    },
    Chomheureuses                               : {
        name       : 'Chomheureuses',
        description: "Les Chômheureuses c'est un collectif qui est ouvert à tout le monde! On se réunit tous les lundis à partir de 9h30 à la Dar au 127 rue d'Aubagne 13006 Marseille. On a aussi un mail chomheureuses@protonmail.com ",
        url        : undefined
    },
    'Chomeurs Précaires 13'                     : {
        name       : 'Chomeurs Précaires 13',
        description: 'Permanences tous les mercredis de 18h à 19h.<br> Réunions les derniers mercredis de chaque mois à 19h.<br> 📍Solidaires | 29 boulevard Longchamp | 13001<br> <br> ',
        url        : 'https://www.instagram.com/collectif.chomeurs.precaires13/'
    },
    'Luttes Locales'                            : {
        name       : 'Luttes Locales',
        description: '',
        url        : undefined
    },
    'Collectif Binkadi'                            : {
        name       : 'Collectif Binkadi',
        description: "jeunes exilés en lutte",
        url        : "https://www.instagram.com/collectif_binkadi_marseille/"
    },
    CHO3                                        : {
        name       : 'CHO3',
        description: 'Collectif des Habitants Organisés du 3e',
        url        : 'https://www.instagram.com/cho3marseille'
    },
    'Approches Cultures & Territoires'          : {
        name       : 'Approches Cultures & Territoires',
        description: "ACT est un centre de ressources, de recherche et de formation sur les migrations. Nous accompagnons les citoyens ainsi que les acteurs publics et privés qui souhaitent comprendre le phénomène migratoire et agir en faveur de la justice sociale. ACT est un lieu où il est possible de réaliser, d'écrire, de créer des constellations théoriques, artistiques et militantes pouvant rassembler des sociologues, des historiens, des philosophes, des psychologues, des politologues, des enseignants, des travailleurs sociaux, des artistes, des militants sans hiérarchisation de savoirs. <br><br> Nous laissons une grande place aux dispositifs et savoirs alternatifs pour repenser la manière d'habiter un « Tout-monde » vivable à partir d'une revitalisation d'une pensée critique et incarnée. Notre activité s'intéresse principalement aux questions suivantes : <ul> <li>Histoire des migrations</li> <li>Sociologie du racisme et des discriminations</li> <li>Transculturalité</li><li>Philosophie critique</li> <li>Pédagogie politique</li><li>Art-thérapie </li></ul><br> Venez nous rencontrer et explorons ensemble un horizon de transformation et d'émancipation collective.<br><br>📍39, rue Paradis - 13002 Marseille<br>✉️ direction@approches.fr<br>📞+33 6 26 06 83 76<br>",
        url        : 'https://www.approches.fr/agendact/'
    },
    'Autodéfense'                               : {
        name       : 'Autodéfense',
        description: '',
        url        : undefined
    },
    "Mars'Soins"                                : {
        name       : "Mars'Soins",
        description: "Collectif street medic autonome basé sur Marseille « Des militants qui se préfèrent debout plutôt qu'à genoux » <br><br> ✉️ mars-soins@protonmail.com",
        url        : "https://www.instagram.com/mars_soins/"
    },
    'Legal Team 13'                             : {
        name       : 'Legal Team 13',
        description: 'Autodéfénse Juridique',
        url        : 'https://www.instagram.com/legalteam.mars/'
    },
    'Arts Martiaux/Box'                         : {
        name       : 'Arts Martiaux/Box',
        description: 'SPAAM propose des temps de rencontres et amène des pratiques de soutien et de soin (écoute active, temps de formation à des outils d’organisation collective, d’hypnose et d’auto-hypnose, massage et auto-massage, herboristerie...) afin de permettre aux personnes ayant subi ou susceptibles de subir ces violences étatiques de ne pas se retrouver isolées et démunies face à ses conséquences.<br><br>Nous avons commencé à nous organiser autour de nos propres expériences de la répression, du soin et de nos besoins anti-répressifs, à nous auto-former à des pratiques de soin qui nous paraissent pertinentes dans ce contexte. Nous avons un info-kiosque sur ces thématiques. La formation, l’auto-formation et l’information sur tout ce qui peut accompagner des processus de soin liés à la répression nous semblent essentiels pour renforcer nos autonomies et nos cultures du soin en milieu militant.<br><br> Pour tout retour, toute question, contactez nous par mail (SPAAM13@RISEUP.NET) et par Signal (+33 7 45 97 56 99)',
        url        : ''
    },
    'Maladroite BoxPop'                         : {
        name       : 'Maladroite BoxPop',
        description: 'Au Centre Social Autogéré DAR',
        url        : ''
    },
    'Box Morozoff'                              : {
        name       : 'Box Morozoff',
        description: 'De lundi à vendredi, 11h au Morozoff',
        url        : 'http://www.lemorozoff.org/'
    },
    Mutualisation                               : {
        name       : 'Mutualisation',
        description: '',
        url        : undefined
    },
    'Matériel/Outils'                           : {
        name       : 'Matériel/Outils',
        description: 'Équipement, Matériel, outils',
        url        : ''
    },
    Marsmut                                     : {
        name       : 'Marsmut',
        description: "Le réseau MarsMut' est né de la volonté de mutualiser du matériel, de s'outiller collectivement et d'accéder à une autonomie matérielle dans nos luttes. La participation financière demandée est différente selon le matériel (prix fixe, prix libre, gratuit). Dans tous les cas nous ne voulons pas que le prix soit un obstacle à l'utilisation du matériel – c'est possible d'en discuter. Tout ceci reste à but non lucratif et ne vise pas à faire du bénéfice sur les prêts. Un coup de main pour des réparations est aussi bienvenu.",
        url        : 'https://marsmut.org/'
    },
    Logement                                    : {
        name       : 'Logement',
        description: '',
        url        : undefined
    },
    Revenus                                     : {
        name       : 'Revenus',
        description: '',
        url        : undefined
    },
    'Mutuelle MTPGB'                            : {
        name       : 'Mutuelle MTPGB',
        description: 'Meufs Trans Pédé Gouines Bi·es MAIL: mut-marseille@lists.riseup.net',
        url        : 'https://mars-infos.org/presentation-de-la-mutuelle-mtpgb-4401'
    },
    Ateliers                                    : {
        name       : 'Ateliers',
        description: '',
        url        : undefined
    },
    'Numérique'                                 : {
        name       : 'Numérique',
        description: '',
        url        : undefined
    },
    Marsnet                                     : {
        name       : 'Marsnet',
        description: '',
        url        : 'http://www.marsnet.org/'
    },
    Bouffe                                      : {
        name       : 'Bouffe',
        description: '',
        url        : undefined
    },
    'Production en ville'                       : {
        name       : 'Production en ville',
        description: '',
        url        : undefined
    },
    'Les Champi. de Marseille'                  : {
        name       : 'Les Champi. de Marseille',
        description: 'Nous sommes des producteurs de champignons à Marseille. Nous cultivons des pleurotes et des shiitakés bio distribués en circuit court #agricultureurbaine #gastronomie #local #circuitcourt',
        url        : 'https://www.facebook.com/champignons.marseille/?locale=fr_FR'
    },
    'Cantines Solidaires'                       : {
        name       : 'Cantines Solidaires',
        description: '',
        url        : undefined
    },
    TheNobelKitchen                             : {
        name       : 'TheNobelKitchen',
        description: 'Au Centre Social DAR',
        url        : ''
    },
    'La Marmite Joyeuse'                        : {
        name       : 'La Marmite Joyeuse',
        description: "Bien plus qu'une cantine, la Marmite Joyeuse est avant tout un lieu dont la vocation est de favoriser le lien social autour des activités culinaires. Convivialité, participation, initiative sont nos maîtres mots.",
        url        : 'https://lamarmitejoyeuse.com/'
    },
    'Casa Consolat'                             : {
        name       : 'Casa Consolat',
        description: 'Cantine solidaire du lundi au vendredi 12h-14h',
        url        : 'https://www.facebook.com/CasaConsolat/?locale=fr_FR'
    },
    'Réseaux PDC'                               : {
        name       : 'Réseaux PDC',
        description: 'Réseaux de production-distribution-consomation',
        url        : undefined
    },
    'Les Paniers Marseillais'                   : {
        name       : 'Les Paniers Marseillais',
        description: "Réseau des Paniers Marseillais. Associations pour le Maintien d'une Agriculture Paysanne",
        url        : 'https://lespaniersmarseillais.org/?CartoGraphie'
    },
    'Le Marché Rouge'                           : {
        name       : 'Le Marché Rouge',
        description: "Le Marché rouge fête ses 4 ans d'existence ! Pourvu que ça dure ! <br>Mars 2020, le confinement est décrété dans l'hexagone. La crise alimentaire frappe d'emblée les plus précaires, travailleur.es sans droits ni titres, TDS, retraité.es sans revenus ou sans papiers, et leurs enfants, beaucoup d'enfants. Les associations classiques de l'aide alimentaire tirent le rideau, leurs salarié.es sommé.es de rester à la maison. Et les quelques distributions de colis ne sont accessibles qu'à la condition de présenter les «bons papiers».<br>Spontanément, la solidarité directe et horizontale s'organise, à l'initiative des activistes du Collectif Soutien Migrants 13 / El Manba et de Association des Usagers de la PADA Marseille (AUP). Puis en lien avec La Caillasse, Parastoo, du Mc Do occupé (qui deviendra L'Après M), des dizaines de personnes rejoignent les distributions alimentaires autonomes et inconditionnelles, à la Dar, au local du Manba et à la Casa consolat.<br>Un camion collectif sera acheté pour récupérer plusieurs fois par semaine les masses d'invendus du Marché d'intérêt national (fruits et légumes), et des fonds sont récoltés pour fournir les conserves et denrées sèches indispensables.<br>Le Marché rouge naissait et n'allait jamais connaître de pause en 4 ans puisqu'il sera relativement vite autogéré par l'AUP qui en assurera la continuité essentielle. Il se poursuit alors tous les samedis matin au local d'Al Manba et concerne plusieurs centaines de paniers partagés par plus de 300 exilé.es organisé.es dans l'AUP et leurs foyers.",
        url        : 'https://mars-infos.org/boum-de-soutien-au-marche-rouge-7482'
    },
    'Gestion Déchets'                           : {
        name       : 'Gestion Déchets',
        description: '',
        url        : undefined
    },
    'Entreprises-Autog.'                        : {
        name       : 'Entreprises-Autog.',
        description: '',
        url        : undefined
    },
    'Les Champignons de Marseille'              : {
        name       : 'Les Champignons de Marseille',
        description: 'à vérifier',
        url        : 'https://www.facebook.com/champignons.marseille/?locale=fr_FR'
    },
    'Circuit Court'                             : {
        name       : 'Circuit Court',
        description: '',
        url        : undefined
    },
    'Épiceries'                                 : {
        name       : 'Épiceries',
        description: '',
        url        : undefined
    },
    'Bar à Vrac'                                : {
        name       : 'Bar à Vrac',
        description: "65 allée Léon Gambetta, 13001 Marseille - Un magasin d'alimentation biologique en vrac ou consigné et un café-resto végétarien.",
        url        : 'https://presdecheznous.fr/annuaire#/fiche/Le-bar-a-vrac/C6i/@43.299,5.385,14z?cat=all'
    },
    'La plaine fraicheur'                       : {
        name       : 'La plaine fraicheur',
        description: '2 Place Jean Jaurès, 13001 Marseille - Ouvert du lundi au samedi de 7h à 20h et le dimanche de 8h à 15h',
        url        : 'https://transiscope.gogocarto.fr/annuaire#/fiche/La-plaine-fraicheur/4EDX/'
    },
    'Adèle'                                     : {
        name       : 'Adèle',
        description: 'Une Association de Distribution, Équitable, Locale et Ecoresponsable. - Épicerie membre du réseau Filière Paysanne',
        url        : 'http://adelemarseille.blogspot.fr/'
    },
    'Restos | Cantines'                         : {
        name       : 'Restos | Cantines',
        description: '',
        url        : undefined
    },
    'Les Ondines'                               : {
        name       : 'Les Ondines',
        description: '19 Rue Saint-Bazile, 13001 Marseille - L-»V 11:45-15:00',
        url        : 'http://www.lesondines.bio/'
    },
    "Café l'Ecomotive"                          : {
        name       : "Café l'Ecomotive",
        description: '2 Place des Marseillaises, 13001 Marseille - Une cantine végétale et bio, du tout fait maison.',
        url        : 'https://www.lecomotive.org/'
    },
    'Le Grain de Sable'                         : {
        name       : 'Le Grain de Sable',
        description: '34 rue du Baignoir, 13001 Marseille - Un salon de thé-restaurant végétarien, qui vend aussi des thés et cafés. Du mardi au samedi de 11h à 20h.',
        url        : 'http://www.graindesable.fr'
    },
    Boulangeries                                : {
        name       : 'Boulangeries',
        description: '',
        url        : undefined
    },
    'Le Bar à Pain'                             : {
        name       : 'Le Bar à Pain',
        description: "18 Cours Joseph Thierry, 13001 Marseille - Produit issus de culture biologique, accueil de stagiaires, exposition d'artistes locaux...",
        url        : 'https://www.facebook.com/PageLeBaraPain'
    },
    "Les Mains Libres"             : {
        name       : "Les Mains Libres",
        description: "📍 117 Boulevard Chave | 13005 <br><br> L'esprit Les Mains Libres, c'est quelques pelletées de farine, un soupçon de patience, une grosse dose de chaleur, une portion de créativité et de la bonne humeur. C'est un fournil, plusieurs mains, de la synchronisation, des accords harmonieux et parfois de fausses notes heureuses. Les Mains Libres ce sont des choix: le choix de travailler avec des farines issues de l'agriculture biologique de la région (Farine du Moulin Saint Joseph à Grans) et d'autres matières premières biologiques, d'utiliser farines de blé, de seigle, de khorasan, de petit épeautre de Haute-Provence, de sarrasin, de châtaignes, de les pétrir seules, de les mélanger, de travailler avec plusieurs levains naturels , d'inventer.C'est une recherche autour du pain, autour du goût et l'envie de mettre en valeur la production locale.",
        url        : "https://www.les-mains-libres.fr/"
    }, 
    'Boulangerie-Café Pain Salvator'            : {
        name       : 'Boulangerie-Café Pain Salvator',
        description: '32 Boulevard Louis Salvator, 13006 Marseille',
        url        : 'https://www.facebook.com/painsalvator/'
    },
    'House of Pain'                             : {
        name       : 'House of Pain',
        description: '14 Rue Fontange, 13006 Marseille - Nouvelle boulangerie bio au cœur de Marseille à Notre Dame du Mont. Ici la panification est 100% artisanale. Une fermentation lente au levain naturel avec des farines bio qui favorisent le bon goût du pain et le développement de ses arômes.',
        url        : 'https://www.facebook.com/pg/Boulangerie.House.of.Pain'
    },
    'Marchés'                                   : {
        name       : 'Marchés',
        description: '',
        url        : undefined
    },
    'Marché des artisans et des producteurs bio': {
        name       : 'Marché des artisans et des producteurs bio',
        description: 'Place Léon Blum, 13001 Marseille - Mardi et Samedi 08h00-13h00',
        url        : ''
    },
    'Supermarchés'                              : {
        name       : 'Supermarchés',
        description: 'Alternatives à la grande distribution, cooperatives, circuit-court, local, qualité.',
        url        : undefined
    },
    'Super Cafoutch'                            : {
        name       : 'Super Cafoutch',
        description: "Un supermarché qui nous ressemble. Coopératif, participatif, convivial, avec des bons produits à prix abordables <br><br>Un supermarché coopératif et participatif est un commerce dont les usagers sont à la fois les patrons, les clients et… les employés bénévoles. Ses membres choisissent les produits et fixent les prix eux-mêmes. Parce que devenir acteurs de notre consommation, c'est accéder à des produits de qualité sans se ruiner.<br><br>📍16 rue du Chevalier Roze 13002 <br> ⏰ Ouvert mardi > vendredi : 9h – 20h30 | samedi : 9h - 19h30",
        url        : 'https://supercafoutch.fr/'
    },
    Friperies                                   : {
        name       : 'Friperies',
        description: '',
        url        : undefined
    },
    "Frip'Insertion - Libération"               : {
        name       : "Frip'Insertion - Libération",
        description: "Frip'Insertion est un chantier d'insertion, lié au mouvement Emmaüs, créé dans le but de donner un avenir professionnel à des personnes exclues du marché du travail, à travers la récupération, le tri, le recyclage et la vente de textiles. Notre objectif est de développer des actions de solidarité afin de lutter contre les diverses formes d'exclusions sociales. Nous avons pu créer 8 postes de travail à durée indéterminée et 25 postes en contrats aidés (Contrat à Durée Déterminée d'Insertion) pour lesquels nous assurons un accompagnement socio-professionnel et une formation professionnelle de base.<br><br> 📍 78 boulevard de la Libération 13004 Marseille<br> 📞 04.91.53.70.93<br> ⏰ Du lundi au samedi: 10h00 à 12h30 - 14h00 à 18h00",
        url        : 'https://fripinsertion.wordpress.com/lassociation/'
    },
    "Frip'Insertion - Capelette "               : {
        name       : "Frip'Insertion - Capelette ",
        description: "Frip'Insertion est un chantier d'insertion, lié au mouvement Emmaüs, créé dans le but de donner un avenir professionnel à des personnes exclues du marché du travail, à travers la récupération, le tri, le recyclage et la vente de textiles. Notre objectif est de développer des actions de solidarité afin de lutter contre les diverses formes d'exclusions sociales. Nous avons pu créer 8 postes de travail à durée indéterminée et 25 postes en contrats aidés (Contrat à Durée Déterminée d'Insertion) pour lesquels nous assurons un accompagnement socio-professionnel et une formation professionnelle de base. <br><br>📍 175 avenue de la Capelette 13010 Marseille <br>📞 04.91.49.88.32 <br>⏰ Du lundi au samedi: 10h00 à 12h30 - 14h00 à 18h00",
        url        : 'https://fripinsertion.wordpress.com/lassociation/'
    },
    Regain                                      : {
        name       : 'Regain',
        description: 'Regain est une société coopérative d’intérêt collectif (SCIC) qui propose de l’accompagnement et des formations pour favoriser l’émergence et la réalisation de projets d’habitats participatifs en PACA et en France.',
        url        : 'https://www.regain-hg.org/'
    },
    'Infra-personnel'                           : {
        name       : 'Infra-personnel',
        description: "Infra-personnel, singulier, intime : quel est mon rapport <b>*singulier*</b> au travail, à l'amour, à l'altérité, à la finitude, à la famille, à l'amitié, à la justice, aux normes, au genre, à l'État, aux institutions, à la folie, au patriarcat, au capitalisme, au colonialisme, etc... Ce niveau nanopolitique aborde le travail des thèmes/problèmes sociaux à partir de la singularité de chaque un·e de nous. Le terrain d'action n'est pas le même que, par exemple, la bataille médiatique face aux lois et politiques gouvernementales (macropolitique), ou les actions de solidarité/entraide/lutte micropolitique (exemple : la mise un place d'un dispositif de soin militant ou d'une cantine solidaire). Une phrase simple pour décrire ce niveau serait « faire travailler (déconstruire-soigner) la société à l'« intérieur » de nous mêmes. À ne pas confondre avec l'esprit du développement personnel car l'approche ici est transversale : il n'est pas question de parler des personnes comme des entités séparées de la société et ses institutions profondément pathogènes, pas question de concevoir le soin et la thérapie/développement débranché·es de la politique, de la déconstruction. Pas question non plus de penser ce niveau comme séparé et indépendant des niveaux macro et micropolitique, car ces trois niveaux font partie d'une même soupe hétérogène. <br><br> Comme le terrain d'action nanopolitique est différent de celui de la macro et micropolitique, il appelle des modes d'action différents. Il n'y a pas dix mille manières de se faire écouter par le gouvernement : bataille médiatique, manifestations, grèves, blocages… ces modes d'action pour équilibrer les rapports de force peuvent s'appliquer un peu partout dans les sociétés à État et branchées au capitalisme mondial. Il n'y a pas non plus dix mille formes pour monter une cantine solidaire, pour constituer un groupe militant, etc. Or, au niveau nanopolitique, chaque personne est un peuple singulier. Si l'on partage tous·tes, disons en occident, un ensemble de constructions de base concernant l'amour, le travail, l'altérité, la folie, la justice, etc, le travail pour s'en défaire ou les transformer se révèle en revanche complètement singulier ; car travailler à déconstruire l'autoritarisme, le sexisme, le racisme, le validisme, le rapport punitif à la justice etc, implique de travailler son rapport singulier à sa famille, à ses professeurs, à ses ami·es, …, à tout ce qui nous a façonné·es et continue de le faire. Ce travail se fait dans nos relations visibles, directes, avec toutes les personnes avec qui l'on interagit à l'extérieur, mais il se fait également de soi à soi-même, dans un rapport intérieur ; depuis le moment où je suis né·e jusqu'à aujourd'hui, toutes mes relations ont été enregistrées dans mon inconscient et continuent d'opérer, de m'influencer à un niveau plus profond, disons en arrière-plan. Je peux donc travailler ces rapports-là de l'« intérieur ». D'une certaine façon, et c'est bien cela la grande découverte de l'inconscient, l'on continue à relationner inconsciemment avec nos parents, nos professeurs, nos collègues de classe, nos ancien·es camarades, avec l'école, la famille, nos « ex », etc. Chaque personne serait alors une intersection de multiples fils, chaque fil ayant son propre parcours, sa propre histoire. Chaque personne aurait donc sa propre géologie, sa propre cartographie. Il n'y a donc pas ni protocole ni recette révolutionnaire qui s'appliquerait à tout le monde pour dépasser ou déconstruire ce que la société nous a imposé et nous impose. Les lacunes, montagnes, barrières, vallées, sentiers, entrées, sorties…  ne sont jamais disposées, agencées de la même façon dans <i>ton</i> territoire que dans le <i>mien</i>. Chacun·e sa géopolitique ! Ç'est-à-dire que chaque processus personnel de déconstruction-soin-thérapie est une œuvre singulière. Mais singulière ne veut pas dire forcement individuelle, solitaire. Au contraire, être seul·es dans nos parcours singuliers n'est pas facile. On peut se sentir justement trop seul·es, perdu·es, démuni·es, ce qui fait qu'on n'arrive pas facilement à travailler cette œuvre et qu'on reste accroché·es aux choses qui nous font sentir en sécurité/confortables malgré la toxicité : ma famille, mon amoureux·se, mon travail, ma façon de vivre l'amitié, ma façon de me confronter à l'altérité, de faire un enfant, etc.  Et  cela se comprend tout à fait, car déconstruire les productions pathogènes de notre société en nous-mêmes nous amène forcément dans des terrains inconnus et effrayants tout en nous obligeant à affronter la violence de cette même société en tant que force normalisatrice. À ce propos, il est peut-être temps d'intégrer sérieusement dans nos milieux militants qu'on n'est pas égaux·ales, ni dans la façon dont on subit ces violences ni dans nos possibilités de déconstruction. Cette différence va dépendre de l'interrelation d'une multiplicité de facteurs : le genre, l'origine/milieu social, l'environnement dans lequel on a grandi ou dans lequel on vit (ville ou campagne), le type d'école qu'on a fréquentée, l'accès aux outils, le bagage politique dans la famille, les traumatismes, l'accès au soutien, l'accès au soin… etc. <br>Et puisqu'on évoque le soin, voici ce que la psychanalyse dominante n'a pas su résoudre ou plutôt ce sur quoi elle n'a pas voulu travailler : comment concevoir et pratiquer la thérapie sans dissocier nos traumatismes et nos névroses de l'ensemble des institutions et constructions pathogènes de nos sociétés ? Prenant en compte que c'est la société tout entière qui a besoin d'une thérapie ! Alors, la psychanalyse a globalement préféré de faire une fixation sur le passé des individu·es et sur les rapports dits œdipiens du triangle enfant-père-mère (Freud) ou sur les « mathèmes » de l'inconscient « structuré comme un langage » (Lacan) plutôt que de travailler davantage l'aspect politique qui est central, non seulement pour l'analyse des névroses ou des psychoses mais aussi pour le bricolage d'une thérapie ayant la politique au cœur de sa conception. Pour cette dernière approche, on trouve heureusement quelques ressources, notamment chez Félix Guattari, au sein du mouvement de la « Psychothérapie Institutionnelle ». <br><br> En définitive, le niveau nanopolitique/infra-personnel est au moins tout aussi important que le niveau macro ou micropolitique dans nos luttes pour la transformation sociale et l'émancipation, car la libération potentielle qui découlerait de ce travail ouvrirait de nouveaux possibles dans ces-derniers niveaux. Ce qui nous bloque souvent dans nos actions à échelle macro ou micro, ce sont justement nos rigidités et blocages au niveau nano, de l'« inconscient » donc. Nous avons trop tendance à négliger le niveau nanopolitique. Pourquoi ? Parce que c'est le plus dur à affronter, le plus intime, et que c'est là qu'on est les plus démuni·es ! La preuve, le seul fait de parler d'inconscient et de politique dans la même phrase nous fait passer pour des fous et déclenche toutes sortes de méfiances. Quel dommage, car c'est le niveau qui abrite en lui un des potentiels les plus transformateurs, surtout lorsqu'on l'articule avec les niveaux micro et macropolotique.",
        url        : undefined
    },
    'Dispositifs Travail Singularité'           : {
        name       : 'Dispositifs Travail Singularité',
        description: "Prise en charge collective du besoin d'acompagnement dans nos processus (parcours) de singularisation. Travailler (déconstruire) la société *aussi* à l'«intérieur» de nous mêmes. Idéntifier et traiter collectivement ce qui bloque les processus de singularisation chez chacun·e d'entre nous ",
        url        : undefined
    },
    'Thérapie Transversale'                     : {
        name       : 'Thérapie Transversale',
        description: '',
        url        : undefined
    },
    Ressources                                  : {
        name       : 'Ressources',
        description: 'Toute sorte de ressources',
        url        : undefined
    },
    Agendas                                     : {
        name       : 'Agendas',
        description: '',
        url        : undefined
    },
    'Le Vortex'                                 : {
        name       : 'Le Vortex',
        description: "Le Vortex est l'agenda des concerts et événements cools sur Marseille! Vous y trouverez un Agenda plutôt complet, ainsi qu'une liste des Orgas de concerts, des labels , des lieux de concerts, des radios, disquaires, libraires et restos marseillais, ainsi qu'une liste des groupes du coin.",
        url        : 'https://www.facebook.com/vortexfrommars/events?ref=page_internal'
    },
    'Journal Ventilo'                           : {
        name       : 'Journal Ventilo',
        description: "Ventilo est un bimensuel culturel gratuit édité par l'association Aspiro. Il est diffusé à Marseille, Aix-en-Provence et dans les Bouches-du-Rhône.",
        url        : 'https://www.journalventilo.fr/'
    },
    'Mars Infos Autonomes'                      : {
        name       : 'Mars Infos Autonomes',
        description: 'Site Collaboratif des Luttes',
        url        : 'https://mars-infos.org/'
    },
    'Radar Squat'                               : {
        name       : 'Radar Squat',
        description: "<b>Radar: An agenda free of commercial interest.</b><br><br> Created by a community of groups announcing their own events, where the dominant power doesn't decide if it's worth a mention.<ul><li><i>Radar is for groups that are challenging the status-quo: DIY, anti-capitalist, intersectional, autonomous, non-hierarchical, anti-fasist, anti-racist, queer-positive, squatting. Organising direct action, challenging the state, patriarchy, ableism, sanism, authority. Protecting the environment, housing, workers, ex-workers, communities. Working to make the structures of mutual aid and communal social relations we need for the future. If your group is all of these things, maybe you fit. If it's just some of them, maybe you fit too. If you're opposed to some of these, or don't challenge the status-quo, your group probably wants to languish on some other platform.</i></li></ul> A tool we build together, to organise, socialise, educate. <br>A tool designed not to track its users, but break from the 'facebook-like' gated communities and to promote open sharing.",
        url        : 'https://radar.squat.net/fr/events/city/Marseille'
    },
    'Démosphère'                                : {
        name       : 'Démosphère',
        description: 'Agendas Alternatives',
        url        : 'https://demosphere.net/fr'
    },
    Mobilizon                                   : {
        name       : 'Mobilizon',
        description: 'Gather ⋅ Organize ⋅ Mobilize',
        url        : 'https://mobilizon.fr/search?locationName=Marseille&lat=43.295314&lon=5.401581&search='
    },
    Cartes                                      : {
        name       : 'Cartes',
        description: '',
        url        : undefined
    },
    'La Cloche Sud'                                      : {
        name       : 'La Cloche Sud',
        description: "<h3>POUR UNE SOCIÉTÉ PLUS INCLUSIVE !</h3>La Cloche a été fondée sur un constat : l’accès aux services de base est aussi important que le respect de la dignité pour les personnes en situation de grande précarité. Créée en 2015 à Paris, elle est reconnue comme association d’intérêt général à but non lucratif.<br><br>En fédérant des commerces et lieux inclusifs via le Carillon, en co-organisant des activités et des évènements de lien social (atelier de cuisine, jardinage, théâtre, chorale...) entre voisins avec et sans domicile et en sensibilisant aux problématique de la rue, La Cloche vise à renforcer la vie de quartier pour que chacun s'y sente bien. et puisse s'épanouir.<br><br>En réponse à l'augmentation constante des besoins, La Cloche s'est progressivement développée sur tout le territoire et est maintenant présente à travers des antennes salariées dans 11 villes de France. Son programme phare du Carillon est quant à lui disponible dans 35 villes.",
        url        : "https://www.facebook.com/LaClocheSUD/"
    },
    'Le Carillon'                                      : {
        name       : 'Le Carillon',
        description: "Depuis sa création, Le Carillon permet aux commerces et autres lieux de quartier de s’engager contre la grande exclusion en proposant gratuitement des produits et/ou services aux personnes en situation précaire et aux habitant·es qui ne souffrent pas de la précarité de les soutenir en faisant le choix de la consommation solidaire et/ou en s'engageant à accueillir toute personne de la même manière qu'elle soit ou non en situation de précarité",
        url        : "https://www.lacloche.org/le-carillon"
    },
    'Réseau des Paniers Marseillais - AMAP'     : {
        name       : 'Réseau des Paniers Marseillais - AMAP',
        description: "Réseau des Paniers Marseillais. Associations pour le Maintien d'une Agriculture Paysanne",
        url        : 'https://lespaniersmarseillais.org/?CartoGraphie'
    },
    'Carte Autogéré Rhizome'                    : {
        name       : 'Carte Autogéré Rhizome',
        description: "Elle sert à cartographier de façon décentralisé tous les projets, initiatives, personnes concernant les communs, les processus de singularisation-autonomisation, l'accès à la multi-thérapie, les lieux de répit, les luttes sociales.",
        url        : 'https://framacarte.org/m/154090/'
    },
    'Carte des Luttes - Reporterre'             : {
        name       : 'Carte des Luttes - Reporterre',
        description: "Aéroports, fermes-usines, barrages, entrepôts, centres commerciaux… Les grands projets inutiles et dévastateurs prolifèrent en France. Face à eux, des collectifs citoyens se mobilisent pour défendre leur environnement. Reporterre publie une carte de toutes ces luttes locales. Elle servira d'outil pour celles et ceux qui veulent empêcher la destruction du monde.",
        url        : 'https://lutteslocales.gogocarto.fr/annuaire#/carte/@43.332,5.456,11z?cat=all'
    },
    'Retab.fr'                                  : {
        name       : 'Retab.fr',
        description: "Une façon simplifiée de trouver une structure de soins ou un dispositif d'accompagnement en santé mentale",
        url        : 'https://www.retab.fr/accueil/index.php'
    },
    'Carto-Marseille'                           : {
        name       : 'Carto-Marseille',
        description: "Cartographie de la Ville de Marseille",
        url        : 'https://carto.marseille.fr/'
    },
    DICADD                                      : {
        name       : 'DICADD',
        description: "Association Régionale de Coordination en Addictologie territoire des Bouches-du-Rhône, est un dispositif de coordination des parcours de santé et d'appui à la pratique professionnelle dans le champ des conduites addictives, dans les Bouches-du-Rhône.",
        url        : 'https://www.dicadd13.fr/annuaire'
    },
    FransGenre                                  : {
        name       : 'FransGenre',
        description: "ASSOCIATION D'ENTRAIDE TRANSGENRE. Fransgenre est une association féministe intersectionnelle.Nous dénonçons les oppressions systémiques telles que la transphobie, la misogynie, l'homophobie, le racisme, le validisme, la xénophobie, l'enbyphobie et l'intersexophobie. Nous nous plaçons contre le système carcéral, la pénalisation du travail du sexe et des usager‧es de drogues.",
        url        : 'https://fransgenre.fr/#ressources'
    },
    Transiscope                                 : {
        name       : 'Transiscope',
        description: 'Le Portail Web des Alternatives',
        url        : 'https://transiscope.org/carte-des-alternatives/#/carte/Marseille'
    },
    'Près de Chez Nous'                         : {
        name       : 'Près de Chez Nous',
        description: "En 2007, Colibris et Le Marché Citoyen décident d'unir leurs forces et leurs réseaux pour référencer sur leurs cartes web les acteurs qui nous permettent, partout en France, de nous alimenter, apprendre, nous soigner, habiter, nous déplacer, voyager... autrement.",
        url        : 'https://presdecheznous.fr/annuaire#/carte/marseille'
    },
    'Terre de Liens'                            : {
        name       : 'Terre de Liens',
        description: "Assemblage inédit d'acteurs et actrices de la société civile, du monde agricole et de la finance solidaire, le mouvement Terre de Liens tire son originalité de l'articulation entre un réseau associatif actif dans toute la France, une entreprise d'investissement solidaire et une fondation reconnue d'utilité publique.",
        url        : 'https://fermes.terredeliens.org/national/hub-ferme/'
    },
    'LGBT+ PACA'                                : {
        name       : 'LGBT+ PACA',
        description: '<b>La vie associative LQBTQI+ en PACA</b><br><br>Vous avez une info ? une question ? une actualité à communiquer ? <br> Écrivez nous : <br>✉️ <b>contact.lgbt.paca@gmail.com</b><br><br>Inscrivez vous librement à la mailing list « membres » en envoyant un mail vide à : <br>✉️ <b>membres-lgbt-paca+subscribe@googlegroups.com</b><br>',
        url        : 'https://www.lgbt-paca.org/'
    },
    Habicoop                                    : {
        name       : 'Habicoop',
        description: "Féderation Française des Coopératives d'Habitants",
        url        : 'https://www.habicoop.fr/'
    },
    'Hameaux Légers'                            : {
        name       : 'Hameaux Légers',
        description: 'Accompagner la création d’écohameaux accessibles financièrement, pour permettre à toutes et tous d’habiter de manière durable et solidaire.',
        url        : 'https://hameaux-legers.gogocarto.fr/map#/carte/@43.35,5.60,10z?cat=all'
    },
    'Regain & Habitat Participatif Fr'          : {
        name       : 'Regain & Habitat Participatif Fr',
        description: 'Cartographie co créée par Habitat Participatif France et la Coopérative Oasis',
        url        : 'https://www.regain-hg.org/carte-des-projets/'
    },
    'Les Écotables'                             : {
        name       : 'Les Écotables',
        description: "Nous sommes des professionnel.le.s et nous voulons faire progresser l’alimentation durable en France, au sein d'une communauté. <br>Nous agissons pour une alimentation saine, nutritive et accessible, pour un modèle vertueux, qui respecte le vivant, l’humain et l’environnement, qui rémunère justement celles et ceux qui cuisinent, produisent et distribuent. <br>Nous croyons qu'une alimentation durable contribue à rendre notre société plus vivable, plus équitable, plus viable.<br><br>Nous nous engageons à : <ul><li> partager, collaborer, s’entraider entre professionnel.le s de la restauration et de l’alimentation</li><li>proposer une alimentation saine, nutritive et délicieuse</li><li>mettre l’humain au centre de notre action et à le choyer</li><li>respecter l’environnement, les saisons et la biodiversité </li><li> rendre accessible l’alimentation durable à tous et toutes </li><li>privilégier un approvisionnement de qualité et de proximité </li><li>veiller à la réduction et au traitement de nos déchets </li><li>être transparent.e.s sur nos actions</li><li>être à l’écoute des membres et des partenaires et leur apporter notre soutien</li><li>adopter un ton fédérateur plutôt que moralisateur</li><li>faciliter une meilleure représentation des petits acteurs indépendants </li><li>toujours guider nos actions par le respect des valeurs environnementales, sociales et économiques propres au développement durable</li><li>soutenir le développement de l’agriculture, de la viticulture, de la pêche durable et de l'élevage, respectueux du vivant et du bien-être animal</li><li>faire tous les jours un peu mieux, la perfection n’existe pas ! </li></ul> Nous luttons contre : <ul><li>les tomates en hiver </li><li>les violences en cuisine </li><li>les produits ultra transformés </li><li>la production industrielle et interventionniste du vin</li><li>la malbouffe </li><li>la perte de pouvoir d'achat de ceux et celles qui produisent </li><li>l’accaparement des biens communs par une poignée d’individus </li><li>l’eau en bouteille </li><li>les discriminations en cuisine, dans les champs et dans la vigne</li><li>la précarisation des métiers de bouche </li><li>le greenwashing </li><li>les pratiques alimentaires dégradant le vivant : déforestation, artificialisation des sols, atteinte à la biodiversité, pollution des sols et des eaux, ...</li><li>la précarité alimentaire </li></ul>",
        url        : 'https://ecotable.fr/ecotables'
    },
    'Médias'                                    : {
        name       : 'Médias',
        description: '',
        url        : undefined
    },
    Primitivi                                   : {
        name       : 'Primitivi',
        description: "Une « téloche de rue » qui, depuis 1998, accompagne, soutient, réalise des films, en projette, en distribue, dans une démarche rebelle, solidaire et non-commerciale. Primitivi est un média d'info local, qui raconte, relaie, rassemble, aux côtés de ceux qui se battent pour une Marseille bouillonnante, indomptable et mélangée. Primitivi est un média qui voyage, pour emmener notre expérience et en ramener d'autres.",
        url        : 'https://www.primitivi.org/'
    },
    'Ami·es'                                    : {
        name       : 'Ami·es',
        description: '',
        url        : undefined
    },
    "L'Empaille (Mouret (12))"                  : {
        name       : "L'Empaille (Mouret (12))",
        description: "C'est l'été ! Achetez l'empaillé n°6 ! Grâce à la soirée de soutien au Grand Mas de Mouret (12), nos liens chaleureux avec les camarades de l'empaillé se sont confirmés. Vive les médias libres !",
        url        : 'https://lempaille.fr/'
    },
    'Association Kokopelli'                     : {
        name       : 'Association Kokopelli',
        description: 'Semences biologiques, libres de droits et reproductibles',
        url        : 'https://kokopelli-semences.fr/fr/'
    },
    'CRIR-AVS PACA'                             : {
        name       : 'CRIR-AVS PACA',
        description: "Les Centres Ressources pour les Intervenants auprès des Auteurs de Violences Sexuelles (CRIAVS) sont des structures de service public1 placées sous la responsabilité du Ministère des Solidarités et de la Santé et sont rattachées à des établissements de santé. Leurs équipes sont pluridisciplinaires (plus de 15 corps de métier représentés), essentiellement composées de professionnels de la santé mentale.<br><br> Leurs actions s’adressent à tout professionnel ou toute institution concernés par la problématique des violences sexuelles, notamment dans les champs suivants : Santé, Justice, Socio-Educatif, Education Nationale, Formation, Enseignement supérieur, Sécurité publique…<br><br>Leurs missions sont les suivantes : <ul><li> FORMATION / Impulser la formation initiale et développer la formation continue auprès de tous les professionnels impliqués dans la prise en charge des auteurs de violences sexuelles. </li><li>RECHERCHE / Encourager, favoriser, initier le développement de nouvelles recherches et réflexions sur l’amélioration de l’évaluation, des traitements, et des suivis des auteurs de violences sexuelles.</li><li> PREVENTION / Participer au développement des principes et actions de prévention des violences sexuelles aux niveaux primaire, secondaire, et tertiaire.</li><li>ANIMATION DE RESEAU / Animer le réseau Santé-Justice, recenser les ressources et les compétences régionales, dans l’objectif d’une collaboration étroite entre acteurs des champs santé, justice, et social.</li><li>ESPACE EXPERT / Fournir un espace de réflexion et d’élaboration autour des difficultés inhérentes aux pratiques professionnelles des intervenants auprès des auteurs de violences sexuelles.</li><li>DOCUMENTATION / Mise à disposition des documents de références, des avancées de la recherche, et de l'évolution des connaissances par l'intermédiaire d'une base de données documentaires nationale accessible en ligne.</li></ul>Chacune de ces missions est mise en œuvre au niveau local en fonction des besoins et ressources spécifiques de chaque territoire. La mise en commun de ces actions au niveau national à travers la Fédération française des CRIAVS permet ainsi un enrichissement mutuel et le déploiement de dispositifs au niveau national.",
        url        : 'https://www.ffcriavs.org/nous_contacter/votre-criavs/crir-avs-paca/'
    },
    Radios                                      : {
        name       : 'Radios',
        description: '',
        url        : undefined
    },
    'Radio Galère (88.4)'                       : {
        name       : 'Radio Galère (88.4)',
        description: "A la fin des années 70 n’existait sur la bande FM que des radios publique d’État ou des radios privées financées par des entreprises privées émettant de l’étranger (RMC, RTL …). Les auditeur·ice·s n’avaient pas d’autre choix que d’écouter des radios aux ordres du pouvoir ou soumis au lobbying du monde marchand. À la fin des années 70, un peu partout en France, des militant·e·s progressistes de tout bord (syndicalistes, personnes issues de l’immigration, écologistes, féministes, défenseur·euse·s des droits, LGBTQI, personnes en situation de handicap) ont décidé qu’il était temps de « donner la parole à celles et ceux qui ne l’ont pas ». Et pas seulement dans l’hexagone mais que des voix résonnent d’un peu partout dans le monde, notamment des anciennes colonies.<br><br>C’est à ce moment-là que sont apparus ce que le gouvernement giscardien de l’époque appelait les fameuses « Radios Pirates ». À Marseille, la première radio qui a osé squatter les ondes FM pour proposer d’autres voix et d’autres points de vue fut « Radio Béton » avec comme slogan « le béton est armé, pourquoi pas nous ». Elle fit son apparition en 1979. Ses prises d’antennes étaient ponctuelles car c’était surtout le jeu du chat et de la souris avec la police. Les animateur·ice·s diffusaient de local en local, ou dans des véhicules pour échapper à la police. On a même pu voir, un émetteur se faire trimbaler sur une mobylette. Les émissions était préenregistrées sur des cassettes.",
        url        : 'https://radiogalere.org/'
    },
    'Radio Grenouille (88.8)'                   : {
        name       : 'Radio Grenouille (88.8)',
        description: "📍Friche Belle de Mai | 41, rue Jobin |13003 <br><br>MarseilleRadio Grenouille est une radio associative et locale née en 1981 avec une antenne diffusant 24h/24 sur le 88.8 FM et en DAB + . L’antenne est constitué d’une programmation musicale d’une part et de programmes éditoriaux de proximité, axés sur des questions de culture urbaine, de quartiers, de citoyenneté, d’écologie d’autre part. Elle partage l’approche participative et le souci d’un travail de proximité des médias locaux, mais est écoutée par un public plus large, plus diversifié, sensible à la notion de citoyenneté et du « vivre ensemble ». <br>Nous déclinons cette pensée dans notre approche concrète par un « faire ensemble », en s’associant et travaillant avec ceux qui agissent dans la ville et le département sur ces thématiques. <br>Nous aspirons à valoriser la parole de l’ensemble de ces personnes, en la restituant au plus proche de son sens, par un travail éditorial approfondi tant sur le choix des thématiques, la préparation que sur le montage et la mise en ondes des productions réalisées.",
        url        : 'https://www.radiogrenouille.com/'
    },
    'Radio Gazelle (98.0)'                      : {
        name       : 'Radio Gazelle (98.0)',
        description: "A Marseille, ville cosmopolite où le nombre des communautés d'immigrés est particulièrement important, le climat social est très tendu. Chômage et pauvreté, supérieurs à la moyenne nationale, côtoient une intolérance de plus en plus exacerbée.C'est dans ce contexte que, en 1981, quelques jeunes Maghrébins créent une radio dans un quartier nord de Marseille. Au tout début, il s'agit d'une « radio-pirate ». Les émissions diffusées se font en toute illégalité, avec un émetteur de 1OO watts. La démarche de ces jeunes s'explique par « une volonté politique de prendre la parole, d'informer leur communauté et de veiller au respect de ses droits ». Les premières émissions diffusent esssentiellement des débats passionnés et de la musique maghrébine.",
        url        : 'http://radiogazelle.net'
    },
    'Radio BAM'                                 : {
        name       : 'Radio BAM',
        description: 'RadioBAM est une association Loi 1901 créée en 2011 par des amis en manque de qualité sonore et de représentation sincère de la cité phocéenne. Nous voulons promouvoir la vie artistique et associative marseillaise, mettre en lumière ce riche et complexe bouillon de culture que Marseille mélange, et donner une voix à tous ceux qui font bouger nos quartiers. ',
        url        : 'http://www.radiobam.org/a-propos/manifesto/'
    },
    'Radio Zinzine (Limans)'                    : {
        name       : 'Radio Zinzine (Limans)',
        description: "Radio Zinzine est une radio d'opinion, de caractère, qui tient à son indépendance financière, refusant toute publicité, s'appuyant sur le Fonds de soutien à l'expression radiophonique, des concerts de soutien, des collectes auprès des auditrices et auditeurs, etc. Une indépendance qui n'en est pas moins un état d'esprit adossé à notre volonté, et la vôtre ..",
        url        : 'https://www.radiozinzine.org/'
    },
    'Radio Zinzine (Aix)'                       : {
        name       : 'Radio Zinzine (Aix)',
        description: 'Radio Zinzine Aix est une radio libre qui diffuse depuis 1998 ses programmes sur les ondes du 88.1FM et sur internet.',
        url        : 'https://www.radiozinzineaix.org/'
    },
    Podcasts                                    : {
        name       : 'Podcasts',
        description: '',
        url        : ''
    },
    'Transféminisme*'                           : {
        name       : 'Transféminisme*',
        description: 'Féminisme « intersectionnel » / transversal',
        url        : undefined
    },
    'Un podcast à soi'                          : {
        name       : 'Un podcast à soi',
        description: 'Chaque mois, Un Podcast à soi mêle intimité et expertise, témoignages et réflexions, pour aborder les questions de genre, de féminismes, d’égalité entre les femmes et les hommes. Un podcast de Charlotte Bienaimé pour ARTE Radio.',
        url        : 'https://open.spotify.com/show/2v0aWpQH9ZJtNHMkokrjmh?si=416382068d944420'
    },
    'Les Couilles sur la table'                 : {
        name       : 'Les Couilles sur la table',
        description: 'Le premier podcast sur les masculinités. Un jeudi sur deux, Victoire Tuaillon parle en profondeur d’un aspect des masculinités contemporaines avec un·e invité·e. Parce qu’on ne naît pas homme, on le devient. Les Couilles sur la table est un podcast de Binge Audio.',
        url        : 'https://open.spotify.com/show/3xk078ZrBB5X75zQzHEHRN?si=66a7b777babb43a9'
    },
    'Le Coeur sur la table'                     : {
        name       : 'Le Coeur sur la table',
        description: "Parce que s'aimer est l'une des façons de faire la révolution. Des épisodes documentaires pour réinventer nos relations amoureuses, nos liens avec nos ami·es, notre famille, nos amant·es. Le Cœur sur la table est un podcast documentaire de Victoire Tuaillon produit par Binge Audio.",
        url        : 'https://open.spotify.com/show/3nKx7fPeDRwLr66Kpk8D2X?si=d2eaa1bcf1594ba9'
    },
    'Un monstre qui vous parle'                 : {
        name       : 'Un monstre qui vous parle',
        description: 'En novembre 2019, Paul B. Preciado s’exprime devant 3500 psychanalystes lors des journées internationales de l’Ecole de la Cause Freudienne à Paris et en appelle à une remise en question fondamentale : « Continuer à pratiquer la psychanalyse en utilisant la notion de différence sexuelle, avec des instruments cliniques comme le complexe d’Œdipe, est aussi aberrant que de prétendre que la terre est plate. » La conférence provoque un véritable séisme dans l’auditoire et depuis les associations psychanalytiques se déchirent. Filmé par des smartphones, le discours est mis en ligne et des fragments sont retranscrits, traduits et publiés sur internet sans souci d’exactitude. Le texte est désormais disponible dans son intégralité et le philosophe vient ce soir, en talentueuse compagnie, en faire entendre des extraits.',
        url        : 'https://youtu.be/0iL0yAE4sAE?list=TLPQMjMwNTIwMjSLvdgtrkfWLQ'
    },
    'Paul B. Preciado, trans-philosophe'        : {
        name       : 'Paul B. Preciado, trans-philosophe',
        description: "Portrait de Paul B. Preciado, philosophe performeur plutôt que sage, et qui travaille à partir du corps, non comme objet anatomique mais comme archive politique vivante constituée d'un ensemble de représentations, et qui fait de sa vie une plateforme d’expérimentations philosophiques.",
        url        : 'https://www.radiofrance.fr/franceculture/podcasts/les-chemins-de-la-philosophie/paul-b-preciado-trans-philosophe-6487363'
    },
    'Paul B. Preciado : trans révolutionnaire'  : {
        name       : 'Paul B. Preciado : trans révolutionnaire',
        description: "Paul B. Preciado a fait de son propre corps un laboratoire politique, un chemin philosophique, comme il l'explique au micro de Sonia Devillers : « Je ne me suis jamais senti uniquement une femme ou un homme. Et donc quand j'ai commencé à travailler sur ces questions du genre, de l'identité sexuelle aussi, j'ai transformé mon corps quelque part en laboratoire politique. Je ne suis pas le seul. Je pense que beaucoup de philosophes ont fait ça. Même Freud, en fait, il a fait aussi une auto-analyse. J'ai commencé à un moment donné, quand j'avais la trentaine à prendre de la testostérone, je suis devenu ce qu'on appelle aujourd'hui un homme trans, même si je ne crois pas à cette binarité. Je me sens plutôt dissident du système de sexe et de genre. Mais aujourd'hui, j'ai un passeport, donc une fiction politique dans ma poche qui dit que je suis un homme. »",
        url        : 'https://www.radiofrance.fr/franceinter/podcasts/l-invite-de-9h10/l-invite-de-sonia-devillers-du-mardi-20-decembre-2022-8258552'
    },
    'Psychothérapie Institutionnelle'           : {
        name       : 'Psychothérapie Institutionnelle',
        description: 'Mouvement de transformation radicale des pratiques psychiatriques développé dans les années de guerre et d’après guerre, notamment par le travail du psychiatre-psychanalyste François Tosquelles à Saint-Alban, suivi plus tard par le psychiatre Jean Oury et par le militant, philosophe et psychanalyste Félix Guattari, fondateurs de la clinique de La Borde. Une phrase peut résumer leur approche : le soin est nécessairement collectif car c’est bien le collectif la source de tout trouble psychique – laissant de côté, pour simplifier, les causes disons « purement » biologiques –. Pour soigner une personne il faut soigner le collectif, c’est-à-dire soigner les institutions. D’où l’expression « psychothérapie institutionnelle ». Il s’agit de produire, de faire fonctionner un ensemble d’institutions nécessaires pour constituer un collectif-soignant, en reproduisant le moins possible les conditions aliénantes, pathogènes et normalisées de la société. ',
        url        : 'https://fr.wikipedia.org/wiki/Psychoth%C3%A9rapie_institutionnelle'
    },
    'De Saint-Alban à La Borde - France Culture': {
        name       : 'De Saint-Alban à La Borde - France Culture',
        description: "<b>Qu'est-ce que la psychothérapie institutionnelle ? En quoi peut-elle être considérée comme l'une des grandes aventures médicales, intellectuelles et politiques du 20e siècle ? Qui étaient François Tosquelles et Jean Oury ? C'est ce que rappelle ce programme d'archives proposé par Albane Penaranda.</b><br><br>Il y a soixante-dix ans cette année, ouvrait près de Blois, la Clinique de La Borde. Cet anniversaire est l'occasion toute trouvée pour consacrer un programme d'archives à la psychothérapie institutionnelle dont La Borde est devenue l'établissement emblématique. Qu'est-ce que la psychothérapie institutionnelle ? En quoi peut-elle être considérée comme l'une des grandes aventures médicales, intellectuelles et politiques du 20e siècle ? Qui étaient François Tosquelles et Jean Oury ? Comment ont-ils respectivement contribué de manière décisive au développement en France de la psychothérapie institutionnelle dans la psychiatrie ?<br><br> <b>Pour soigner les malades, il faut soigner l'hôpital</b><br>Psychiatre et psychanalyste, combattant antifasciste de la Guerre d'Espagne, François Tosquelles fut le premier, dès 1940, à développer cette toute autre manière d'accueillir la maladie mentale et les malades mentaux à l'Hôpital de Saint-Alban en Lozère, qui fut aussi durant l'Occupation un lieu de résistance, un lieu de refuge pour tous les pourchassés. Psychiatre et psychanalyste lui aussi, ayant passé deux ans comme interne à Saint-Alban après-guerre, ce fut en héritier de Tosquelles que Jean Oury quitta un jour la clinique du Loir-et-Cher où il était en poste en emmenant avec lui sur les routes une trentaine de malade pour fonder en 1953 la Clinique de la Borde.<br><br><b>En quoi la psychothérapie institutionnelle est-elle révolutionnaire ? </b><br>Convaincus que prétendre soigner les malades sans soigner l'hôpital était de la folie, François Tosquelles et Jean Oury se seront appliqués à mettre en pratique leurs convictions et à développer en pionniers dans les établissements psychiatriques qu'ils dirigeaient «un ensemble de méthodes destinées à résister à tout ce qui est concentrationnaire ou ségrégatif», pour reprendre les propres termes de Jean Oury. Comment cela s'est-il traduit à Saint-Alban, puis à La Borde ? C'est ce que nous diront les archives de cette Nuit.<br>Peu avant sa mort en 1994, François Tosquelles disait ceci : « Malgré la confusion et le pessimisme où se trouvent engagés l’ensemble des hommes [...] je reste convaincu que tant qu’il y a des hommes à la surface du monde, quelque chose de leur démarche reste acquis, se retransmet, disparaît parfois, mais aussi ressurgit quoi qu’il en soit des catastrophes mortifères qui nous assaillent souvent ».<br>En un temps où un vent contraire semble s'être levé sur la psychiatrie, que ce programme soit un modeste encouragement adressé à tous ceux qui travaillent chaque jour à ce que perdure l'essentiel de ce qui était né à Saint-Alban et s'est ensuite affirmé à La Borde.",
        url        : 'https://www.radiofrance.fr/franceculture/podcasts/selection-de-saint-alban-a-la-borde-la-psychotherapie-institutionnelle'
    },
    'Revues | Journaux'                         : {
        name       : 'Revues | Journaux',
        description: '',
        url        : undefined
    },
    'Revue Rhizome'                             : {
        name       : 'Revue Rhizome',
        description: 'Articulation, souplesse, nomadisme. Transversalité.',
        url        : 'https://www.instagram.com/rhizome_13/'
    },
    'Revue Charbon'                             : {
        name       : 'Revue Charbon',
        description: "<b>REVUE CHARBON</b> interroge la place et les effets du travail dans nos vies. Elle n’établit aucune hiérarchie entre analyses théoriques, lettres de démission ou récits d’expériences illustrés. Elle est éditée bénévolement à Marseille par une équipe non-professionnelle et curieuse.<br> Elle se pose résolument en faveur de l’émancipation collective et contre le mythe de la valeur travail. <br>Elle parait sitôt la quantité de contributions nécessaire recueillie ou quand elle a le temps. Chômeur·euse·s, workaholics, démissionnaires en puissance, toute contribution est bienvenue !",
        url        : 'https://www.instagram.com/editions_charbon/'
    },
    SoinSoin                                    : {
        name       : 'SoinSoin',
        description: 'Journal de réflexion sur le soin psychiatrique',
        url        : 'https://soinsoin.fr/'
    },
    "Sang D'encre"                              : {
        name       : "Sang D'encre",
        description: "SaNg d'EnCRe est une revue d'information et d'expression collective qui aborde des questions liées à la santé, aux modes de vie précaires… Elle rassemble des écrits, du feuilleton à la recherche en passant par des témoignages, de la bande dessinée, des interviews, des billets d'humeur.Se mêlent à ces écrits des créations graphiques réalisées en ateliers mais aussi envoyées par des contributeurs extérieurs.",
        url        : 'https://sangdencre.nouvelleaube.org/'
    },
    'Les Cahiers A2C'                           : {
        name       : 'Les Cahiers A2C',
        description: "🌊Par, pour et dans le mouvement: construisons l'autonomie de notre classe!",
        url        : 'https://www.autonomiedeclasse.org'
    },
    'Un Autre Monde'                            : {
        name       : 'Un Autre Monde',
        description: "Un autre Monde est un journal social et culturel marseillais, dit journal de rue, vendu par des personnes en situation de grande précarité. Devenues colporteuses et colporteurs de presse, ces personnes bénéficient d’une partie des ventes et participent pleinement à la vie du journal et à sa conception. Espace d’échanges et de rencontres, le journal invite les auteurs, illustrateurs, photographes, amateurs, colporteurs, personnes de tout horizon à écrire et à réfléchir sur un thème commun. Le journal se veut ouvert et fédérateur. Il se propose de multiplier les liens avec les structures sociales, éducatives et alternatives, d’évoluer en parallèle des actualités culturelles de la ville et des initiatives sociales et solidaires et d’aller à la rencontre des habitants avec des points de vente fixes dans des librairies, musées, cinémas, théâtres, festivals et des ventes ambulantes sur les lieux publics, afin de rendre visible cet autre monde et de le construire ensemble et maintenant.<br><br>Un journal vendu par des colporteurs de presse<ul><li>En vente ambulante dans les lieux publics de la ville de Marseille</li><li>Avec des points de vente fixes en partenariat avec des lieux et des évènements culturels de Marseille</li><li>En dépôt solidaire en librairie et autres commerces de Marseille</li></ul>Les partenaires<ul><li>Yaya Collectif</li><li>Radio BAM</li><li>La Cloche Sud</li><li>La Bagagerie</li><li>L'Atelier des Artistes en Exil</li><li>L'ADPEI</li><li>Coco Velten</li></ul>Impression CCI Imprimerie – Marseille<br><br><b>Association UN AUTRE MONDE</b><br>📍 Cité des Associations | 93 La Canebière | 13001<br><br>",
        url        : 'https://www.journalunautremonde.com/'
    },
    InfoKiosques                                : {
        name       : 'InfoKiosques',
        description: '',
        url        : undefined
    },
    'Folie et Politique -Barge'                 : {
        name       : 'Folie et Politique -Barge',
        description: "Cet infokiosque compilé par Héloïse K., autrice de BARGE, contient des centaines de ressources à imprimer sur les thèmes suivants : <br><ul> <li>Addictions</li><li> Droits des usagers</li><li> Folie et politique</li><li> Empowerment</li><li> Entendeurs de voix</li><li> Fanzines</li><li> Les thérapies, l'hospitalisation, les lieux de soin</li><li> Les troubles</li><li> Médicament</li><li> Outils de rétablissement</li><li> Soutien et entraide mutuels, pairs aidants, usagers</li><li> Autres sources et liens</li></ul><br> Cette collection a été mise à jour le 31 mai 2022. Vous pouvez la télécharger entièrement : infokiosque.zip (896 Mo) <br><br>",
        url        : 'https://infokiosque.entre.la/'
    },
    'Répertoires'                               : {
        name       : 'Répertoires',
        description: '',
        url        : undefined
    },
    TransFriendly                               : {
        name       : 'TransFriendly',
        description: "Carte collaborative des praticien‧nes trans-friendly. Elle compte à ce jour plus 3000 adresses recommandées, en France comme à l'étranger, ainsi que presque 500 adresses à éviter.",
        url        : 'https://fransgenre.fr/#ressources'
    },
    PsySafe                                     : {
        name       : 'PsySafe',
        description: "<b>PSY* Situé·es prenant en compte les oppressions systémiques</b><br><br> Qu'entend-on par «situé·es» ? <br>Une pratique située veille à garantir un espace dans lequel on s'attache à ne pas reproduire les oppressions systémiques existant dans le champ social : racismes, patriarcat, validisme, classisme, âgisme, transphobie...<br> Un.e psy* situé·e a conscience de ces discriminations, connaît ces effets et conséquences, les prend en compte et cherche à les exclure de ses propres pratiques.<br><br>",
        url        : 'https://psysafeinclusifs.wixsite.com/psysafe'
    },
    Juridique                                   : {
        name       : 'Juridique',
        description: '',
        url        : ''
    },
    'Réseaux'                                   : {
        name       : 'Réseaux',
        description: '',
        url        : undefined
    },
    'Actions Communes'                                   : {
        name       : 'Actions Communes',
        description: "<h4>Le réseau des communes et collectifs participatifs</h4>Nous sommes un réseau de collectifs citoyens engagés dans nos communes, villes et villages. La plupart de ces collectifs se sont présentés aux élections municipales de 2020 et certains sont aujourd’hui élu.es dans leur commune.<br><br>Notre ambition est de porter haut la voix des citoyennes et citoyens, que nous sommes. C’est pour cela que l’on défend un monde social plus équitable ; une justice climatique capable d’endiguer l’extinction exponentielle du vivant, à notre échelle. Enfin, la condition de possibilité d’un tel projet, repose sur la mise en place d’une démocratie radicalement transformée.<ul><li>D’abord en instituant une gouvernance partagée au coeur des pouvoirs publics. Pour que le.la citoyen.ne décide.</li><li>Ensuite, et dans le même temps, redonner du pouvoir d’agir aux habitants. Pour que tout le monde décide des politiques dans l’avenir.</li></ul>",
        url        : "https://www.actionscommunes.org/"
    },
    RezoProspec                                 : {
        name       : 'RezoProspec',
        description: "rezopropec signifie : RESeau PROfessionnel du SPEctacle et de la Culture. C'est une plateforme communautaire gratuite de mutualisation de moyens, d'échange de services, de partage de savoir et d'entraide destinée à faciliter le partage de message entre toutes les personnes physiques  pour aider à la production, la diffusion de l'art, au développement culturel et des pratiques artistiques en Région Provence Alpes Cote d'Azur.",
        url        : 'https://www.facebook.com/rezoprospec'
    },
    'Réseau Les Paniers Marseillais (AMAP)'     : {
        name       : 'Réseau Les Paniers Marseillais (AMAP)',
        description: "Réseau des Paniers Marseillais. Associations pour le Maintien d'une Agriculture Paysanne",
        url        : 'https://lespaniersmarseillais.org'
    },
    'Réseau COFOR'                              : {
        name       : 'Réseau COFOR',
        description: '<b>Le CoFoR est un Centre de Formation au Rétablissement basé à Marseille</b><br> <br> Il s’adresse aux personnes, vivant en région PACA, qui ont ou ont eu des troubles psychiques, qui souhaitent se rétablir et avoir une vie plus satisfaisante.<br> <br> Parce que son approche est différente, le CoFoR est complémentaire aux structures existantes en santé mentale, les personnes en formation sont des étudiant·es. Elles sont actrices de leur rétablissement et contribuent à faire évoluer le projet du CoFoR. <br> <br><b>Venez nous rencontrer !</b><br> À l’occasion d’un focus group<br>un lundi de 14h à 16h à la Cité des associations <br> <br>',
        url        : 'https://www.coforetablissement.fr/'
    },
    'Projet ASSAB'                              : {
        name       : 'Projet ASSAB',
        description: "PROJET DE MISE EN RÉSEAU EN FAVEUR DE L'ACCÈS AUX DROITS ET DE L'ACCÈS AUX SOINS ET LA CONTINUITÉ DES SOINS POUR LES PERSONNES SANS ABRI À MARSEILLE.",
        url        : 'https://projetassab.org/'
    },
    TRUC                                        : {
        name       : 'TRUC',
        description: " <b>Terrain de Rassemblement pour l'Utilité des Clubs</b><br><br>Le TRUC est un collectif <i>[ qui travail la mise en réseau de clubs thérapeutiques, de groupes d'autosupport, des GEM, … , etc ]</i> fondé sur l'esprit club et garant de l'équité soignants-soignés: chacun a les mêmes droits de parole, de décision avec le souci d'une transversalité du « pouvoir » située en dehors des statuts.<br>C'est un espace imaginé pour faciliter la rencontre mais qui n'est pas décisionnaire. Les prises de positions se feront collégialement et sur des points concernant le collectif lui-même. Il ne s'agit pas de créer une autre strate bureaucratique ni une superstructure.<br>Ce que nous avons en commun ce n'est pas la gestion mais des expériences, des idées, des envies, des difficultés à mettre au travail.<br>La Reconnaissance et le respect de la singularité des pratiques et expériences de chacun sont au coeur de nos préoccupations. Il ne s'agit pas de s'ériger en modèle de ce que serait un club. Le club comme outil associatif et thérapeutique, adossé à une psychiatrie humaine, se soucie de la reconnaissance des droits de l'homme et du citoyen, de la personne en tant que sujet, de sa dignité et du respect qu'on lui doit.<br>L'accueil de l'autre dans sa différence, la prise en compte de son savoir faire, de son expérience et de ses capacités, sont essentielles dans ce travail d'échanges.<br>Notre démarche se situe donc dans l'ouverture d'un champ de création d'un réseau d'accueil des clubs. Etayé sur des expériences concrètes de rapprochements inter-club, ce nouvel espace fédérateur, lieu de circulations, de rencontres, de liberté et d'échanges, nous le pensons comme un club au cube, un outil thérapeutique qui nous déplace, nous sort de l' “entre soi”, nous ouvre le champ des possibles. L'amorce du « faire ensemble » est déjà en marche avec les forums inter-club, les séjours thérapeutiques en commun, l'écriture d'un article à 4 mains pour la revue Institutions et la rédaction de ce manifeste.<br>Il s'agirait également de se saisir de cet élan qui nous pousse à penser, à réinventer sans cesse la fonction club et l'inscrire dans l'actuel, avec les nouvelles modalités de notre époque. <br>La perspective est politique dans le sens du politique dont parlait Jean Oury. Elle est aussi poétique. L'idée même de ce collectif offre une ouverture et un soutien aux personnes isolées (soignants comme soignés).<br>Transmettre, défendre et résister plus fort ensemble face aux attaques actuelles d'une société de plus en plus normalisatrice sont les maîtres mots de notre démarche.<br>Il existe des équivalents de fédérations de soignants ou de soignés mais c'est la première fois qu'un collectif aussi large se constitue. C'est un outil thérapeutique et politique comme défini préalablement.<br>Alors, quelque soit ton nom (Club Thérapeutique, GEM, Asamblea de Communauté Thérapeutique, association, collectif) et si tu partages les valeurs qui sont les nôtres, rejoins-nous pour poursuivre l'aventure et écrire l'histoire de ce collectif naissant. Il sera ce que nous en ferons tous ensemble.",
        url        : 'https://www.facebook.com/groups/clubstherapeuthiques'
    },
    'Réseau des Créfad'                         : {
        name       : 'Réseau des Créfad',
        description: 'Le Réseau des Créfad coordonne 16 associations qui œuvrent en réseau pour construire et agir ensemble et avec d’autres. L’inter-associatif est inscrit dans les statuts du Réseau et dans ses pratiques (priorités d’action).<br><br>Nous sommes impliqués dans des espaces collectifs plus larges comme le CELAVAR Auvergne-Rhône-Alpes (comité d’étude et de liaison des associations à vocation agricole et rurale), le Réseau National des Espace-Test Agricoles (RENETA), Piments (Réseau des espaces-test pour les activités des jeunes), le Réseau International des Hautes Études des Pratiques Sociales (RIHEPS). Nous travaillons en grande proximité avec le Réseau des Cafés Culturels et Cantines Associatifs.<br><br>Les associations membres du Réseau des Créfad se reconnaissent dans des valeurs communes en référence au Manifeste de Peuple et Culture. Et depuis peu elles se sont donnés des mots pour nommer ce qui les relient. Ils ont fait l’objet d’un texte Faire mouvement, faire réseau.',
        url        : 'https://reseaucrefad.org/'
    },
    'Ateliers Vélo'                             : {
        name       : 'Ateliers Vélo',
        description: '',
        url        : undefined
    },
    'Vélo Sapiens'                              : {
        name       : 'Vélo Sapiens',
        description: "📍39 rue Mazagran, 13001 Marseille<br><br>VÉLO SAPIENS est une association pour la promotion de l'usage du vélo dans Marseille et ses environs.<br><br>Vous voulez réparer ou apprendre à réparer votre vélo ? Notre atelier met à votre disposition outillage et conseils, avec l'aide des bénévoles.<br>Vous cherchez un vélo d'occasion, adapté à vos besoins autant qu'à vos moyens ? Vous en trouverez peut-être un chez nous, ou on vous aidera à le monter vous-même.<br>Vous avez un vélo dont vous ne vous servez plus ? Donnez-le nous, il aura une seconde vie et fera un heureux, ou permettra à un autre vieux vélo de reprendre la route.<br>Vous hésitez à faire du vélo en ville, par crainte de la circulation, du vol ou des montées ? On vous informera sur les faux problèmes, et on vous aidera pour les vrais !<br>Vous voulez plus d'aménagements cyclables, une meilleure prise en compte du vélo, une ville plus respirable ? Ensemble nous serons plus forts pour être écoutés.",
        url        : 'http://www.velosapiens.fr/'
    },
    'Collectif Vélos en Ville'                  : {
        name       : 'Collectif Vélos en Ville',
        description: "Atelier solidaire de réparation de vélo. Ce lieu permet également une activité de recyclage des vélos et de marquage anti-vol.<br><br>L'association gère aussi une activité de Vélo-Ecole pour adultes, de stage de cyclistes urbains, des sorties à thèmes deux fois par mois, auxquelles participent des personnes non-voyantes sur tandem. L'objectif du Collectif est : <br>Promouvoir l'usage du vélo à Marseille, chercher des remèdes aux problèmes qui peuvent dissuader les Marseillais d'utiliser ou réutiliser le vélo. Il participe à des concertations et s'associe à diverses manifestations.<br><br> 📍24 Rue Moustier, 13001<br> 📞09 54 58 88 77",
        url        : 'http://velosenville.org'
    },
    Festivals                                   : {
        name       : 'Festivals',
        description: '',
        url        : ''
    },
    'Relève'                                    : {
        name       : 'Relève',
        description: 'Hello, bienvenue au RELÈVE Festival ! <br> <br> Nous c’est ESPER PRO*, une association de médiateurs·rices en santé mentale. En collaboration avec l’UNAFAM* nous avions envie d’aborder le sujet avec vous le temps d’un week-end, <b>du 25 au 26 mai</b>, en entrée libre, au Dock des suds – Marseille.<br> <br> L’idée est de se réunir, partager des ressources, s’informer, agir pour l’inclusion, montrer que les lignes bougent dans le domaine de la santé mentale et que nous sommes tous·tes acteur·ices de ce changement ! Et pour ça, nous vous proposons : <ul><li>40 stands de sensibilisation et d’informations</li><li>2 scènes</li><li>16 heures de musique</li><li>100 intervenant·es </li><li>15 conférences thématiques</li><li>20 ateliers interactifs</li></ul>Rejoins-nous pour le RELÈVE Festival, une invitation à parler de santé mentale autrement, ce festival est ouvert à tous·tes. <br> <br><i>*ESPER PRO, première plateforme employeuse de pairs aidants dont elle soutient la professionnalisation. Accueillante, dynamique et innovante, vient en soutien à des personnes concernées qui souhaitent améliorer ou maintenir leur santé psychique.<br>*L’UNAFAM, association reconnue d’utilité publique. Accueille, soutient, forme, se bat pour l’effectivité des droits des personnes concernées et de leurs familles et lutte contre les préjugés.</i>',
        url        : 'https://www.releve-festival.com/'
    },
    Outils                                      : {
        name       : 'Outils',
        description: '',
        url        : undefined
    },
    'Calculs Mutuelle'                          : {
        name       : 'Calculs Mutuelle',
        description: 'En dévelopement',
        url        : 'https://cryptpad.fr/sheet/#/2/sheet/edit/QdZ7ULUcigjF0MNAZ8S7E742/'
    },
    'Numériques'                                : {
        name       : 'Numériques',
        description: '',
        url        : undefined
    },
    Visualisation                               : {
        name       : 'Visualisation',
        description: '',
        url        : undefined
    },
    'LiveGap Charts'                            : {
        name       : 'LiveGap Charts',
        description: 'Créateur de graphiques en ligne avec aperçu en direct | Gratuit, sans inscription ou téléchargement',
        url        : 'https://charts.livegap.com/index.php?lan=fr'
    },
    'Graph Maker'                               : {
        name       : 'Graph Maker',
        description: 'Créateur de graphiques en ligne avec aperçu en direct | Gratuit, sans inscription ou téléchargement',
        url        : 'https://graphmaker.imageonline.co/index-fr.php'
    },
    'Savoir-faire'                              : {
        name       : 'Savoir-faire',
        description: 'Connaissances ou techniques oubliées ou non-médiatisées',
        url        : undefined
    },
    Autonomie                                   : {
        name       : 'Autonomie',
        description: '',
        url        : undefined
    },
    'Sélection Rhizome'                         : {
        name       : 'Sélection Rhizome',
        description: '',
        url        : undefined
    },
    'Le moteur stirling'                        : {
        name       : 'Le moteur stirling',
        description: '',
        url        : 'https://youtu.be/duuk_r--lqU'
    },
    'Mouche Soldat Noir'                        : {
        name       : 'Mouche Soldat Noir',
        description: 'Larves, compostage rapide, haut contenu de protéine (40%) et de graisse (30%) <br><br> Cliquez pour accèder au tutoriel',
        url        : 'https://wiki.lowtechlab.org/wiki/Elevage_de_Mouches_Soldats_Noires/fr'
    },
    Aquaponie                                   : {
        name       : 'Aquaponie',
        description: '',
        url        : ''
    },
    Biodigesteur                                : {
        name       : 'Biodigesteur',
        description: "Un biodigesteur est une solution technique de valorisation des déchets organiques utilisée pour produire un gaz combustible (le biogaz) et un fertilisant (le digestat). La particularité du biodigesteur est que la dégradation est réalisée par des bactéries dans un milieu privé d'oxygène, on parle de fermentation anaérobique. <br>Le biogaz est un mélange de gaz contenant principalement du méthane, il peut être utilisé pour alimenter un bruleur de gazinière ou de chaudière ou bien comme combustible pour des moteurs.<br>La fermentation méthanogène qui se produit dans le biodigesteur existe dans la nature. C'est par exemple ce qui se produit dans les marais lorsque de la matière organique se décompose sous l'eau. Les feu-follets sont de petites torchères de biogaz.<br><br>La domestication du biogaz remonte au début du XIXe siècle et le nombre et la variété de biodigesteurs n'ont cessé de croitre depuis. Ils sont particulièrement présents dans les pays en développement de la ceinture tropicale où la petite paysannerie s'autonomise en énergie grâce à leur production de gaz avec leurs déchets organiques. La chaleur étant un catalyseur important de la fermentation, sous ces latitudes, de petites unités sont économiquement intéressantes.<br>En France et dans certains pays, le coût de l'énergie étant très faible par rapport à celui de la main d'œuvre, peu de petits digesteurs existent. Cependant de nombreuses installations industrielles équipent les stations d'épurations et les grands élevages agricoles.<br><br>Il existe plusieurs types de biodigesteurs, continus ou discontinus, et avec des plages de production selon la température (psychrophile : 15-25°C, mésophile : 25-45°C ou thermophile : 45 – 65°C). Nous allons étudier les biodigesteurs continus mésophiles à 38°C, solutions les plus utilisées en zone tempérée.<br>La caractéristique principale de ce système est sa ressemblance avec un système digestif. Tout comme lui, il cultive des bactéries, a besoin d'une certaine température pour être efficace et reçoit une alimentation régulièrement.<br>Dans un compost, en milieu aérobie, la décomposition des matières organiques conduit à la formation de gaz (H2S, H2, NH3) et à une production de chaleur importante. Seule la décomposition à l'abri de l'air conduit à la formation du méthane. C'est une des raisons pour laquelle la fermentation a lieu dans une cuve étanche.Dans ce tutoriel nous allons étudier les différents éléments constituants un biodigesteur (circuit matière et circuit gaz) et comment l'utiliser.<br>Cette documentation réalisée avec l'association Picojoule retrace la fabrication d'un de leurs prototypes de micro-méthanisation, il ne permet pas l'autonomie en gaz de cuisson mais est une bonne introduction à la biodigestion. Le digesteur semi-enterré d'Hélie Marchand à Madagascar est de plus grande capacité : Biodigesteur<br><br>Les explications sont largement inspirées du travail de Bernard LAGRANGE dans ses ouvrages Biométhane 1 et 2, que nous vous recommandons vivement ! Ce travail est libre et ouvert, n'hésitez pas à le clarifier et le compléter de vos connaissances et expériences.<br><br><i> Source :wiki.lowtechlab.org</i>",
        url        : 'https://youtu.be/f-Lz7vqIai0'
    },
    'Pompe à bélier'                            : {
        name       : 'Pompe à bélier',
        description: "Le bélier hydraulique est une technique qui permet de pomper de l'eau jusqu'à une hauteur plus élevée que la source en utilisant l'énergie d'une chute d'eau de hauteur plus faible, grâce à un dispositif mécanique et hydraulique.",
        url        : 'https://fr.wikipedia.org/wiki/B%C3%A9lier_hydraulique'
    },
    'Énergie'                                   : {
        name       : 'Énergie',
        description: '',
        url        : ''
    },
    Compostage                                  : {
        name       : 'Compostage',
        description: '',
        url        : ''
    },
    'Systèmes Agricoles'                        : {
        name       : 'Systèmes Agricoles',
        description: '',
        url        : ''
    },
    'Pompes à eau'                              : {
        name       : 'Pompes à eau',
        description: '',
        url        : ''
    },
    "Massalia VOx"             : {
        name       : "Massalia VOx",
        description: "📍15 Boulevard de la Liberté | 13001 <br><br> Massalia VOx est un lieu partagé, créé par des personnes qui souhaitent se l’approprier, faire ensemble et avec les autres, sans discrimination. Un Lieu d’Initiatives et d’Entraide Urbain qui accueille les associations, collectifs, militants, habitantes et habitants, quels que soient leurs âges, origines, handicaps, expériences, expertises.  Il permet de créer des rencontres improbables, d’amener des gens différents à vivre des expériences partagées et à mener ensemble des actions antiracistes, antivalidistes, féministes, de lutte contre toutes les discriminations.<br><br> Massalia VOx est un lieu partagé, le lieu où se rencontrent des personnes de tous horizons, pour échanger et faire ensemble. Lieu d’éducation populaire, il lutte activement contre toutes les discriminations et œuvre au partage et la coproduction des savoirs. Situé au pied de la gare Saint-Charles, Massalia VOx accueille dans ses 130m² les idées et les projets qui souhaitent y éclore. Ici, les voix des marseillaises et marseillais émergent collectivement et s’expriment librement. Chacun·e est force de proposition pour suggérer des ateliers et des événements. Aujourd’hui, nous proposons des événements (débats, projections, expositions, festivals…), des ateliers (de conversation, de self-défense, de bricolage, d’écriture, de cuisine, de jeux….), des permanences (juridique, administrative, accompagnement de projet…) et un centre de ressources documentaire. <br><br> <h3> Le Lieu </h3> Massalia VOx, propose en plein coeur de Marseille, un espace partagé de 130m², accessible à toustes (signaler besoin spécifique), composé de 5 salles : <ul><li> Baya Jurquet – Une grande salle d’activité, modulable et polyvalente (projection, débats, expositions, ateliers d’arts, ateliers sportifs…)</li><li>Ousmane Sembène – Un atelier de création média et arts (résidences d’artistes plasticiens, studio de podcast à venir)</li><li>Charlie Bauer – Un centre de documentation militant (éducation populaire, pédagogies, français langue étrangère, sociologie, urbanisme, histoire décoloniale… et des jeux ! ) et bureau (permanences, entretiens…)</li><li>Ahed Tamimi – Une salle de réunion, formation, recherche, ateliers, conception et prototypage de jeux… </li><li>Kamel Guemari – Une cuisine partagée, accessible et intergénérationnelle, pour faire ensemble et être ensemble !</li></ul> Avec ses multiples et magnifiques portes (merci aux street artists RAMZ, NÔ, JUAN, JOKE), chaque espace a un accès direct à la rue, au niveau de la rue Lafayette ou du 15 boulevard de la Liberté (accès en transports publics, Gare Saint Charles). L’espace est partagé entre ateliers, permanences, événements, et peut être loué ponctuellement.<br><br><h3>Associations fondatrices</h3><ul><li><b>Didac'Ressources</b>: Créée en 2015, Didac’Ressources est une association de loi 1901 reconnue d’intérêt général et centre de formation, dont l’objet est la lutte contre toute forme de discrimination et le partage de savoirs. Elle crée des outils ludo-pédagogiques dans le cadre de son groupe recherche action formation (GRAF), elle forme les formateurs et acteurs sociaux et accompagne l’accès à la formation pour tous, et elle promeut et facilite le partage de savoirs et de ressources (site ressources collaboratif, fonds documentaire – centre ressources, contribution collaborative à l’encyclopédie Wikipédia). Didac’Ressources a développé plusieurs secteurs d’activités : inclusion médiation handicap, laboratoire d’innovation de recherche et d’expérimentation, ingénierie didactique, culture et bien-être pour tous, inclusion numérique, valorisation des compétences.</li><li><b>Eclore</b> : Créée en 2011 pour accompagner et développer les initiatives utiles et originales, Eclore soutient les porteurs de projets solidaires et citoyens, et crée et partage des outils et des espaces de rencontre en ce sens (outils pédagogiques et ressources documentaires papiers et numériques, jeux éducatifs, formation, conseil individualisé, rencontres associations-financeurs, portage de projet, etc). L’association a également un laboratoire de projets avec une volonté d’innovation méthodologique et de réflexivité sur ses pratiques. Elle a notamment mis en place des projets sur la participation citoyenne des jeunes par la création média, l'esprit critique et le dialogue, la lutte contre les discriminations systémiques, et la réciprocité et l'interculturalité !</li></ul>",
        url        : "https://massaliavox.fr/"
    },  
    "L'Astronef"             : {
        name       : "L'Astronef",
        description: "Centre Hospitalier Edouard Toulouse<br>📍118 Chemin de Mimet | 13015 <br><br><i>Astronef (n.m.) : Vaisseau spatial pour la navigation interplanétaire.</i> <br><br>Rapprocher des planètes qui trop souvent s’ignorent pour partager collectivement des créations et des moments de plaisir, c’est la mission que s’est donnée le théâtre de l’Astronef !<br><br> L’Astronef, à sa mesure, tient à participer à l’œuvre émancipatrice de l’art et au maintien du lien social que réalise la culture. Sa position, à la fois au sein des quartiers nord de Marseille et dans l’enceinte d’un lieu de soin en santé mentale - deux lieux où la culture n’est pas toujours facile d’accès - ne fait que renforcer cette idée.<br><br>Le public qui fréquente le théâtre est donc divers, mouvant et se réinvente à chaque spectacle !<br><br> Un défi réussi pour l'Astronef qui a vu sa fréquentation doubler depuis 2021. Dans ce sens, le tarif unique de moins de 10 € par personne est proposé pour favoriser l’ouverture à la mixité des publics.<br><br> Dans une volonté constante d’ouverture, des artistes « complices », sont ainsi forces de proposition au sein du théâtre ! Et les projets n’en finissent de fuser … avec des partenaires institutionnels mais également des bénévoles du théâtre, des soignants ou encore des patients du centre hospitalier Édouard Toulouse.<br><br> Des partenariats avec les écoles du quartier et les centres sociaux sont mis en place tout comme des collaborations avec les acteurs majeurs du paysage culturel marseillais (théâtre La Criée, théâtre de La Cité, cinéma L'Alhambra, Festival Jazz des 5 continents, etc.). Au-delà de ces projets « partagés », des troupes de renommée nationale, parfois repérées au Festival d’Avignon, sont séduites par le projet « hors-normes » de l’Astronef et sont ravis d’y être accueillies pour jouer leur spectacle. Dans un esprit « d’incubateurs d’idées », ce lieu culturel reste constamment ouvert aussi bien aux artistes confirmés qu’aux jeunes talents.<br><br> L’architecture du théâtre, minérale, avec sa grande porte rectangulaire en béton, évoque les temples grecs antiques. On peut y voir l’allusion aux portes de ville de l’architecture orientale. Ainsi, ce lieu est un clin d’œil à Marseille, porte de l’Orient et cité grecque. C’est une invitation à la culture et au voyage… <br><br> <h3>TARIFS</h3> <ul><li>Adhésion à L'ACNA (Association Culturelle pour un Nouvel Astronef): 2€ /saison </li><li>Tarif enfant (moins de 16 ans): 5€ / jour, quel que soit le nombre de spectacles </li><li>Tarif groupe (à partir de 5 personnes): 5€ /pers /jour, quel que soit le nombre de spectacles</li><li>Tarif adulte : 10€ /jour, quel que soit le nombre de spectacles</li></ul>",
        url        : "https://astronef.org/"
    }, 
    "Maisons de Solidarité"             : {
        name       : "Maisons Departamentales de Solidarité",
        description: "<h3>Un lieu d'accueil</h3> La Maison Départementale de la Solidarité vous accueille, vous conseille et vous oriente, notamment sur les dispositifs d’accès aux droits. Elle vous propose un accompagnement individuel, des actions collectives, des consultations (enfants âgés de 0 à 6 ans - femmes enceintes, gynécologiques, conseil d’hygiène alimentaire...) des activités d’éveil du jeune enfant et un accompagnement à la parentalité.<br><br> La Maison Départementale de la Solidarité organise également des bilans en école maternelle, des campagnes de vaccination, vous soutient dans vos difficultés liées à la vie quotidienne (budget, surendettement) ou à votre logement (Fonds de solidarité logement, prévention des expulsions), développe des actions visant l’insertion sociale, favorise les actions liées à la scolarisation de vos enfants, intervient dans le cadre de la protection des personnes mineures et majeures en situation de danger ou de vulnérabilité et réalise des évaluations en vue de l’agrément des familles candidates à l’adoption.<br><br>Au sein de la Maison Départementale de la Solidarité vous trouverez des professionnels aux compétences spécifiques et complémentaires. Sous l’autorité d’un directeur et de ses adjoints, cette équipe est composée d'assistants sociaux, d'éducateurs spécialisés, d'auxiliaires de puériculture, d'éducateurs de jeunes enfants, de puéricultrices, d'infirmières, de sages-femmes, de psychologues, de conseillères en économie sociale et familiale, de médecins et de secrétaires.<br><br>Sur quelques sites interviennent également une diététicienne, une psychomotricienne, une orthoptiste et une orthophoniste. Certains de ces professionnels peuvent être amenés également à vous rencontrer à votre domicile.<br><br> Les 22 maisons départementales de la Solidarité ont chacune un territoire d'intervention déterminé. Vous devez vous adresser à la maison Départementale de la Solidarité la plus proche de votre domicile.<br><br> Elles sont ouvertes au public, du lundi au vendredi, de 8h30 à 12h et de 13h30 à 17h, à l'exception du mardi après-midi.Il est souhaitable de téléphoner au préalable, pour vous renseigner, sur les horaires et jours de consultation ou pour rencontrer une assistante sociale ou un référent social.",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/",
        	
    },     
    "MDS COLBERT | 01"             : {
        name       : "MDS COLBERT | 01",
        description: "📍14 Rue Sainte Barbe | 13001 <br>☎️ 04 13 31 59 17 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
    
    "MDS DU LITTORAL | 02"       : {
        name       : "MDS DU LITTORAL",
        description: "📍 18 Avenue Robert Schuman, Immeuble le Schuman | 13002 <br>☎️ 04 13 31 76 75 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
    "MDS BELLE DE MAI | 03"             : {
        name       : "MDS BELLE DE MAI",
        description: "📍 24 rue Jobin | 13001 <br>☎️ 04 13 31 65 10 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
    "MDS CHARTREUX | 04"             : {
        name       : "MDS CHARTREUX",
        description: "📍21 Rue Pierre Roche | 13004 <br>☎️ 04 13 31 67 13 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
     "MDS ST SEBASTIEN | 06"             : {
        name       : "MDS ST SEBASTIEN",
        description: "📍66 Rue Saint Sébastien | 13006 <br>☎️ 04 13 31 72 72 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
    "MDS LE NAUTILE | 13"             : {
        name       : "MDS LE NAUTILE",
        description: "📍29 Avenue de Frais Vallon, Immeuble Le Nautile | 13013 <br>☎️ 04 13 31 57 77 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
     "MDS MALPASSÉ | 13"             : {
        name       : "MDS MALPASSÉ | 13",
        description: "📍15 rue Raymonde Martin | 13013 <br>☎️ 04 13 31 06 50 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
     "MDS PONT DE VIVAUX | 10"             : {
        name       : "MDS PONT DE VIVAUX",
        description: "📍250 boulevard Mireille Lauze | 13010 <br>☎️ 04 13 31 53 13 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
     "MDS SAINT MARCEL | 11"             : {
        name       : "MDS SAINT MARCEL",
        description: "📍10 Route de la Valentine | 13011 <br>☎️ 04 13 31 75 01 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
     "MDS DE BONNEVEINE | 08"             : {
        name       : "MDS DE BONNEVEINE",
        description: "📍35, Boulevard Baptistin Cayol | 13008 <br>☎️ 04 13 31 77 60 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
    "MDS DE LA VISTE | 15"             : {
        name       : "MDS DE LA VISTE",
        description: "📍43, Avenue de la Viste | 13015 <br>☎️ 04 13 31 64 03 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    }, 
    "MDS DE L'ESTAQUE | 16"             : {
        name       : "MDS DE L'ESTAQUE",
        description: "📍2 Allée Saccoman, Immeuble Le Carré | 13016 <br>☎️ 04 13 31 55 85 <br> 🕓 Ouvert au public, du <b>lundi au vendredi, De 8h30 à 12h et de 13h30 à 17h. Fermeture le mardi après-midi.</b>",
        url        : "https://www.departement13.fr/nos-actions/sante/les-lieux/les-maisons-departementales-de-la-solidarite/"
    },
    'Théâtres'                                   : {
        name       : '',
        description: '',
        url        : ''
    },
    "L'Auberge"                                          : {
        name       : "L'Auberge",
        description: "📍47 Imp. du Dr Bonfils | 13008<br><br>L’Auberge Marseillaise est un <b>lieu refuge dédié aux femmes en situation de grande vulnérabilité</b>, prenant place dans l’ancienne Auberge de Jeunesse Bonneveine, située dans les quartiers Sud de Marseille, à 300m des plages du Prado.<br><br>Le projet a été lancé en mars 2021, en pleine crise sanitaire et sociale, dans un élan inédit de coopération entre 9 associations impliquées localement dans la lutte contre la précarité, avec le soutien de la Ville et de l’Etat. C’est une expérimentation qui vise à construire un lieu de vie participatif en alternative aux hôtels sociaux et aux centres d’hébergement d’urgence dans un bâtiment public temporairement disponible.<br><br>À ce jour, elle accueille 70 personnes (femmes avec ou sans enfants) qui ont accès aux besoins primaires (sécurité, eau, hygiène, hébergement, alimentation). Avec l’équipe, elles initient des parcours d’accès aux droits et à la santé, à l’insertion et au logement, dans une approche globale d’amélioration des conditions de vie et de santé des personnes accueillies. L’accueil à l’Auberge marseillaise y est inconditionnel et sa durée s’adapte aux besoins et aux temporalités de rétablissement des personnes.",
        url        : 'https://yeswecamp.org/lauberge-marseillaise/'
    },
    "Tiers-Lab"                                          : {
        name       : "Tiers-Lab",
        description: "Tiers LAB est une société d’architecture, urbanisme et recherche spécialisée dans la mise en projet des situations d’entre-deux. <br> Notre champ d'action relève des espaces délaissés, du foncier inoccupé, des immeubles dégradés, des conflits d’usages...<h3> Valeurs </h3> <ul><li>Privilégier les tissus bâtis et sociaux déjà en place : valoriser l’existant et renforcer les identités </li><li>Récupérer les ressources socio-spatiales sous-exploitées dans la perspective d’un développement durable des territoires.</li><li>Privilégier la prise de décision horizontale en promouvant des démarches participatives</li><li>Rechercher des solutions économes en matière d’énergie, de matériaux et de techniques.</li><li>Privilégier des approches interdisciplinaires (architecture, urbanisme, histoire, sciences sociales)</li></ul><h3> Projets</h3> <b>Architecture</b> <ul><li> Aménagement d'un Tiers Lieu dans la mezzanine du restaurant universitaire du Campus les Gazelles | Aix-en-provence</li><li>Aménagement du hall du cinéma Cascade Rénoire en espace de partage et convivialité | Martigues </li><li>Restauration des façades d'un immeuble du XVIIe siécle en proximité du Vieux-Port | Marseille </li><li>Reconversion de l'ancienne bibliothèque Aristide Albert en Tiers lieu associatif | Briançon </li><li> Aménagement d'un appartement de 55 m2 situé dans le centre de | Bordeaux </li><li> Aménagement d'un petit appartement situé dans le centre-ancien de | Sanary-sur-Mer </li><li> Animation et réconversion d'un ancien cinéma desaffecté en centre social associatif | Rome </li></ul> <b> Urbanisme </b> <ul><li> Une Cité en transition : Repenser les espace et les usages de la Savine à Marseille, une Cité en forte transformation </li><li> Revitalisation d'un centre ancien : La Cité Vauban de Briançon entre tourisme, abandon, et renouvellement : étude sur son devenir </li><li> 2030 Utopies pavillonaires : L'évolution de trois lotissements pavillonaires dans la Métropole Aix-Marseille-Provence </li><li> Deux friches à Marseille : L'aménagement des friches issue de la démolition des immeubles J et G à la Savine </li><li> Les sommets du réemploi : Mobilités et programmations innovantes en montagne : projet pour Briançon </li><li> Dallo slargo alla piazza : De l'echangeur à la place, projet de transformation par l'usage d'une entrée de ville à Palerme </li><li> La métropole des villages --> Goussainville : la mise en réseaux des paysages périurbain comme levier pour les rehabiter </li><li> Orti Saraceni a Tricarico : Projet de restauration et aménagement des jardins des Sarrasins (XI siécle) à Tricarico (Italie) </li></ul> <b> Recherche </b> <ul><li>Laboratoire d'expériences collaboratives à Breil-sur-Roya : Le tiers foncier, ressource pour les centres anciennes Architecture et Paysage --» Caisse des Dépôts 2020 </li><li> Recherche action sur le centre ancien de Briançon | Le tiers foncier : une ressource pour les petites villes POPSU Territoires 2018/2019 </li><li> CAPACITY BUILDING : Reinventer la ville pavillonaire | Recherche expérimentation à Aix-en-Provence | URBAN-EUROPE 2020 </li><li> Le tiers foncier : une recherche action à Marseille | Thèse de doctorat en architecture 2014-2018 | Bourse : Ministere de la Culture </li><li> La République des autoroutes : le cas de la métropoles AMP | Thèse de doctorat en architecture 2015-2019 | Bourse : Région PACA </li></ul> <b> Ateliers </b><ul><li> Habiter la montagne, habiter la forêt : Atelier interdisciplinaire sur le bois et la forêt - Guillestre - Juillet 2023 </li><li>Vers une architecture de résistance : Atelier d'architecture en montagne, Breil-sur-Roya - Juillet 2021 </li><li>Centres anciens, architecture de demain : Atelier d'architecture en montagne, Briançon, Cité Vauban - 24 - 31 Aout 2019</li></ul>",
        url        : 'https://www.tiers-lab.com/'
    },
    "Tiers-lieux"                                          : {
        name       : "",
        description: "",
        url        : ''
    },
    "La Cuisine du 101"                                          : {
        name       : "La Cuisine du 101",
        description: "📍 12 Rue des Cigarières | 13003 <br><br> La Cuisine du 101 est un lieu co-construit par SoliHa Provence, Nexity Non-Profit et le Bouillon de Noailles, ouvert aux personnes hébergées à l’hôtel ou en foyer, et à la rue. Elles peuvent apporter leurs ingrédients, cuisiner, emporter ou se restaurer sur place dans un cadre quasi domestique. Un accompagnement culinaire et social y est proposé. Des animateurs et animatrices proposent des ateliers de cuisine, de création artistique, également accessibles aux associations et écoles du quartier.<br><br> Ce projet, financé par la Direction Interministérielle à l’Hébergement et à l’Accès au Logement (DIHAL), vise à participer à la lutte contre la précarité alimentaire. Il s’inscrit dans les principes de solidarité et de dignité, et vise à promouvoir l’émancipation, le renforcement du lien social et la santé par l’alimentation <h3> Horaires </h3> Accueil cuisine : les mardis, jeudis, vendredis et samedis de 11h à 18h <br><br> Ateliers : les mercredis <br><br>Lieu : St Mauront - Station National M2 <br><br> Sur réservation : 04 91 95 76 16 <br><br> Facebook, Instagram : @lacuisinedu101 ",
        url        : 'https://www.facebook.com/lacuisinedu101/'
    },
    "SPAAM-Répertoire"                                          : {
        name       : "RÉPERTOIRE DE SOIN COMMUNAUTAIRE MILITANT A MARSEILLE",
        description: "Au SPAAM, on s'organise autour des questions de soin psycho-émotionnel liées à la répression (policière, étatique, administrative) dans les milieux militants de gauche à Marseille. On est amené.es à orienter les personnes qui nous sollicitent et expriment le besoin d'un accompagnement par des professionel.les de la santé.Pour autant, on sait aujourd'hui qu'il existe des violences systémiques de la part de certain.es (la majorité) de ces professionnel.les.<br><br> Par ce répertoire, notre intention est de réunir des ressources de soin, des contacts de praticien.nes de la santé,  des outils  et des lieux de soin à Marseille en y associant des retours d'expériences. On y centralise et actualise les infos et retours subjectifs qui tournent dans nos milieux militants pour faciliter la prise de soins avec les personnes adaptées en fonction des situations. On y élabore au fur et à mesure une catégorisation dynamique en fonction des violences systémiques (transféminisme, psychotrauma VSS, anticolonialisme et antiracisme, classisme, validisme) qui nous permettent de situer ces ressources.<br><br>Dans cette catégorisation peuvent apparaître aussi les caractéristiques humaines liées à la profession (capacité d'écoute, humilité, empathie, froideur, etc.) et les outils employés (Open dialogue, décision médicale partagée, EMDR, etc.) <br><br>Ces catégories n'ont pas pour but d'enfermer ou de figer les personnes citées et seront actualisées en fonction des retours. Il est important pour nous de souligner le caractère subjectif de ces retours car ces derniers peuvent être contradictoires ou mitigés selon les expériences, recommandables pour certains critères et non-recommandables pour d'autres. Ces critères et retours peuvent aiguiller le choix, faire économiser de l'énergie, selon les caractéristiques recherchées, besoins et contraintes.<br><br>On travaille actuellement à un protocole d'entretien avec ces professionnel.les, inspiré de celui de la liste des Psys situé.es.",
        url        : 'https://annuel2.framapad.org/p/r.4171cc53778e172be7990842de1b4de2'
    },
    "Pédagogie"                                          : {
        name       : "",
        description: "",
        url        : ''
    },
    "Pédagogie Institutionnelle"                                          : {
        name       : "",
        description: "",
        url        : ''
    },
    "Freinet"                                          : {
        name       : "",
        description: "",
        url        : ''
    },
    "RERS"                                          : {
        name       : "Réseaux d'Échanges Réciproques de Savoirs",
        description: "Les RERS sont des réseaux de personnes qui se rencontrent pour échanger des savoirs, des savoir-faire et des connaissances. Cette démarche est fondée sur deux principes essentiels : la réciprocité et la gratuité. Chaque participant offre et reçoit des savoirs, créant ainsi un échange réciproque et enrichissant mutuellement.<h3> Histoire </h3> Les RERS ont été créés dans les années 1970 par Claire et Marc Héber-Suffrin, deux éducateurs qui ont constaté que les élèves, même ceux en difficulté, détenaient des savoirs méconnus ou ignorés par l’école et parfois par eux-mêmes. Leur pédagogie s’appuyait sur la possibilité pour les élèves de transmettre ces savoirs à leurs camarades. Les RERS se sont développés dans la ville d’Orly et ont été structurés en association nationale, le Mouvement des Réseaux d’Échanges Réciproques de Savoirs (MRERS), en 1987. En 2009, l’association a été remplacée par FORESCO (Formations Réciproques Échanges de Savoirs et Créations Collectives), agréée en 2010 “Association nationale de jeunesse et d’Éducation populaire” par le Ministère de la Jeunesse et des solidarités actives.",
        url        : 'https://www.rers-asso.org/index.htm'
    },
    "Orga. Politiques"                                          : {
        name       : "",
        description: "",
        url        : ''
    },
    "Fédérations"                                          : {
        name       : "",
        description: "",
        url        : ''
    },
    "Coalitions"                                          : {
        name       : "",
        description: "",
        url        : ''
    },
    "Fédération Anarchiste"                                          : {
        name       : "Fédération Anarchiste",
        description: "L’FA est affiliée à l’Internationale des fédérations anarchistes et représente l’organisation visible principale du mouvement anarchiste en France, tant par le nombre de ses militants que par les organes dont elle dispose (librairie, radio, éditions, hebdomadaire).<br><br>Groupe Germinal (Marseille)<br>Contact : loran(at)w-n-e.net",
        url        : 'https://www.federation-anarchiste.org/'
    },
    "NFP-13"                                          : {
        name       : "Nouveau Front Populaire | Marseille",
        description: "Réserve citoyenne du Front Pop<br><br>Les citoyen·nes et la société civile marseillaise font front populaire.",
        url        : 'https://linktr.ee/FPMarseille'
    },
    "EELV-PACA"                                          : {
        name       : "Europe Écologie Les Verts",
        description: "Permanences : les lundi, mardi, jeudi et vendredi de 9h a 12h<br><br>📍 111 rue du Rouet, 13008 Marseille<br><br>Métro Périer<br><br>Tél. :  04 91 79 50 53<br><br> Courriel : contact-paca@eelv.fr<br><br> Nous sommes des écologistes anonymes du quotidien, des activistes engagé.e.s, des syndicalistes, des militant.e.s politiques et des élu.e.s. Toutes et tous, à notre manière, nous souhaitons nous rassembler et œuvrer pour la préservation du commun, du vivant et de tout ce qui nous lie.  Notre constat est sans appel. <br><br>Aujourd'hui, plus personne ne peut garantir que la Terre sera habitable pour les générations futures. Or, tout le monde sait que cette catastrophe écologique est enclenchée depuis des décennies et qu’elle est aussi une catastrophe sociale et démocratique qui touche toutes les strates de nos sociétés, notamment les plus précaires.<br><br>Pourtant, aucune action politique d’envergure n’a jamais été menée pour tenter d’enrayer ce processus dévastateur. Cette lâcheté politique est inacceptable. Mais nous ne nous résignons pas.<br><br>Car il existe, comme le décrivait Bruno Latour, une « classe écologique », transverse et potentiellement majoritaire, qui ne demande qu’à se rassembler. Consciemment ou non, des millions de personnes sont engagées au quotidien dans la recherche d’un mode de vie durable et respectueux des équilibres environnementaux et sociétaux. Cette énergie est nécessaire, mais insuffisante. Il faut maintenant fédérer les initiatives et les engagements pour peser face aux défenseurs inconséquents du statu quo.",
        url        : 'https://paca.eelv.fr/'
    },
    "Les ateliers blancarde"                                          : {
        name       : "Les ateliers blancarde",
        description: "📍 1 place de la gare de la Blancarde | 13004 <br> 🕓 du mardi au vendredi · de 9h à 13h et de 14h à 18h<br> samedi · de 9h à 12h30 <br><br> Les Ateliers Blancarde est un tiers-lieu artistique situé au premier étage de la gare SNCF de la Blancarde. Nous y installons pour plusieurs années des projets qui nous tiennent à cœur : une Artothèque pour que chacun puisse emprunter une œuvre d’art selon ses envies, une Bricothèque qui rassemble tous les outils dont vous pourriez avoir besoin pour vos travaux de bricolage ou de création, une Bibliothèque pour consulter ou emprunter des livres sur l’art, un Jardin collectif et participatif et des événements tout au long de l’année pour adopter de nouveaux gestes éco-responsables. Un programme de résidences de création pour artistes visuels se déroule pendant l’année.<br><br> Laboratoire d’innovation sociale, Les Ateliers Blancarde est un pont entre les problématiques de l’art contemporain et celles de notre société en mutation. Il s’agit de construire et d’enrichir le lien social et culturel entre les personnes à travers la circulation de biens et de connaissances. Cette initiative repose sur la nécessité actuelle d’inventer un mode de consommation raisonné, respectueux de l’environnement, à échelle humaine et locale autant que d’établir et affirmer le rôle de l’artiste au cœur de la ville.<br><br> En adhérant aux Ateliers Blancarde vous devenez membre de l’association Dos Mares et bénéficiez d’un accès illimité à l’ensemble des projets. L’adhésion est valable une année<br><br>",
        url        : 'https://www.lesateliersblancarde.com/'
    },
    "La Réserve des Arts"                                          : {
        name       : "La Réserve des Arts | pour une création circulaire et solidaire",
        description: "📍 85 boulevard Danielle Casanova | 13014 <br>🚇 Métro Bougainville <br> 🕓 mercredi au samedi de 10h à 12h30 puis de 13h30 à 17h30 <br><br> LA RÉSERVE DES ARTS est une association à but non lucratif qui favorise et​ soutient le développement d’une économie circulaire et solidaire au sein du secteur culturel, créatif et artisanal.<br><br> Nous accompagnons les professionnel·le·s du secteur de la culture dans l'appropriation des pratiques de l'économie circulaire et le réemploi de matériaux. <h3> NOTRE MISSION​ </h3> <ul><li>Offrir une réponse opérationnelle et logistique dans la transmission des matériaux</li><li>Développer, professionnaliser et transmettre un savoir­-faire spécifique de la valorisation pour réemploi</li><li>Mesurer la performance de la circularité</li><li>Diffuser la culture du réemploi via la formation, la sensibilisation et l'expérimentation</li></ul> <h3> NOS SERVICES </h3> <ul><li>VALORISATION POUR RÉEMPLOI - collecte ou réception de matériaux du secteur culturel </li><li> VENTE DE MATÉRIAUX DE RÉEMPLOI - à prix solidaires</li><li> SENSIBILISATION aux métiers et enjeux du réemploi dans la culture et de l'économie circulaire</li><li>FORMATION ET PROFESSIONNALISATION - organisme de formation certifié Qualiopi</li><li>LOCATION D'ATELIERS - location de parcs outils et résidences artistiques</li></ul> NOS POINTS DE VENTE <ul><li>L’ENTREPÔT - Montreuil </li><li>LA BOUTIQUE - Paris 14ème</li><li>LA CASANOVA - Marseille 14ème</li></ul>",
        url        : 'https://www.lareservedesarts.org/'
    },

    


};

export default class Entities {
    static get() {
        return entities;
    }

    static get_by_name(entity_name) {
        return entities[entity_name] || false;
    }
}