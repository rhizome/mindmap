export default function get() {
    return {
        'og_image': "",
        'app_id': null,
        'autoLogAppEvents': true,
        'xfbml': true,
        "version": 'v3.1'
    };
}