export default class View {
    static get() {
        return {
            title                   : "Rhizome",
            default_meta_description: "Aide à la création de rhizome entre collectifs, associations et institutions sociales et solidaires",
            menus                   : {}
        };
    }
}