/**
 * Deployment tasks :
 * - Processing with the production mode of webpack.
 * - SourceMap building.
 * - Format output css filename in prod mode : hash in the name of output file.
 * - Idem for js filename
 * - Webpack manifest file building.
 */

const {merge} = require("webpack-merge");
const common = require("./webpack.common.js");
const production_tasks_custom = require("./workspace/webpack/production_tasks");
const utils = require("./webpack.utils");

const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const {WebpackManifestPlugin} = require("webpack-manifest-plugin");
const TerserPlugin = require("terser-webpack-plugin");

module.exports = merge(common, production_tasks_custom, {
    optimization: {
        usedExports: false,
        minimizer  : [
            new TerserPlugin({
                terserOptions: {
                    mangle         : false,
                    keep_classnames: true,
                },
            }),
        ],
    },
    output      : {
        filename: (pathData) => {
            let filename = '[name].js';

            /**
             * If a CSS file : will not run js filename calculation
             */
            if (typeof pathData.chunk.contentHash["css/mini-extract"] === "undefined") {
                filename = utils.getJsFile(pathData.chunk, "production");
            }

            return filename;
        },
    },
    mode        : "production",
    devtool     : "source-map",
    plugins     : [
        new MiniCssExtractPlugin({
            filename: ({chunk}) => {
                return utils.getCssFile(chunk, "production");
            },
        }),
        new WebpackManifestPlugin({
            generate: (seed, files, entries) => {
                let content = {};

                for (const [key, value] of Object.entries(entries)) {
                    let filename = value[0].split("/");

                    filename = filename[filename.length - 1];
                    content[key] = filename;
                }

                return content;
            },
        }),
    ],
});
