#!/usr/bin/env node
const mapping_nodename_with_their_ways = {};
const final_model = {};
const hierarchy_tree_data = {
    "name"    : "Rhizome Marseille",
    "free"    : true,
    "children": [
        {
            "name"    : "Lieux",
            "free"    : true,
            "children": [

                {
                    "name"    : "Cinémas",
                    "free"    : true,
                    "children": [

                        {
                            "name": "Le Polygone Étoilé",
                            "free": true,
                        },
                        {
                            "name": "Vidéodrome 2",
                            "free": true,
                        },
                        {
                            "name": "Le Gyptis",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Théâtres",
                    "free"    : true,
                    "children": [
                        {
                            "name": "L'Astronef",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Bars | Cafés | Salles",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Ciné Bar (ClubCoop)",
                            "free": true,
                        },
                        {
                            "name"    : "La Merveilleuse",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Jam Poèsie·Musique",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name": "El Aché de Cuba",
                            "free": true,
                        },
                        {
                            "name": "L'Embobineuse",
                            "free": true,
                        },
                        {
                            "name": "DATA",
                            "free": true,
                        },
                        {
                            "name": "LESTOCKK",
                            "free": true,
                        },
                        {
                            "name": "Rallumeurd'étoiles",
                            "free": true,
                        },

                    ]
                },
                {
                    "name"    : "Entreprises Autogérés",
                    "free"    : true,
                    "children": [
                        {
                            "name": "L'Après M",
                            "free": true,
                        },
                        {
                            "name": "L'Épicerie Paysanne",
                            "free": true,
                        },
                        {
                            "name": "Association Cuve",
                            "free": true,
                        },
                        {
                            "name": "Les Champignons de Marseille (?)",
                            "free": true,
                        },
                        {
                            "name": "1336 - Coop. production de thé ",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Entrerpises | Cooperatives",
                    "free"    : true,
                    "children": [
                        {
                            "name": "La Ruche",
                            "free": true,
                        },
                        {
                            "name": "Le Plan de A à Z",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Librairies | Bibliothèques",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Bibliothèque Alcazar",
                            "free": true,
                        },
                        {
                            "name": "Cité de l'Agriculture",
                            "free": true,
                        },
                        {
                            "name": "Wildproject",
                            "free": true,
                        },
                        {
                            "name": "Mille Bâbords",
                            "free": true,
                        },
                        {
                            "name": "La DAR",
                            "free": true,
                        },
                        {
                            "name": "Transit",
                            "free": true,
                        },
                        {
                            "name": "L'Hydre aux milles têtes",
                            "free": true,
                        },
                        {
                            "name": "Manifesten",
                            "free": true,
                        },
                        {
                            "name": "Mémoire des Sexualités",
                            "free": true,
                        },
                        {
                            "name": "L'Histoire de l'oeil",
                            "free": true,
                        },
                        {
                            "name": "Les Héroïnes",
                            "free": true,
                        },
                        {
                            "name": "CIRA",
                            "free": true,
                        },
                        {
                            "name": "DATA",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Lieux Urbains Autogérés",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Manifesten",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Manifestin",
                                    "free": true,
                                },
                                {
                                    "name": "MARSS",
                                    "free": true,
                                },
                                {
                                    "name": "Cercle de Nageurs en Eaux Troubles",
                                    "free": true,
                                },
                                {
                                    "name": "SPAAM",
                                    "free": true,
                                },
                                {
                                    "name": "Saccage (Anti-JO's)",
                                    "free": true,
                                },
                                {
                                    "name": "Groupe Anti-gentrification",
                                    "free": true,
                                },
                                {
                                    "name": "Technopolice",
                                    "free": true,
                                },
                                {
                                    "name": "CQFD",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "La DAR",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Le Café des Chômheureuses",
                                    "free": true,
                                },
                                {
                                    "name": "Atelier du Mouvement (danse)",
                                    "free": true,
                                },
                                {
                                    "name": "Atelier Théâtre",
                                    "free": true,
                                },
                                {
                                    "name": "accueil de jour Sirakadjan",
                                    "free": true,
                                },
                                {
                                    "name": "maladroite boxe populaire",
                                    "free": true,
                                },
                                {
                                    "name": "Atelier de Couture",
                                    "free": true,
                                },
                                {
                                    "name": "Bibliothèque Nulle part Ailleurs",
                                    "free": true,
                                },
                                {
                                    "name": "Cantine The Noble Kitchen",
                                    "free": true,
                                },
                                {
                                    "name": "Cantine Chaubouillante",
                                    "free": true,
                                },
                                {
                                    "name": "les gadjis",
                                    "free": true,
                                },
                                {
                                    "name": "antifa social club",
                                    "free": true,
                                },
                                {
                                    "name": "sirakadjan",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "La Base",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Alternatiba",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name"    : "ANV COP-21m",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Testing children node",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                    ]
                                },
                                {
                                    "name": "Transat",
                                    "free": true,
                                },
                                {
                                    "name": "XR - Extinction Rebellion",
                                    "free": true,
                                },
                                {
                                    "name": "Youth for Climate",
                                    "free": true,
                                },
                                {
                                    "name": "Résistance Aggression Publicitaire",
                                    "free": true,
                                },
                                {
                                    "name": "Aïolibre",
                                    "free": true,
                                },
                                {
                                    "name": "La Roue (monnaie)",
                                    "free": true,
                                },
                                {
                                    "name": "Télé Mouche",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Mille Bâbords",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "AG InterPro",
                                    "free": true,
                                },
                                {
                                    "name": "CNT13",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Solidaires13",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "SESL - solidaires étudiants",
                                    "free": true,
                                },
                                {
                                    "name": "BTP autonome",
                                    "free": true,
                                },
                                {
                                    "name": "AG précaires",
                                    "free": true,
                                },
                                {
                                    "name": "sud éduc",
                                    "free": true,
                                },
                                {
                                    "name": "Asso",
                                    "free": true,
                                },
                                {
                                    "name": "Le Social Brûle 13",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name": "Massalia VOx",
                            "free": true,
                        },
                        {
                            "name"    : "Al Manba",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Permanence Juridique",
                                    "free": true,
                                },
                                {
                                    "name": "Cours FLE",
                                    "free": true,
                                }
                            ]
                        },
                        {
                            "name": "La Nebula",
                            "free": true,
                        },
                        {
                            "name"    : "Bourse du Travail",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "CGT chômeur précaires",
                                    "free": true,
                                },
                                {
                                    "name": "CGT spectacle",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Centre LGBTQIA+",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "G.L.A.M",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name": "Les 8 Pillards",
                            "free": true,
                        },
                        {
                            "name"    : "Imprimerie Partagée",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Gratuit pour les urgences administratives",
                                    "free": true,
                                },
                                {
                                    "name": "Prix libre pour affiches/flyers/micro-éditions",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "CIRA",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Causeries Mensueles",
                                    "free": true,
                                },
                                {
                                    "name": "Atelier Recherche Formation",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Vidéodrome 2",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Les Mains Gauches-festival queer féministe",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Le Morozoff",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Box",
                                    "free": true,
                                },
                                {
                                    "name": "Clownozoff",
                                    "free": true,
                                },
                                {
                                    "name": "Tango Queer",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Mémoire des Sexualités",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Genre de Lutte",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name": "La Déviation",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Lieux Ruraux Autogérés",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Café Villageois (Lauris)",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Lieux Publics",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Cité d'Arts de la Rue",
                            "free": true,
                        },
                        {
                            "name": "Bibliothèque Alcazar",
                            "free": true,
                        },
                        {
                            "name": "Cité des Associations",
                            "free": true,
                        },
                    ]

                },
                {
                    "name"    : "Tiers-lieux",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Tiers-Lab",
                            "free": true,
                        },
                        {
                            "name": "Les ateliers blancarde",
                            "free": true,
                        },
                        {
                            "name": "La Réserve des Arts",
                            "free": true,
                        },
                        {
                            "name": "Le Talus",
                            "free": true,
                        },
                        {
                            "name": "Le Plan de A à Z",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Laboratoires de vie",
                    "free"    : true,
                    "children": [
                        {
                            "name": "L'école des vivants",
                            "free": true,
                        },
                        {
                            "name"    : "Longo Maï",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Grange neuve (Forcarlquier)",
                                    "free": true,
                                },
                                {
                                    "name": "Mas de Granier (Saint Martin de Crau)",
                                    "free": true,
                                },
                                {
                                    "name": "La Cabrery (Luberon)",
                                    "free": true,
                                },
                                {
                                    "name": "Filature de Chantemerle (Hautes-Alpes)",
                                    "free": true,
                                },
                                {
                                    "name": "Treynas (Ardèche)",
                                    "free": true,
                                },
                            ]
                        },
                    ]
                },
                {
                    "name"    : "Fermes",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Longo Maï",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Grange neuve (Forcarlquier)",
                                    "free": true,
                                },
                                {
                                    "name": "Mas de Granier (Saint Martin de Crau)",
                                    "free": true,
                                },
                                {
                                    "name": "La Cabrery (Luberon)",
                                    "free": true,
                                },
                                {
                                    "name": "Filature de Chantemerle (Hautes-Alpes)",
                                    "free": true,
                                },
                                {
                                    "name": "Treynas (Ardèche)",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name": "La Caillasse (Cucuron)",
                            "free": true,
                        },
                        {
                            "name": "La ferme Capri",
                            "free": true,
                        },
                        {
                            "name": "Le Collet des Comtes",
                            "free": true,
                        },
                        {
                            "name": "Ferme du Roy d'Espagne",
                            "free": true,
                        },
                        {
                            "name": "Ferme Pastière (Meryrargues)",
                            "free": true,
                        },
                        {
                            "name": "Ferme de l'Etoile",
                            "free": true,
                        },
                        {
                            "name": "Le Talus",
                            "free": true,
                        },
                        {
                            "name": "La Bastide à Fruits",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Squats",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Le Snack",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "GSN Géstion de Conflits",
                                    "free": true,
                                }
                            ]
                        },
                        {
                            "name": "La Tarantula",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Lieux de Répit",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Lieu de Répit",
                            "free": true,
                        },
                        {
                            "name": "Palama",
                            "free": "true",
                        },
                    ]
                },
                {
                    "name"    : "Refuge | Hébergement",
                    "free"    : true,
                    "children": [
                        {
                            "name": "L'Auberge",
                            "free": "true",
                        },
                    ]
                },
                {
                    "name"    : "Services Sociaux",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Assistance Sociale",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "CCAS",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Agence Centre|13002",
                                            "free": true,
                                        },
                                        {
                                            "name": "Agence Est|13004",
                                            "free": true,
                                        },
                                        {
                                            "name": "Agence Sud|13008",
                                            "free": true,
                                        },
                                        {
                                            "name": "Agence Nord|13014",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Maisons de Solidarité",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "MDS COLBERT | 01",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS DU LITTORAL | 02",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS BELLE DE MAI | 03",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS CHARTREUX | 04",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS ST SEBASTIEN | 06",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS DE BONNEVEINE | 08",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS PONT DE VIVAUX | 10",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS SAINT MARCEL | 11",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS LE NAUTILE | 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS MALPASSÉ | 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS DE LA VISTE | 15",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS DE L'ESTAQUE | 16",
                                            "free": true,
                                        },


                                    ]
                                },
                                {
                                    "name"    : "Logement",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "EAH",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                    ]
                },
                {
                    "name"    : "Santé",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Le Spot-Longchamp",
                            "free": "true",
                        },
                        {
                            "name": "LE CHÂTEAU EN SANTÉ",
                            "free": true,
                        },
                    ]
                }
            ]
        },
        {
            "name"    : "Collectifs | Assos | Projets",
            "free"    : true,
            "children": [
                {
                    "name"    : "Macropolitique",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Bataille Médiatique",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Journaux Indépendants",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "CQFD",
                                            "free": true,
                                        },
                                        {
                                            "name": "Marsactu",
                                            "free": true,
                                        },
                                        {
                                            "name": "La terre en Thiers",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name": "Chaines Youtube",
                                    "free": true,
                                },
                                {
                                    "name": "Blogs",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Blocage",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Zads",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Zone à Patates",
                                            "free": true,
                                        }
                                    ]
                                },
                                {
                                    "name"    : "S.d.l.Terre",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Comité Local 13",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                        {
                            "name"    : "Face aux lois/politiques Gouv.",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Autonomes",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "AG-InterPro",
                                            "free": true,
                                        },
                                        {
                                            "name": "Marseille vs Darmanin",
                                            "free": true,
                                        },
                                        {
                                            "name": "CoMob-StCharles",
                                            "free": true,
                                        },
                                        {
                                            "name": "Le Social Brûle 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "Urgence Palestine",
                                            "free": true,
                                        }
                                    ]
                                },
                                {
                                    "name"    : "Syndicats",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name"    : "Solidaires13",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "SESL - solidaires étudiants",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "BTP autonome",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "AG précaires",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "sud éduc",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Asso",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Le Social Brûle 13",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name": "CNT13",
                                            "free": true,
                                        },
                                        {
                                            "name": "UNEF-13",
                                            "free": true,
                                        },
                                        {
                                            "name": "Confédération Paysanne PACA",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Partis",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "NUPES13",
                                            "free": true,
                                        },
                                        {
                                            "name": "NPA13",
                                            "free": true,
                                        },
                                        {
                                            "name": "LFI13",
                                            "free": true,
                                        },
                                        {
                                            "name": "Rev.Permanente13",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                        {
                            "name"    : "Face au système Financier",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Banques Alternatives",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "La NEF",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Monnaies Locales",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "La Roue (monnaie)",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Cryptomonnaies",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "La Ĝ (June)",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                        {
                            "name"    : "Face au Néo-colonialisme",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Survie - PACA",
                                    "free": true,
                                },
                                {
                                    "name": "Urgence Palestine",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Face au Fascisme",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Urgence Palestine",
                                    "free": true,
                                },
                                {
                                    "name": "Riposte Antifa",
                                    "free": true,
                                },
                                {
                                    "name": "Brigade Antifa",
                                    "free": true,
                                },
                                {
                                    "name": "Colletif Antifa",
                                    "free": true,
                                },
                                {
                                    "name": "Antifa Social Club",
                                    "free": true,
                                },
                                {
                                    "name": "Assemblées Antifa",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Face à la Psychiatrie",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Fondation ERIE",
                                    "free": true,
                                },
                                {
                                    "name": "AiLSi",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : " Face à la Gentrification",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Collectif Anti-Gentrification",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Face à la Surveillance",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Technopolice",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "F. au Système Alimentaire",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Riposte Alimentaire",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Internationnalisme",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "CIMK",
                                    "free": true,
                                },
                            ]
                        },
                    ]
                },
                {
                    "name"    : "Micropolitique",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Santé | Soin | Thérapie",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Thérapie",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Pour une Thérapie Transversale",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name": "Remèdes,Potions,Médocs",
                                    "free": true
                                },
                                {
                                    "name"    : "Sexualité",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name"    : "Contraception",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "13ticules",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Psycho-Social",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "MARSS",
                                            "free": true,
                                        },
                                        {
                                            "name": "COFOR",
                                            "free": true,
                                        },
                                        {
                                            "name": "JUST",
                                            "free": true,
                                        },
                                        {
                                            "name": "Nouvelle Aube",
                                            "free": true,
                                        },
                                        {
                                            "name"    : "GEMs",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Lieu d'Échanges et d'Ouvertures LEO",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "CLUB PARENTHESE",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name": "Planning Familial 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "IMAJSanté",
                                            "free": true,
                                        },
                                        {
                                            "name": "Autres Regards",
                                            "free": true,
                                        },
                                        {
                                            "name": "Solidarité Femmes 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "Centre LGBTQIA+",
                                            "free": true,
                                        },
                                        {
                                            "name": "Transat",
                                            "free": true,
                                        },
                                        {
                                            "name": "LE CHÂTEAU EN SANTÉ",
                                            "free": true,
                                        },
                                        {
                                            "name": "Equipe SIDIIS",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Addictions",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Nouvelle Aube",
                                            "free": true,
                                        },
                                        {
                                            "name": "Bus 31/32",
                                            "free": true,
                                        }
                                    ]
                                },
                            ]
                        },
                        {
                            "name"    : "Médiation | Gestion de Conflits",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Collectif WD-40",
                                    "free": true,
                                },
                                {
                                    "name": "Réseau Gestion_Mars_Conflits",
                                    "free": true,
                                },
                                {
                                    "name": "AVAD",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Éducation | Formation",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Arts",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name"    : "Thêatre",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Thêatre Autogéré DAR",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Thêatre de l'Opprimé·e LABASE",
                                                    "free": true,
                                                }
                                            ]
                                        },
                                        {
                                            "name"    : "Musique",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "LES RASCASSES",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "La lutte enchantée",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Atelier Palmas(€)",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name"    : "Cirque",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Extrême Jonglerie",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name"    : "Cinéma",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Porn on Mars festival de porn queer",
                                                    "free": true,
                                                }
                                            ]
                                        },
                                        {
                                            "name": "Peinture",
                                            "free": true,
                                        },
                                        {
                                            "name"    : "Cuisine",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Le Bouillon de Noailles",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Artisanat",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name"    : "Couture",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Atelier Couture (DAR)",
                                                    "free": true,
                                                },
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "name"    : "Politique",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "A2C-AutonomieDeClasse",
                                            "free": true,
                                        },
                                        {
                                            "name": "UEEH",
                                            "free": true,
                                        }
                                    ]
                                },
                                {
                                    "name"    : "Genre & Sexualités",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name"    : "Masculinités",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Manoeuvre",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name": "UEEH",
                                            "free": true,
                                        },
                                        {
                                            "name": "Centre LGBTQIA+",
                                            "free": true,
                                        }
                                    ]
                                },
                                {
                                    "name"    : "Psycho-Social",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name"    : "Ateliers Nebula",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Gestion de Crises",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name"    : "SPAAM",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Écoute Active",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Hypnose",
                                                    "free": true,
                                                },

                                            ]
                                        },
                                        {
                                            "name"    : "Modules COFOR",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Bien Être",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Vivre Avec",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Rétablissement",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Droit",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Addictions",
                                                    "free": true,
                                                }
                                            ]
                                        },
                                        {
                                            "name": "CRIR-AVS PACA",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Navigation",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Lounapo",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Autres",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Crefada",
                                            "free": true,
                                        },
                                    ]
                                },

                            ]
                        },
                        {
                            "name"    : "Solidarité | Entraide",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Exilées",
                                    "free"    : true,
                                    "children": [

                                        {
                                            "name": "Ramina",
                                            "free": true,
                                        },
                                        {
                                            "name": "Le GR1",
                                            "free": true,
                                        },
                                        {
                                            "name"    : "Al Manba",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Permanence Juridique",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Cours FLE",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name": "Solidaires MNA",
                                            "free": true,
                                        },
                                        {
                                            "name": "QX1 - WelcomeMap",
                                            "free": true,
                                        },
                                        {
                                            "name": "G.L.A.M",
                                            "free": true,
                                        },
                                        {
                                            "name": "AUP - Demandeurs d'asile",
                                            "free": true,
                                        },
                                        {
                                            "name": "Collectif Binkadi",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name": "Administratif",
                                    "free": true
                                },
                                {
                                    "name"    : "Langues",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Mot de Passe (Français)",
                                            "free": true,
                                        }
                                    ]
                                },
                                {
                                    "name"    : "Psycho-Social",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "MARSS",
                                            "free": true,
                                        },
                                        {
                                            "name": "COFOR",
                                            "free": true,
                                        },
                                        {
                                            "name"    : "GEMs",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Lieu d'Échanges et d'Ouvertures LEO",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "CLUB PARENTHESE",
                                                    "free": true,
                                                }
                                            ]
                                        },
                                        {
                                            "name": "IMAJSanté",
                                            "free": true,
                                        },
                                        {
                                            "name": "H.A.S",
                                            "free": true,
                                        },
                                        {
                                            "name": "Solidarité Femmes 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "Centre LGBTQIA+",
                                            "free": true,
                                        },
                                        {
                                            "name": "Planning Familial 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "Transat",
                                            "free": true,
                                        },
                                        {
                                            "name": "Autres Regards",
                                            "free": true,
                                        },
                                        {
                                            "name": "LE CHÂTEAU EN SANTÉ",
                                            "free": true,
                                        },
                                        {
                                            "name": "AVAD",
                                            "free": true,
                                        },
                                        {
                                            "name": "L'Auberge",
                                            "free": "true",
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Chomage | Precarité",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Chomheureuses",
                                            "free": true,
                                        },
                                        {
                                            "name": "Chomeurs Précaires 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "Cultures Du Cœur 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "La Cuisine du 101",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                        {
                            "name"    : "Luttes Locales",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Cité de l'Agriculture",
                                    "free": true,
                                },
                                {
                                    "name": "CHO3",
                                    "free": true,
                                },
                                {
                                    "name": "MARSS",
                                    "free": true,
                                },
                                {
                                    "name": "JUST",
                                    "free": true,
                                },
                                {
                                    "name": "Nouvelle Aube",
                                    "free": true,
                                },
                                {
                                    "name": "La Roue (monnaie)",
                                    "free": true,
                                },
                                {
                                    "name": "Approches Cultures & Territoires",
                                    "free": true,
                                },
                                {
                                    "name": "AUP - Demandeurs d'asile",
                                    "free": true,
                                },
                                {
                                    "name": "La Cloche Sud",
                                    "free": true,
                                },
                                {
                                    "name": "Collectif Binkadi",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Autodéfense",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Legal Team 13",
                                    "free": true,
                                },
                                {
                                    "name": "SPAAM",
                                    "free": true,
                                },
                                {
                                    "name": "Mars'Soins",
                                    "free": true,
                                },
                                {
                                    "name"    : "Arts Martiaux/Box",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Maladroite BoxPop",
                                            "free": true,
                                        },
                                        {
                                            "name": "Box Morozoff",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                        {
                            "name"    : "Mutualisation",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Matériel/Outils",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Marsmut",
                                            "free": true,
                                        },
                                        {
                                            "name": "La Cuisine du 101",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name": "Logement",
                                    "free": true,
                                },
                                {
                                    "name"    : "Revenus",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Mutuelle MTPGB",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name": "Ateliers",
                                    "free": true,
                                },
                                {
                                    "name"    : "Numérique",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Marsnet",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                        {
                            "name"    : "Bouffe",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Production en ville",
                                    "free"    : true,
                                    "children": [

                                        {
                                            "name": "Les Champi. de Marseille",
                                            "free": true,
                                        },
                                        {
                                            "name": "Association Cuve",
                                            "free": true,
                                        },
                                        {
                                            "name": "Le Talus",
                                            "free": true,
                                        },
                                        {
                                            "name": "La ferme Capri",
                                            "free": true,
                                        }
                                    ]
                                },
                                {
                                    "name"    : "Cantines Solidaires",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "CHO3",
                                            "free": true,
                                        },
                                        {
                                            "name": "TheNobelKitchen",
                                            "free": true,
                                        },
                                        {
                                            "name": "Cantine du Midi",
                                            "free": true,
                                        },
                                        {
                                            "name": "Le Bouillon de Noailles",
                                        },
                                        {
                                            "name": "La Marmite Joyeuse",
                                            "free": true,
                                        },
                                        {
                                            "name": "La Cuisine du 101",
                                            "free": true,
                                        },
                                        {
                                            "name": "Manifestin",
                                            "free": true,
                                        },
                                        {
                                            "name": "Casa Consolat",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Réseaux PDC",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Les Paniers Marseillais",
                                            "free": true,
                                        },
                                        {
                                            "name": "Casa Consolat",
                                            "free": true,
                                        },
                                        {
                                            "name": "Le Marché Rouge",
                                            "free": true,
                                        }
                                    ]
                                },
                                {
                                    "name": "Gestion Déchets",
                                    "free": true,
                                },
                                {
                                    "name"    : "Entreprises-Autog.",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "L'Après M",
                                            "free": true,
                                        },
                                        {
                                            "name": "L'Épicerie Paysanne",
                                            "free": true,
                                        },
                                        {
                                            "name": "Association Cuve",
                                            "free": true,
                                        },
                                        {
                                            "name": "Les Champignons de Marseille",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Circuit Court",
                                    "free"    : true,
                                    "children": [

                                        {
                                            "name"    : "Épiceries",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Bar à Vrac",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "L'Épicerie Paysanne",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "La plaine fraicheur",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Adèle",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name"    : "Restos | Cantines",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Cantine du Midi",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Les Ondines",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Café l'Ecomotive",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Casa Consolat",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Le Grain de Sable",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "La Marmite Joyeuse",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name"    : "Boulangeries",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Le Bar à Pain",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Les Mains Libres",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Boulangerie-Café Pain Salvator",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "House of Pain",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name"    : "Marchés",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Le Marché Rouge",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Marché des artisans et des producteurs bio",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name"    : "Supermarchés",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Super Cafoutch",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                        {
                                            "name"    : "Friperies",
                                            "free"    : true,
                                            "children": [
                                                {
                                                    "name": "Frip'Insertion - Libération",
                                                    "free": true,
                                                },
                                                {
                                                    "name": "Frip'Insertion - Capelette ",
                                                    "free": true,
                                                },
                                            ]
                                        },
                                    ]
                                },
                            ]
                        },
                        {
                            "name"    : "Logement",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Regain",
                                    "free": true,
                                },
                            ]
                        },
                    ]
                },
                {
                    "name"    : "Infra-personnel",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Dispositifs Travail Singularité",
                            "free": true,
                        },
                        {
                            "name": "Thérapie Transversale",
                            "free": true,
                        }
                    ]
                }
            ]
        },
        {
            "name"    : "Orga. Politiques",
            "free"    : true,
            "children": [
                {
                    "name"    : "Partis",
                    "free"    : true,
                    "children": [
                        {
                            "name": "LFI13",
                            "free": true,
                        },
                        {
                            "name": "EELV-PACA",
                            "free": true,
                        },
                        {
                            "name": "NPA13",
                            "free": true,
                        },

                        {
                            "name": "Rev.Permanente13",
                            "free": true,
                        },


                    ]
                },
                {
                    "name"    : "Syndicats",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Solidaires13",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "SESL - solidaires étudiants",
                                    "free": true,
                                },
                                {
                                    "name": "BTP autonome",
                                    "free": true,
                                },
                                {
                                    "name": "AG précaires",
                                    "free": true,
                                },
                                {
                                    "name": "sud éduc",
                                    "free": true,
                                },
                                {
                                    "name": "Asso",
                                    "free": true,
                                },
                                {
                                    "name": "Le Social Brûle 13",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name": "CNT13",
                            "free": true,
                        },
                        {
                            "name": "UNEF-13",
                            "free": true,
                        },
                        {
                            "name": "Confédération Paysanne PACA",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Fédérations",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Fédération Anarchiste",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Coalitions",
                    "free"    : true,
                    "children": [
                        {
                            "name": "NUPES13",
                            "free": true,
                        },
                        {
                            "name": "NFP-13",
                            "free": true,
                        },
                    ]
                },
            ]
        },
        {
            "name"    : "Ressources",
            "free"    : true,
            "children": [
                {
                    "name"    : "Agendas",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Le Vortex",
                            "free": true,
                        },
                        {
                            "name": "Journal Ventilo",
                            "free": true,
                        },
                        {
                            "name": "Mars Infos Autonomes",
                            "free": true,
                        },
                        {
                            "name": "Mille Bâbords",
                            "free": true,
                        },
                        {
                            "name": "Centre LGBTQIA+",
                            "free": true,
                        },
                        {
                            "name": "Approches Cultures & Territoires",
                            "free": true,
                        },
                        {
                            "name": "Radar Squat",
                            "free": true,
                        },
                        {
                            "name": "Démosphère",
                            "free": true,
                        },
                        {
                            "name": "Mobilizon",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Cartes",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Réseau des Paniers Marseillais - AMAP",
                            "free": true,
                        },
                        {
                            "name": "Carte Autogéré Rhizome",
                            "free": true,
                        },
                        {
                            "name": "QX1 - WelcomeMap",
                            "free": true,
                        },
                        {
                            "name": "Carte des Luttes - Reporterre",
                            "free": true,
                        },
                        {
                            "name": "Retab.fr",
                            "free": true,
                        },
                        {
                            "name": "Carto-Marseille",
                            "free": true,
                        },
                        {
                            "name": "DICADD",
                            "free": true,
                        },
                        {
                            "name": "FransGenre",
                            "free": true,
                        },
                        {
                            "name": "Transiscope",
                            "free": true,
                        },
                        {
                            "name": "Près de Chez Nous",
                            "free": true,
                        },
                        {
                            "name": "Terre de Liens",
                            "free": true,
                        },
                        {
                            "name": "LGBT+ PACA",
                            "free": true,
                        },
                        {
                            "name": "Habicoop",
                            "free": true,
                        },
                        {
                            "name": "Hameaux Légers",
                            "free": true,
                        },
                        {
                            "name": "Regain & Habitat Participatif Fr",
                            "free": true,
                        },
                        {
                            "name": "Les Écotables",
                            "free": true,
                        },
                        {
                            "name": "Le Carillon",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Médias",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Primitivi",
                            "free": true,
                        },
                        {
                            "name": "Télé Mouche",
                            "free": true,
                        },
                        {
                            "name": "Mars Infos Autonomes",
                            "free": true,
                        },
                        {
                            "name": "Mille Bâbords",
                            "free": true,
                        },
                        {
                            "name": "CQFD",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Radios",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Radio Galère (88.4)",
                            "free": true,
                        },
                        {
                            "name": "Radio Grenouille (88.8)",
                            "free": true,
                        },
                        {
                            "name": "Radio Gazelle (98.0)",
                            "free": true,
                        },
                        {
                            "name": "Radio BAM",
                            "free": true,
                        },
                        {
                            "name": "Radio Zinzine (Limans)",
                            "free": true,
                        },
                        {
                            "name": "Radio Zinzine (Aix)",
                            "free": true,
                        },
                        {
                            "name": "DATA",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Podcasts",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Transféminisme*",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Un podcast à soi",
                                    "free": true,
                                },
                                {
                                    "name": "Les Couilles sur la table",
                                    "free": true,
                                },
                                {
                                    "name": "Le Coeur sur la table",
                                    "free": true,
                                },
                                {
                                    "name": "Un monstre qui vous parle",
                                    "free": true,
                                },
                                {
                                    "name": "Paul B. Preciado, trans-philosophe",
                                    "free": true,
                                },
                                {
                                    "name": "Paul B. Preciado : trans révolutionnaire",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Psychothérapie Institutionnelle",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "De Saint-Alban à La Borde - France Culture",
                                    "free": true,
                                }
                            ]
                        }
                    ]
                },
                {
                    "name"    : "Revues | Journaux",
                    "free"    : true,
                    "children": [
                        {
                            "name": "SoinSoin",
                            "free": true,
                        },
                        {
                            "name": "Sang D'encre",
                            "free": true,
                        },
                        {
                            "name": "Les Cahiers A2C",
                            "free": true,
                        },
                        {
                            "name": "Un Autre Monde",
                            "free": true,
                        },
                        {
                            "name": "CQFD",
                            "free": true,
                        },
                        {
                            "name": "La terre en Thiers",
                            "free": true,
                        },
                        {
                            "name": "Revue Charbon",
                            "free": true,
                        },
                        {
                            "name": "Revue Rhizome",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "InfoKiosques",
                    "free"    : true,
                    "children": [
                        {
                            "name": "SPAAM-infos",
                            "free": true,
                        },
                        {
                            "name": "Folie et Politique -Barge",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Répertoires",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Santé | Soin | Thérapie",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "TransFriendly",
                                    "free": true,
                                },
                                {
                                    "name": "PsySafe",
                                    "free": true,
                                },
                                {
                                    "name": "SPAAM-Répertoire",
                                    "free": true,
                                }
                            ]
                        },
                        {
                            "name": "Juridique",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Réseaux",
                    "free"    : true,
                    "children": [
                        {
                            "name": "RezoProspec",
                            "free": true,
                        },
                        {
                            "name": "Réseau Les Paniers Marseillais (AMAP)",
                            "free": true,
                        },
                        {
                            "name": "Réseau COFOR",
                            "free": true,
                        },
                        {
                            "name": "Projet ASSAB",
                            "free": true,
                        },
                        {
                            "name": "Réseau Gestion_Mars_Conflits",
                            "free": true,
                        },
                        {
                            "name": "Le Marché Rouge",
                            "free": true,
                        },
                        {
                            "name": "TRUC",
                            "free": true,
                        },
                        {
                            "name": "Réseau des Créfad",
                            "free": true,
                        },
                        {
                            "name": "Habicoop",
                            "free": true,
                        },
                        {
                            "name": "Actions Communes",
                            "free": true,
                        },
                    ]
                },
                {
                    "name"    : "Circuit Court",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Épiceries",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Bar à Vrac",
                                    "free": true,
                                },
                                {
                                    "name": "L'Épicerie Paysanne",
                                    "free": true,
                                },
                                {
                                    "name": "La plaine fraicheur",
                                    "free": true,
                                },
                                {
                                    "name": "Adèle",
                                    "free": true,
                                }
                            ]
                        },
                        {
                            "name"    : "Restos | Cantines",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Cantine du Midi",
                                    "free": true,
                                },
                                {
                                    "name": "Les Ondines",
                                    "free": true,
                                },
                                {
                                    "name": "Café l'Ecomotive",
                                    "free": true,
                                },
                                {
                                    "name": "Casa Consolat",
                                    "free": true,
                                },
                                {
                                    "name": "Le Grain de Sable",
                                    "free": true,
                                },
                                {
                                    "name": "La Marmite Joyeuse",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Boulangeries",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Le Bar à Pain",
                                    "free": true,
                                },
                                {
                                    "name": "Les Mains Libres",
                                    "free": true,
                                },
                                {
                                    "name": "Boulangerie-Café Pain Salvator",
                                    "free": true,
                                },
                                {
                                    "name": "House of Pain",
                                    "free": true,
                                }
                            ]
                        },
                        {
                            "name"    : "Marchés",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Le Marché Rouge",
                                    "free": true,
                                },
                                {
                                    "name": "Marché des artisans et des producteurs bio",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Supermarchés",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Super Cafoutch",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Friperies",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Frip'Insertion - Libération",
                                    "free": true,
                                },
                                {
                                    "name": "Frip'Insertion - Capelette ",
                                    "free": true,
                                },
                            ]
                        },
                    ]
                },
                {
                    "name"    : "Ateliers",
                    "free"    : true,
                    "children": [

                        {
                            "name"    : "Ateliers Vélo",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Vélo Sapiens",
                                    "free": true,
                                },
                                {
                                    "name": "Collectif Vélos en Ville",
                                    "free": true,
                                }
                            ]
                        }
                    ]
                },
                {
                    "name"    : "Festivals",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Relève",
                            "free": true,
                        }
                    ]
                },
                {
                    "name"    : "Outils",
                    "free"    : true,
                    "children": [
                        {
                            "name": "Calculs Mutuelle",
                            "free": true,
                        },
                        {
                            "name"    : "Numériques",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Visualisation",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "LiveGap Charts",
                                            "free": true,
                                        },
                                        {
                                            "name": "Graph Maker",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                    ]
                },
                {
                    "name"    : "Savoir-faire",
                    "free"    : true,
                    "children": [

                        {
                            "name"    : "Autonomie",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "Sélection Rhizome",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Le moteur stirling",
                                            "free": true,
                                        },
                                        {
                                            "name": "Mouche Soldat Noir",
                                            "free": true,
                                        },
                                        {
                                            "name": "Aquaponie",
                                            "free": true,
                                        },
                                        {
                                            "name": "Biodigesteur",
                                            "free": true,
                                        },
                                        {
                                            "name": "Pompe à bélier",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name": "Énergie",
                                    "free": true,
                                },
                                {
                                    "name": "Compostage",
                                    "free": true,
                                },
                                {
                                    "name": "Systèmes Agricoles",
                                    "free": true,
                                },
                                {
                                    "name": "Pompes à eau",
                                    "free": true,
                                },
                            ]
                        },
                        {
                            "name"    : "Pédagogie",
                            "free"    : true,
                            "children": [
                                {
                                    "name": "Freinet",
                                    "free": true,
                                },
                                {
                                    "name": "Pédagogie Institutionnelle",
                                    "free": true,
                                },
                                {
                                    "name": "RERS",
                                    "free": true,
                                },
                            ]
                        },
                    ]
                },
                {
                    "name"    : "Services Sociaux",
                    "free"    : true,
                    "children": [
                        {
                            "name"    : "Assistance Sociale",
                            "free"    : true,
                            "children": [
                                {
                                    "name"    : "CCAS",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "Agence Centre|13002",
                                            "free": true,
                                        },
                                        {
                                            "name": "Agence Est|13004",
                                            "free": true,
                                        },
                                        {
                                            "name": "Agence Sud|13008",
                                            "free": true,
                                        },
                                        {
                                            "name": "Agence Nord|13014",
                                            "free": true,
                                        },
                                    ]
                                },
                                {
                                    "name"    : "Maisons de Solidarité",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "MDS COLBERT | 01",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS DU LITTORAL | 02",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS BELLE DE MAI | 03",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS CHARTREUX | 04",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS ST SEBASTIEN | 06",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS DE BONNEVEINE | 08",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS PONT DE VIVAUX | 10",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS SAINT MARCEL | 11",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS LE NAUTILE | 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS MALPASSÉ | 13",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS DE LA VISTE | 15",
                                            "free": true,
                                        },
                                        {
                                            "name": "MDS DE L'ESTAQUE | 16",
                                            "free": true,
                                        },


                                    ]
                                },
                                {
                                    "name"    : "Logement",
                                    "free"    : true,
                                    "children": [
                                        {
                                            "name": "EAH",
                                            "free": true,
                                        },
                                    ]
                                },
                            ]
                        },
                    ]
                },
            ]
        }
    ]
};

const browse_data = (node, depth = []) => {
    if (node.name.length === 0) {
        return;
    }

    let node_depth_index = 0;

    if (depth.length === 0) {
        mapping_nodename_with_their_ways[node.name] = [];
        mapping_nodename_with_their_ways[node.name][0] = [];
    } else if (!mapping_nodename_with_their_ways[node.name]) {
        // console.log(node.name);
        mapping_nodename_with_their_ways[node.name] = [];
        mapping_nodename_with_their_ways[node.name][0] = depth;
        /* mapping_nodename_with_their_ways[node.name][0] = "";
         mapping_nodename_with_their_ways[node.name][0] = depth;*/
    } else {
        // console.log("another time : " + node.name);
        mapping_nodename_with_their_ways[node.name].push(depth);

        node_depth_index = mapping_nodename_with_their_ways[node.name].length - 1;
    }
    // console.log(mapping_nodename_with_their_ways[node.name], node_depth_index);
    if (node.children) {
        node.children.forEach((child, index) => {
            // console.log(child.name, mapping_nodename_with_their_ways[node.name].concat([index]))
            browse_data(child, mapping_nodename_with_their_ways[node.name][node_depth_index].concat([index]));
        });
    }
};

browse_data(hierarchy_tree_data);

delete mapping_nodename_with_their_ways["Rhizome Marseille"];

Object
    .keys(mapping_nodename_with_their_ways)
    .sort((a, b) => a.localeCompare(b, "fr", {ignorePunctuation: true}))
    .forEach((node_name, index) => {
        // console.log(mapping_nodename_with_their_ways[node_name]);
        final_model[node_name] = [];

        mapping_nodename_with_their_ways[node_name].forEach((way) => {
            // console.log(way);
            final_model[node_name].push(way.join(","));
        });

    });
console.log(final_model);
