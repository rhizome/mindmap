# INIT
echo "Init Build"
. ~/.nvm/nvm.sh
# chemin vers la clé privée à utiliser
SSH_PRIVATE_KEY=~/.ssh/mattieu_barbarescho_github
# l'empreinte de la clé
SSH_KEY_FINGERPRINT=`ssh-keygen -E sha256 -lf "$SSH_PRIVATE_KEY" | cut -d\  -f2`
# chargement de keychain en mode silencieux
eval `keychain --quiet --agents ssh --noask --eval "$SSH_PRIVATE_KEY"`
# vérification que la clé privée est bien chargée dans keychain
if keychain --list | grep $SSH_KEY_FINGERPRINT > /dev/null; then
  echo "PRIVATE KEY FOR GIHUB SUCCESSFULLY LOADED"

  # RESET
  echo "Reset & update local remote"
  cd /var/www/mindmap/
  echo "Current Directory:"
  pwd
  rm -rf ./app/src
  git checkout .
  git checkout main
  #Get origin main
  git pull origin main

  # Build and deploy
  echo "Build & deploy"
  cd ./app/
  echo "Current Directory:"
  pwd
  nvm install 20.18.0
  npm install -g npm@latest
  rm -rf ./node_modules
  echo "Install node_modules to build assets"
  npm install
  echo "Update HIND"
  npx hind -v
  npx hind update
  cp -r ../../local_config/ ./

  # Build assets
  echo "Build assets"
  npm run build

  # Remove uneccessary files & dir
  rm package.json package-lock.json README_ADMIN.md new.*.json .babelrc.json webpack.*.js

  # Fill ./src directory - Step 1
  mkdir src
  mv ./local_config/ ./src/
  mv server.js ./src
  mv workspace/ ./src/

  # Prepare and install prod dependencies
  mv package.preprod.json package.json
  mv .babelrc_preprod.json .babelrc.json
  mv ./node_modules/hind/ ../
  rm -rf node_modules/
  #rm -r !(hind) => Plus besoin
  echo "Install node_modules to build server files"
  npm install
  rm package-lock.json
  mv ../hind/ ./node_modules/

  # Build server files
  echo "Build server files"
  npm run build

  # Update PM2 && Restart server
  echo "Reload server"
  npm install pm2 -g
  /home/debian/.npm-global/bin/pm2 update
else
  printf "Erreur, clé %s non chargée...\n" "$SSH_PRIVATE_KEY" >&2
fi