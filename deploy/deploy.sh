#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER_PROD_DNS

echo "Deploying to ${DEPLOY_SERVER}"

ssh debian@${DEPLOY_SERVER} 'bash -s' < ./deploy/server.sh